/*jslint browser:true */
(function ($) {
    "use strict";

    $.fn.do_action_notice = function()
    {
        var container = $(this),
            notice    = container.find('.epic-action-notice');

        notice.addClass('expanded').slideDown();

        notice.find('.fa').on('click', function(e)
        {
            e.stopPropagation();
            notice.removeClass('expanded').slideUp();
        });

        setTimeout( function()
        {
            if ( notice.hasClass('expanded') && !notice.hasClass('warning') )
            {
                notice.slideUp().removeClass('expanded');
            }
        }, 3000);
    };

    function do_copy_system_report()
    {
        $(".debug-report").on('click', function(e)
        {
            e.preventDefault();
            $(this).find('textarea').focus().select();
            document.execCommand('copy');
            $(this).do_action_notice();
        });
    }

    // Ready function
    function do_ready() {
        do_copy_system_report();
    }

    $(document).ready(do_ready);
})(jQuery);
