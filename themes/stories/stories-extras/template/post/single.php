<?php get_header(); ?>

<?php do_action( 'epic_single_post_template_before_content' ); ?>
<?php global $post; ?>
<!-- improve single classlarini alabilirmiyiz -->
<article id="post-<?php $post->ID; ?>" class="blog-post-wrapper post-<?php $post->ID; ?> post type-post hentry">
	<?php
	if ( have_posts() ) :
		the_post();

        $template_id  = epic_get_option('single_post_template_id');

		if ( $template_id )
			echo jeg_render_builder_content( $template_id );

	endif;
	?>
</article>

<?php do_action( 'epic_single_post_template_after_content' ); ?>

<?php get_footer(); ?>
