<?php

// improve adlari duzenleme
defined( 'EPIC' ) or define( 'EPIC', 'stories-extras' );
defined( 'EPIC_VERSION' ) or define( 'EPIC_VERSION', '2.2.1' );
defined( 'EPIC_URL' ) or define( 'EPIC_URL', get_template_directory_uri( ).'/stories-extras' );
defined( 'EPIC_FILE' ) or define( 'EPIC_FILE', get_template_directory().'/stories-extras/stories-extras.php' );
defined( 'EPIC_DIR' ) or define( 'EPIC_DIR', get_template_directory().'/stories-extras/' );
defined( 'EPIC_PATH' ) or define( 'EPIC_PATH', 'stories-extras/stories-extras.php' );

defined( 'JEG_FRAMEWORK' ) or define( 'JEG_FRAMEWORK', 'jeg_customizer_framework' );
defined( 'JEG_THEME_URL' ) or define( 'JEG_THEME_URL', EPIC_URL );
define( 'THEME_URI', get_template_directory_uri() );
require_once EPIC_DIR . 'lib/jeg-framework/bootstrap.php';
require_once EPIC_DIR . 'lib/epic-dashboard/bootstrap.php';
require_once EPIC_DIR . 'includes/autoload.php';

EPIC\Init::getInstance();



// Elementor-Modules.php uses this code. 
remove_filter( 'nav_menu_description', 'strip_tags' );



// Breadcrumb Trail

if (!function_exists('breadcrumb_trail')) {

	/**
	 * Load libraries.
	 */

	require_once EPIC_DIR . '/breadcrumb-trail/breadcrumb-trail.php';
}



// Modules for Header & Footer Builder


