<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Option;

use Jeg\Customizer;

Class Option
{
    /**
     * @var Option
     */
    private static $instance;

    /**
     * @return Option
     */
    public static function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * FrontendAjax constructor.
     */
    private function __construct()
    {
        add_action('jeg_register_customizer_option', array($this, 'register_lazy_section'), 91);
        add_filter('jeg_register_lazy_section', array($this, 'load_customizer'));
    }

    public function register_lazy_section()
    {
        $customizer = Customizer\Customizer::get_instance();
        $this->build_module_option( $customizer );
	    $this->build_custom_template_option( $customizer );
	    $this->build_template_style_option( $customizer );
    }

    public function build_custom_template_option( $customizer ) {

	    $customizer->add_panel(array(
		    'id' => 'epic_stories_site_layout',
		    'title' => esc_html__('Layout - width, sidebar…', 'Stories'),
		    'description' => esc_html__('Jeg ', 'Stories'),
		    'priority' => 171
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_module_header_builder_section',
		    'title'         => esc_html__('Header Builder', 'Stories'),
		    'panel'         => 'epic_stories_site_layout',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_layout_site_section',
		    'title'         => esc_html__('Site Layout', 'Stories'),
		    'panel'         => 'epic_stories_site_layout',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
		));

	    $customizer->add_section(array(
		    'id'            => 'epic_layout_index_section',
		    'title'         => esc_html__('Index Layout', 'Stories'),
		    'panel'         => 'epic_stories_site_layout',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_layout_single_section',
		    'title'         => esc_html__('Single Layout', 'Stories'),
		    'panel'         => 'epic_stories_site_layout',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_layout_page_section',
		    'title'         => esc_html__('Page Layout', 'Stories'),
		    'panel'         => 'epic_stories_site_layout',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_layout_archieve_section',
		    'title'         => esc_html__('Archieve Layout - category, author…', 'Stories'),
		    'panel'         => 'epic_stories_site_layout',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));
 
	}
	
    public function build_template_style_option( $customizer ) {

		$customizer->add_panel(array(
		    'id' => 'epic_site_style',
		    'title' => esc_html__('Styling - color, fonts…', 'Stories'),
		    'description' => esc_html__('Jeg ', 'Stories'),
		    'priority' => 171
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_site_font_section',
		    'title'         => esc_html__('Fonts', 'Stories'),
		    'panel'         => 'epic_site_style',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_site_color_section',
		    'title'         => esc_html__('Colors', 'Stories'),
		    'panel'         => 'epic_site_style',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_sidebar_styles_section',
		    'title'         => esc_html__('Sidebar Styles', 'Stories'),
		    'panel'         => 'epic_site_style',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));
 
    }

    public function build_module_option( $customizer ) {

	    $customizer->add_panel(array(
		    'id' => 'epic_module_panel',
		    'title' => esc_html__('Blocks - shortcode, hero, modules…', 'Stories'),
		    'description' => esc_html__('Jeg ', 'Stories'),
		    'priority' => 171
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_module_image_section',
		    'title'         => esc_html__('Module Image Setting', 'Stories'),
		    'panel'         => 'epic_module_panel',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_module_loader_section',
		    'title'         => esc_html__('Module Loader', 'Stories'),
		    'panel'         => 'epic_module_panel',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_module_meta_section',
		    'title'         => esc_html__('Module Meta Option', 'Stories'),
		    'panel'         => 'epic_module_panel',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

	    $customizer->add_section(array(
		    'id'            => 'epic_module_custom_post_type_section',
		    'title'         => esc_html__('Module Custom Post Type', 'Stories'),
		    'panel'         => 'epic_module_panel',
		    'priority'      => 171,
		    'type'          => 'jeg-lazy-section',
		    'dependency'    => array()
	    ));

    }

    public function load_customizer($result)
    {
        $array = [
            'module_image',
			'module_loader',
			
            'module_meta',
			'module_header_builder',
			'module_custom_post_type',		

			'layout_archieve',
	        'layout_site',
	        'layout_index',
	        'layout_single',
			'layout_page',

	        'layout_custom_post_type',

	        'module_global_layout',
			'site_font',
			'site_color',
			'site_sidebar_styles',
        ];

        $path = EPIC_DIR . "includes/class/Option/sections/";

        foreach($array as $id) {
            $result["epic_{$id}_section"][] = "{$path}{$id}.php";
        }

        return $result;
    }
}
