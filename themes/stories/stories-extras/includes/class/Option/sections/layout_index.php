<?php

$options = array();

$all_sidebar = $GLOBALS['wp_registered_sidebars'];
foreach ( $all_sidebar as $sidebar ){
   $sidebar_options[$sidebar['id']] = $sidebar['name'];
}

$options[] = array(
    'id'            => 'epic-ne[index_sidebar_option]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Layout','Stories'),
    'description'   => esc_html__('Set custom layout/sidebar for Single','Stories'),
);


$options[] = array(
    'id'            => 'epic-ne[index_sidebar]',
    'type'          => 'jeg-header',
    'label'         => esc_html__('Index Sidebar','Stories' ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[index_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[index_layout]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'right-sidebar',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('index Layout','jnews' ),
    'description'   => esc_html__('Choose index layout.','jnews' ),
    'choices'       => array( 
        'left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-left-sidebar.jpg')
        ),
        'right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-right-sidebar.jpg' )
        ),
        '2-left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-left-sidebar.jpg')
		),
        '2-right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-right-sidebar.jpg')
		),
        'both-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-both-sidebar.jpg')
        ),
        'no-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-no-sidebar.jpg')
        ),
        'build-your-own' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/build-your-own.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'home_tag',
            'refresh'   => true
        )
        ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[index_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[index_sidebar_first]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Index Sidebar','jnews'),
    'description'   => wp_kses(__("Select index sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[index_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
        ),
        array(
            'setting'  => 'epic-ne[index_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
    'id'            => 'epic-ne[index_sidebar_second]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Second Sidebar','jnews'),
    'description'   => wp_kses(__("Select index sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[index_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
        ),
        array(
            'setting'  => 'epic-ne[index_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);




return $options;
