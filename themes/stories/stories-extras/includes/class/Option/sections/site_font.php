<?php

$options = array();

// improve - google font ve custom fontlar cekilcek

$options[] = array(
    'id'            => 'jnews_additional_font',
    'transport'     => 'postMessage',
	'option_type' => 'option',
    'type'          => 'jeg-repeater',
    'label'         => esc_html__('Add Custom Font', 'jnews' ),
    'description'   => wp_kses(sprintf(__("<a href='%s' target='_blank'>How to Add Custom Font?</a>.
                                ", "jnews"), "http://www.fontsquirrel.com/tools/webfont-generator"),
        wp_kses_allowed_html()),
    'default'       => array(),
    'row_label'     => array(
        'type' => 'text',
        'value' => esc_attr__( 'Custom font', 'jnews' ),
        'field' => false,
    ),
    'fields' => array(
        'font_name' => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Font Name', 'jnews' ),
            'description' => esc_attr__( 'Please fill your font name. You can put same font name on font uploader in case you have several types of font (bold, italic or other).', 'jnews' ),
            'default'     => '',
        ),
        'font_weight' => array(
            'type'        => 'select',
            'label'       => esc_attr__( 'Font Weight', 'jnews' ),
            'description' => esc_attr__( 'Please choose this file\'s font weight.', 'jnews' ),
            'choices'     => array(
                '100'     => '100',
                '200'     => '200',
                '300'     => '300',
                '400'     => '400',
                '500'     => '500',
                '600'     => '600',
                '700'     => '700',
                '800'     => '800',
                '900'     => '900',
            ),
            'default'     => '400',
        ),
        'font_style' => array(
            'type'        => 'select',
            'label'       => esc_attr__( 'Font Style', 'jnews' ),
            'description' => esc_attr__( 'Please fill this file\'s font style.', 'jnews' ),
            'choices'     => array(
                'italic'        => esc_attr__('Italic', 'jnews'),
                'normal'       => esc_attr__('Regular', 'jnews'),
            ),
            'default'     => 'regular',
        ),
        'eot' => array(
            'type'        => 'upload',
            'label'       => esc_attr__( 'EOT File', 'jnews' ),
            'default'     => '',
            'mime_type'   => 'font',
        ),
        'woff' => array(
            'type'        => 'upload',
            'label'       => esc_attr__( 'WOFF File', 'jnews' ),
            'default'     => '',
            'mime_type'   => 'font',
        ),
        'ttf' => array(
            'type'        => 'upload',
            'label'       => esc_attr__( 'TTF File', 'jnews' ),
            'default'     => '',
            'mime_type'   => 'font',
        ),
        'svg' => array(
            'type'        => 'upload',
            'label'       => esc_attr__( 'SVG File', 'jnews' ),
            'description'       => esc_attr__( 'Refresh the page to see your custom font on the Font List', 'jnews' ),
            'default'     => '',
            'mime_type'   => 'font',
        ),
    )
);



$options[] = array(
	'id'          => 'epic-ne[stories_style_main_font]',
    'transport'   => 'refresh',
	'option_type' => 'option',
	'type'        => 'jeg-typography',
	'label'       => esc_html__( 'Body Font', 'Stories' ),
	'description' => esc_html__( 'Set global content font.', 'Stories' ),
	'option_type' => 'option',
	'default'     => array(
		'font-family' => 'Merriweather',

	),
);



// Improve Custom Font ile caliscak hale getir.

$options[] = array(
    'id'            => 'epic-ne[stories_style_title_font]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'HK Grotesk',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Primary Font', 'Stories'),
    'description'   => esc_html__('Choose Primary font.', 'Stories'),
    'choices'       => array(
        'Playfair Display'      => 'Playfair Display',
        'HK Grotesk'		    => 'HK Grotesk',
        'Merriweather'          => 'Merriweather'
    ),
);


$options[] = array(
	'id'          => 'epic-ne[stories_style_helper_title_font]',
	'transport'   => 'refresh',
	'type'        => 'jeg-typography',
	'label'       => esc_html__( 'Secondary Title Font', 'Stories' ),
	'description' => esc_html__( 'Set Secondary Title font.', 'Stories' ),
	'option_type' => 'option',
	'default'     => array(
		'font-family' => esc_html__('Playfair Display','Stories'),

	),
);


return $options;
