<?php

$options = array();

$options[] = array(
    'id'            => 'epic-ne[block_notice]',
    'option_type'   => 'option',
    'type'          => 'jeg-alert',
    'default'       => 'info',
    'label'         => esc_html__('Notice','Stories' ),
    'description'   => wp_kses(__(
        '<ul>
            <li>Every element will behave differently when option changed depend on default meta on each element</li>
        </ul>',
        'Stories'), wp_kses_allowed_html()),
);

$options[] = array(
    'id'            => 'epic-ne[show_block_meta]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => true,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Show Block Meta','Stories'),
    'description'   => esc_html__('Show meta for block','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[show_block_meta_author]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => true,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Show Block Meta - Author','Stories'),
    'description'   => esc_html__('Show author on meta block','Stories'),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[show_block_meta_date]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => true,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Show Block Meta - Date','Stories'),
    'description'   => esc_html__('Show date on meta block','Stories'),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[show_block_meta_comment]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => true,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Show Block Meta - Comment','Stories'),
    'description'   => esc_html__('Show comment on meta block','Stories'),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);






$options[] = array(
    'id'            => 'epic-ne[global_post_date]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'modified',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Post Date Meta','Stories'),
    'description'   => esc_html__('Choose which post date type that you want to show for global post date meta.','Stories'),
    'choices'       => array(
        'publish' => esc_attr__( 'Publish Date', 'Stories' ),
        'modified' => esc_attr__( 'Modified Date', 'Stories' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        ),
        array(
            'setting'  => 'epic-ne[show_block_meta_date]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[show_block_meta_comment]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => true,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Show Block Meta - Comment','Stories'),
    'description'   => esc_html__('Show comment icon on meta block','Stories'),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[show_block_meta_format]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => true,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Show Block Meta - Format','Stories'),
    'description'   => esc_html__('Show format icon on meta block','Stories'),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);
$options[] = array(
    'id'            => 'epic-ne[show_block_meta_reading_time]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => true,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Show Block Meta - Reading Time','Stories'),
    'description'   => esc_html__('Show Reading time  on meta block','Stories'),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[show_block_meta_post_view]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => true,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Show Block Meta - Post View','Stories'),
    'description'   => esc_html__('Show Post View on meta block','Stories'),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[show_block_meta_share]',
	'option_type'   => 'option',
	'transport'     => 'refresh',
	'default'       => true,
	'type'          => 'jeg-toggle',
	'label'         => esc_html__('Show Block Meta - Share','Stories'),
	'description'   => esc_html__('Show share icon on meta block','Stories'),
	'active_callback'  => array(
		array(
			'setting'  => 'epic-ne[show_block_meta]',
			'operator' => '==',
			'value'    => true,
		)
	),
);

$options[] = array(
	'id'            => 'epic-ne[show_block_meta_post_rating]',
	'option_type'   => 'option',
	'transport'     => 'refresh',
	'default'       => true,
	'type'          => 'jeg-toggle',
	'label'         => esc_html__('Show Block Meta - Rating','Stories'),
	'description'   => esc_html__('Show rating icon on meta block','Stories'),
	'active_callback'  => array(
		array(
			'setting'  => 'epic-ne[show_block_meta]',
			'operator' => '==',
			'value'    => true,
		)
	),
);

$options[] = array(
	'id'            => 'epic-ne[show_block_meta_read_more]',
	'option_type'   => 'option',
	'transport'     => 'refresh',
	'default'       => true,
	'type'          => 'jeg-toggle',
	'label'         => esc_html__('Show Block Meta - Read More','Stories'),
	'active_callback'  => array(
		array(
			'setting'  => 'epic-ne[show_block_meta]',
			'operator' => '==',
			'value'    => true,
		)
	),
);

$options[] = array(
    'id'            => 'epic-ne[meta_seperator_type]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'times',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Meta Seperator Type', 'epic-ne'),
    'description'   => esc_html__('Choose Meta Seperator Type.', 'epic-ne'),
    'choices'       => array(
            'times'     =>    esc_html__('Times', 'epic-ne'),
            'dot'       =>    esc_html__('Dot', 'epic-ne'),
            'hyphens'   =>    esc_html__('Hyphens', 'epic-ne'),
            'vline'     =>    esc_html__('Vertical Line', 'epic-ne'),
            'slash'     =>    esc_html__('Slash', 'epic-ne'),
            'none'     =>    esc_html__('None', 'epic-ne'),
    ),
); 

$options[] = array(
	'id'            => 'epic-ne[show_block_meta_share_item]',
	'option_type'   => 'option',
	'transport'     => 'postMessage',
	'type'          => 'jeg-repeater',
	'label'         => esc_html__('Share', 'Stories'),
	'description'   => esc_html__('Add Social Site for Share List', 'Stories'),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[show_block_meta]',
            'operator' => '==',
            'value'    => true,
        ),
    ),
	'default'       => array(
		array(
			'social_icon' => 'facebook',
		),
		array(
			'social_icon' => 'twitter',
		),
        array(
            'social_icon' => 'pinterest',
        ),
	),
	'row_label'     => array(
		'type' => 'text',
		'value' => esc_attr__( 'Social Icon', 'Stories' ),
		'field' => false,
	),
	'fields' => array(
		'social_icon' => array(
			'type'        => 'select',
			'label'       => esc_attr__( 'Social Icon', 'Stories' ),
			'default'     => '',
			'choices'     => array(
				''              => esc_attr__( 'Choose Icon', 'Stories' ),
				'facebook'      => esc_attr__( 'Facebook', 'Stories' ),
				'twitter'       => esc_attr__( 'Twitter', 'Stories' ),
				'linkedin'      => esc_attr__( 'Linkedin', 'Stories' ),
				'pinterest'     => esc_attr__( 'Pinterest', 'Stories' ),
				'tumblr'        => esc_attr__( 'Tumblr', 'Stories' ),
                'stumbleupon'   => esc_attr__( 'StumbleUpon', 'Stories' ),
                'whatsapp'      => esc_attr__( 'WhatsApp', 'Stories' ),
                'email'          => esc_attr__( 'E-mail', 'Stories' ),
				'vk'            => esc_attr__( 'Vk', 'Stories' ),
				'reddit'        => esc_attr__( 'Reddit', 'Stories' ),
				'wechat'        => esc_attr__( 'WeChat', 'Stories' ),
			),
		)
	)
);

return $options;
