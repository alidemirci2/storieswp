<?php

$options = array();

$all_sidebar = $GLOBALS['wp_registered_sidebars'];
foreach ( $all_sidebar as $sidebar ){
   $sidebar_options[$sidebar['id']] = $sidebar['name'];
}


/**
 * Category
 */
$options[] = array(
	'id'    => 'epic-ne[single_category_template_header]',
	'type'  => 'jeg-header',
	'label' => esc_html__( 'Category Template', 'Stories' ),
);





$options[] = array(
    'id'            => 'epic-ne[category_sidebar_option]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Layout','Stories'),
    'description'   => esc_html__('Set custom layout/sidebar for category','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[category_sidebar]',
    'type'          => 'jeg-header',
	'label'         => esc_html__('Category Sidebar','Stories' ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[category_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[category_layout]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'right-sidebar',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('Category Layout','jnews' ),
    'description'   => esc_html__('Choose category layout.','jnews' ),
    'choices'       => array( 
        'left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-left-sidebar.jpg')
        ),
        'right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-right-sidebar.jpg' )
        ),
        '2-left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-left-sidebar.jpg')
		),
        '2-right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-right-sidebar.jpg')
		),
        'both-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-both-sidebar.jpg')
        ),
        'no-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-no-sidebar.jpg')
        ),
        'build-your-own' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/build-your-own.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'home_tag',
            'refresh'   => true
        )
        ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[category_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[category_sidebar_first]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('category Sidebar','jnews'),
    'description'   => wp_kses(__("Select category sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[category_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[category_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[category_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
    'id'            => 'epic-ne[category_sidebar_second]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Second Sidebar','jnews'),
    'description'   => wp_kses(__("Select category sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[category_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[category_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[category_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);



$options[] = array(
	'id'              => 'epic-ne[single_category_template_id]',
	'option_type'     => 'option',
	'transport'       => 'refresh',
	'default'         => '',
	'type'            => 'jeg-select',
	'label'           => esc_html__( 'Category Template List', 'Stories' ),
	'description'     => wp_kses( sprintf( __( 'Create custom category template from <a href="%s" target="_blank">here</a>', 'Stories' ), get_admin_url() . 'edit.php?post_type=archive-template' ), wp_kses_allowed_html() ),
	'multiple'        => 1,
	'choices'         => epic_get_all_custom_archive_template(),
	'active_callback' => array(
        array(
            'setting'  => 'epic-ne[category_layout]',
            'operator' => '==',
            'value'    => 'build-your-own',
		),
        array(
            'setting'  => 'epic-ne[category_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )	
	),
	'postvar'         => array(
		array(
			'redirect' => 'category_tag',
			'refresh'  => true
		)
	),
);

$options[] = array(
	'id'              => 'epic-ne[single_category_template_number_post]',
	'option_type'     => 'option',
	'transport'       => 'refresh',
	'default'         => 10,
	'type'            => 'jeg-text',
	'label'           => esc_html__( 'Number of Post', 'Stories' ),
	'description'     => esc_html__( 'Set the number of post per page on category page.', 'Stories' ),
	'active_callback' => array(
        array(
            'setting'  => 'epic-ne[category_layout]',
            'operator' => '==',
            'value'    => 'build-your-own',
		),
        array(
            'setting'  => 'epic-ne[category_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )	
	),
	'postvar'         => array(
		array(
			'redirect' => 'category_tag',
			'refresh'  => true
		)
	)
);




/**
 * Tag
 */
$options[] = array(
	'id'    => 'epic-ne[single_tag_template_header]',
	'type'  => 'jeg-header',
	'label' => esc_html__( 'Tag Template', 'Stories' ),
);



$options[] = array(
    'id'            => 'epic-ne[tag_sidebar_option]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Layout','Stories'),
    'description'   => esc_html__('Set custom layout/sidebar for tag','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[tag_sidebar]',
    'type'          => 'jeg-header',
	'label'         => esc_html__('Tag Template','Stories' ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[tag_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[tag_layout]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'right-sidebar',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('tag Layout','jnews' ),
    'description'   => esc_html__('Choose tag layout.','jnews' ),
    'choices'       => array( 
        'left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-left-sidebar.jpg')
        ),
        'right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-right-sidebar.jpg' )
        ),
        '2-left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-left-sidebar.jpg')
		),
        '2-right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-right-sidebar.jpg')
		),
        'both-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-both-sidebar.jpg')
        ),
        'no-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-no-sidebar.jpg')
        ),
        'build-your-own' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/build-your-own.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'home_tag',
            'refresh'   => true
        )
        ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[tag_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[tag_sidebar_first]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('tag Sidebar','jnews'),
    'description'   => wp_kses(__("Select tag sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[tag_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[tag_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
		),		
        array(
            'setting'  => 'epic-ne[tag_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
    'id'            => 'epic-ne[tag_sidebar_second]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Second Sidebar','jnews'),
    'description'   => wp_kses(__("Select tag sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[tag_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[tag_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[tag_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
	'id'              => 'epic-ne[single_tag_template_id]',
	'option_type'     => 'option',
	'transport'       => 'refresh',
	'default'         => '',
	'type'            => 'jeg-select',
	'label'           => esc_html__( 'Tag Template List', 'Stories' ),
	'description'     => wp_kses( sprintf( __( 'Create custom category template from <a href="%s" target="_blank">here</a>', 'Stories' ), get_admin_url() . 'edit.php?post_type=archive-template' ), wp_kses_allowed_html() ),
	'multiple'        => 1,
	'choices'         => epic_get_all_custom_archive_template(),
	'active_callback' => array(
		array(
			'setting'  => 'epic-ne[tag_layout]',
			'operator' => '==',
			'value'    => 'build-your-own'
		),
        array(
            'setting'  => 'epic-ne[tag_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
	),
	'postvar'         => array(
		array(
			'redirect' => 'single_post_tag',
			'refresh'  => true
		)
	),
);

$options[] = array(
	'id'              => 'epic-ne[single_tag_template_number_post]',
	'option_type'     => 'option',
	'transport'       => 'refresh',
	'default'         => 10,
	'type'            => 'jeg-text',
	'label'           => esc_html__( 'Number of Post', 'Stories' ),
	'description'     => esc_html__( 'Set the number of post per page on tag archive page.', 'Stories' ),
	'active_callback' => array(
		array(
			'setting'  => 'epic-ne[tag_layout]',
			'operator' => '==',
			'value'    => 'build-your-own'
		),
        array(
            'setting'  => 'epic-ne[tag_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
	),
	'postvar'         => array(
		array(
			'redirect' => 'single_post_tag',
			'refresh'  => true
		)
	)
);

/**
 * Author
 */
$options[] = array(
	'id'    => 'epic-ne[single_author_template_header]',
	'type'  => 'jeg-header',
	'label' => esc_html__( 'Author Template', 'Stories' ),
);






$options[] = array(
    'id'            => 'epic-ne[author_sidebar_option]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Layout','Stories'),
    'description'   => esc_html__('Set custom layout/sidebar for author','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[author_sidebar]',
    'type'          => 'jeg-header',
	'label'         => esc_html__('author Sidebar','Stories' ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[author_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[author_layout]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'right-sidebar',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('author Layout','jnews' ),
    'description'   => esc_html__('Choose author layout.','jnews' ),
    'choices'       => array( 
        'left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-left-sidebar.jpg')
        ),
        'right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-right-sidebar.jpg' )
        ),
        '2-left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-left-sidebar.jpg')
		),
        '2-right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-right-sidebar.jpg')
		),
        'both-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-both-sidebar.jpg')
        ),
        'no-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-no-sidebar.jpg')
        ),
        'build-your-own' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/build-your-own.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'home_tag',
            'refresh'   => true
        )
        ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[author_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[author_sidebar_first]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('author Sidebar','jnews'),
    'description'   => wp_kses(__("Select author sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[author_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[author_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[author_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
    'id'            => 'epic-ne[author_sidebar_second]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Second Sidebar','jnews'),
    'description'   => wp_kses(__("Select author sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[author_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[author_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[author_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
	'id'              => 'epic-ne[single_author_template_id]',
	'option_type'     => 'option',
	'transport'       => 'refresh',
	'default'         => '',
	'type'            => 'jeg-select',
	'label'           => esc_html__( 'Author Template List', 'Stories' ),
	'description'     => wp_kses( sprintf( __( 'Create custom category template from <a href="%s" target="_blank">here</a>', 'Stories' ), get_admin_url() . 'edit.php?post_type=archive-template' ), wp_kses_allowed_html() ),
	'multiple'        => 1,
	'choices'         => epic_get_all_custom_archive_template(),
	'active_callback' => array(
        array(
            'setting'  => 'epic-ne[author_layout]',
            'operator' => '==',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[author_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
	),
	'postvar'         => array(
		array(
			'redirect' => 'author_tag',
			'refresh'  => true
		)
	),
);

$options[] = array(
	'id'              => 'epic-ne[single_author_template_number_post]',
	'option_type'     => 'option',
	'transport'       => 'refresh',
	'default'         => 10,
	'type'            => 'jeg-text',
	'label'           => esc_html__( 'Number of Post', 'Stories' ),
	'description'     => esc_html__( 'Set the number of post per page on post author page.', 'Stories' ),
	'active_callback' => array(
        array(
            'setting'  => 'epic-ne[author_layout]',
            'operator' => '==',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[author_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
	),
	'postvar'         => array(
		array(
			'redirect' => 'author_tag',
			'refresh'  => true
		)
	)
);



/**
 * Date
 */
$options[] = array(
	'id'    => 'epic-ne[single_date_template_header]',
	'type'  => 'jeg-header',
	'label' => esc_html__( 'Date Template', 'Stories' ),
);







$options[] = array(
    'id'            => 'epic-ne[archieve_sidebar_option]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Layout','Stories'),
    'description'   => esc_html__('Set custom layout/sidebar for archieve','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[archieve_sidebar]',
    'type'          => 'jeg-header',
	'label'         => esc_html__('archieve Sidebar','Stories' ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[archieve_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[archieve_layout]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'right-sidebar',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('archieve Layout','jnews' ),
    'description'   => esc_html__('Choose archieve layout.','jnews' ),
    'choices'       => array( 
        'left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-left-sidebar.jpg')
        ),
        'right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-right-sidebar.jpg' )
        ),
        '2-left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-left-sidebar.jpg')
		),
        '2-right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-right-sidebar.jpg')
		),
        'both-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-both-sidebar.jpg')
        ),
        'no-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-no-sidebar.jpg')
        ),
        'build-your-own' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/build-your-own.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'home_archieve',
            'refresh'   => true
        )
        ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[archieve_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[archieve_sidebar_first]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('archieve Sidebar','jnews'),
    'description'   => wp_kses(__("Select archieve sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[archieve_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[archieve_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[archieve_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
    'id'            => 'epic-ne[archieve_sidebar_second]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Second Sidebar','jnews'),
    'description'   => wp_kses(__("Select archieve sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[archieve_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[archieve_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[archieve_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
	'id'              => 'epic-ne[single_date_template_id]',
	'option_type'     => 'option',
	'transport'       => 'refresh',
	'default'         => '',
	'type'            => 'jeg-select',
	'label'           => esc_html__( 'Date Template List', 'Stories' ),
	'description'     => wp_kses( sprintf( __( 'Create custom category template from <a href="%s" target="_blank">here</a>', 'Stories' ), get_admin_url() . 'edit.php?post_type=archive-template' ), wp_kses_allowed_html() ),
	'multiple'        => 1,
	'choices'         => epic_get_all_custom_archive_template(),
	'active_callback' => array(
        array(
            'setting'  => 'epic-ne[archieve_layout]',
            'operator' => '==',
            'value'    => 'build-your-own',
		),	
        array(
            'setting'  => 'epic-ne[archieve_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
	)
);

$options[] = array(
	'id'              => 'epic-ne[single_date_template_number_post]',
	'option_type'     => 'option',
	'transport'       => 'refresh',
	'default'         => 10,
	'type'            => 'jeg-text',
	'label'           => esc_html__( 'Number of Post', 'Stories' ),
	'description'     => esc_html__( 'Set the number of post per page on date archive page.', 'Stories' ),
	'active_callback' => array(
        array(
            'setting'  => 'epic-ne[archieve_layout]',
            'operator' => '==',
            'value'    => 'build-your-own',
		),
        array(
            'setting'  => 'epic-ne[archieve_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )	
	),
);






return $options;
