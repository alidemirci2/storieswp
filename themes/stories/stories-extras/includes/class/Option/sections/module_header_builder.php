<?php

$options = array();
$args = array( 'post_type' => 'elementor-hf', 'posts_per_page' =>-1,  'meta_query' => array(
    array(
        'key'     => 'ehf_template_type',
        'value'   => 'type_header',
        'compare' => '>=',
    ),
), );
$post_type[]= array();
$loop = new WP_Query( $args );
$i = 0;
while ( $loop->have_posts() ) : $loop->the_post();
    $post_type[$i] ["postid"] = get_the_ID(); 
    $post_type[$i] ["title"] = get_the_title();
    $post_type[$i] ["img"] =get_the_post_thumbnail_url();  
    $post_type[$i] ["selected"] = get_post_meta(get_the_ID(),'set_main_header', true); 
     
    $i++;
endwhile;  
//var_dump ($post_type); 
$options[] = array(
    'id'            => 'epic-ne[module_header_builder_notice]',
    'type'          => 'jeg-alert',
    'default'       => 'warning',
    'label'         => esc_html__('Notice','Stories' ),
    'description'   => wp_kses(__(
        '<ul>
                    <li>We will reset all options inside header builder panel when you click one of the starter layout</li>
                    <li>You can modify your header using header builder like normal after choosing header builder layout.</li>
                </ul>',
        'Stories'), wp_kses_allowed_html()),
);
  

$options[] = array(
    'id'            => 'epic-ne[module_header_builder_options]',
    'transport'     => 'refresh',
    'default'       => '',
    'type'          => 'jeg-radio-image',
    'label'         => 'Preset',
    'multiple'      => 1,
    'choices'     =>  $post_type
);

return $options;
