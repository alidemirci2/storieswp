<?php

$options = array();
 

$post_types =  array(
    "foo" => "bar",
    "bar" => "foo",
);

if ( ! empty( $post_types ) && is_array( $post_types ) ) {

	foreach ( $post_types as $key => $label ) {

		$options[] = array(
			'id'          => 'epic-ne[enable_cpt_' . $key . ']',
			'option_type' => 'option',
			'transport'   => 'postMessage',
			'default'     => true,
			'type'        => 'jeg-toggle',
			'label'       => sprintf( esc_html__( 'Enable %s Post Type', 'Stories' ), $label ),
			'description' => sprintf( esc_html__( 'Enable %s post type and their custom taxonomy as content filter.', 'Stories' ), strtolower( $label ) )
		);
	}

} else {
	$options[] = array(
		'id'          => 'epic-ne[enable_post_type_alert]',
		'type'        => 'jeg-alert',
		'default'     => 'info',
		'label'       => esc_html__( 'Notice', 'Stories' ),
		'description' => esc_html__( 'There\'s no custom post type found.', 'Stories' ),
	);
}

return $options;
