<?php

$options = array();

$all_sidebar = $GLOBALS['wp_registered_sidebars'];
foreach ( $all_sidebar as $sidebar ){
   $sidebar_options[$sidebar['id']] = $sidebar['name'];
}




$options[] = array(
    'id'            => 'epic-ne[single_post_template_width_option]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Single Width','Stories'),
    'description'   => esc_html__('Set custom width','Stories'),
);


$options[] = array(
	'id'            => 'epic-ne[single_post_template_dekstop_container]',
	'option_type'   => 'option',
	'transport'     => 'postMessage',
	'default'       => '825',
	'type'          => 'jeg-range-slider',
	'label'         => esc_html__('Single Width', 'Stories'),
	'description'   => esc_html__('Set your Single MAX Width Size', 'Stories'),
	'choices'     => array(
		'min'       => '600',
		'max'       => '1300',
		'step'      => '10',
	),
	'active_callback'  => array(
		array(
			'setting'  => 'epic-ne[single_post_template_width_option]',
			'operator' => '==',
			'value'    => true,
		)
	),
	'postvar'       => array(
		array(
			'redirect'  => 'single_post_tag',
			'refresh'   => true
		)
	),
	'output'     => array(
		array(
			'mediaquery'    => '@media only screen and (min-width : 1200px)',
			'method'        => 'inject-style',
			'element'       => '.default-single article',
			'property'      => 'max-width',
			'units'         => 'px',
		)
	)
);


$options[] = array(
    'id'            => 'epic-ne[single_sidebar_option]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Layout','Stories'),
    'description'   => esc_html__('Set custom layout/sidebar for Single','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[single_dropcap]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Dropcap','Stories'),
    'description'   => esc_html__('Add Dropcap','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[single_sidebar]',
    'type'          => 'jeg-header',
	'label'         => esc_html__('Single Sidebar','Stories' ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[single_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[single_layout]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'right-sidebar',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('Single Layout','jnews' ),
    'description'   => esc_html__('Choose single layout.','jnews' ),
    'choices'       => array( 
        'left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-left-sidebar.jpg')
        ),
        'right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-right-sidebar.jpg' )
        ),
        '2-left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-left-sidebar.jpg')
		),
        '2-right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-right-sidebar.jpg')
		),
        'both-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-both-sidebar.jpg')
        ),
        'no-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-no-sidebar.jpg')
        ),
        'build-your-own' => array (  
            'img' => esc_url( EPIC_URL . '/assets/img/admin/layouts/build-your-own.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'single_post_tag',
            'refresh'   => true
        )
        ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[single_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[single_sidebar_first]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Single Sidebar','jnews'),
    'description'   => wp_kses(__("Select single sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[single_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
        ),
        array(
            'setting'  => 'epic-ne[single_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
        ),
        array(
            'setting'  => 'epic-ne[single_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
    'id'            => 'epic-ne[single_sidebar_second]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Second Sidebar','jnews'),
    'description'   => wp_kses(__("Select Single sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[single_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
        ),
        array(
            'setting'  => 'epic-ne[single_layout]',
            'operator' => '!=',
            'value'    => 'build-your-own',
        ),
        array(
            'setting'  => 'epic-ne[single_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);



$options[] = array(
	'id'            => 'epic-ne[single_post_template_id]',
	'option_type'   => 'option',
	'transport'     => 'refresh',
	'default'       => '',
	'type'          => 'jeg-select',
	'label'         => esc_html__('Single Post Template List','Stories'),
	'description'   => wp_kses(sprintf(__('Create custom single post template from <a href="%s" target="_blank">here</a>','Stories'), get_admin_url() . 'edit.php?post_type=custom-post-template'), wp_kses_allowed_html()),
	'multiple'      => 1,
	'choices'       => call_user_func(function(){
		$post = get_posts(array(
			'posts_per_page'   => -1,
			'post_type'        => 'custom-post-template',
		));

		$template = array();
		$template[] = esc_html__('Choose Custom Template', 'Stories');

		if($post) {
			foreach($post as $value) {
				$template[$value->ID] = $value->post_title;
			}
		}

		return $template;
	}),
	'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[single_layout]',
            'operator' => '==',
            'value'    => 'build-your-own',
        ),	
        array(
            'setting'  => 'epic-ne[single_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
	),
	'postvar'       => array(
		array(
			'redirect'  => 'single_post_tag',
			'refresh'   => true
		)
	),
);


return $options;
