<?php

$options = array();

$all_sidebar = $GLOBALS['wp_registered_sidebars'];
foreach ( $all_sidebar as $sidebar ){
   $sidebar_options[$sidebar['id']] = $sidebar['name'];
}



$options[] = array(
	'id'            => 'epic-ne[site_container]',
	'option_type'   => 'option',
	'transport'     => 'postMessage',
	'default'       => '1360',
	'type'          => 'jeg-range-slider',
	'label'         => esc_html__('Site Width ', 'Stories'),
	'description'   => esc_html__('Set your Site MAX Width Size', 'Stories'),
	'choices'     => array(
		'min'       => '960',
		'max'       => '1440',
		'step'      => '10',
	),
	'output'     => array(
		array(
			'mediaquery'    => '@media only screen and (min-width : 1200px)',
			'method'        => 'inject-style',
			'element'       => '.stories_viewport .container, .container, .elementor-section.elementor-section-boxed > .elementor-container',
			'property'      => 'max-width',
			'units'         => 'px',
		)
	)
);




$options[] = array(
	'id'            => 'epic-ne[site_gutter]',
	'option_type'   => 'option',
	'transport'     => 'postMessage',
	'default'       => '15',
	'type'          => 'jeg-range-slider',
	'label'         => esc_html__('Space Between Columns', 'Stories'),
	'description'   => esc_html__('Set gutter. Default: 15px', 'Stories'),
	'choices'     => array(
		'min'       => '0',
		'max'       => '60',
		'step'      => '5',
	),
	'output'     => array(
		array(
			'mediaquery'    => '@media only screen and (min-width : 1200px)',
			'method'        => 'inject-style',
			'element'       => '.main-column.col-lg-1, .main-column.col-lg-10, .main-column.col-lg-11, .main-column.col-lg-12, .main-column.col-lg-2, .main-column.col-lg-3, .main-column.col-lg-4, .main-column.col-lg-5, .main-column.col-lg-6, .main-column.col-lg-7, .main-column.col-lg-8, .main-column.col-lg-9, .main-column.col-md-1, .main-column.col-md-10, .main-column.col-md-11, .main-column.col-md-12, .main-column.col-md-2, .main-column.col-md-3, .main-column.col-md-4, .main-column.col-md-5, .main-column.col-md-6, .main-column.col-md-7, .main-column.col-md-8, .main-column.col-md-9, .main-column.col-sm-1, .main-column.col-sm-10, .main-column.col-sm-11, .main-column.col-sm-12, .main-column.col-sm-2, .main-column.col-sm-3, .main-column.col-sm-4, .main-column.col-sm-5, .main-column.col-sm-6, .main-column.col-sm-7, .main-column.col-sm-8, .main-column.col-sm-9, .main-column.col-xs-1, .main-column.col-xs-10, .main-column.col-xs-11, .main-column.col-xs-12, .main-column.col-xs-2, .main-column.col-xs-3, .main-column.col-xs-4, .main-column.col-xs-5, .main-column.col-xs-6, .main-column.col-xs-7, .main-column.col-xs-8, .main-column.col-xs-9',
			'property'      => 'padding-left',
			'units'         => 'px',
        ),
		array(
			'mediaquery'    => '@media only screen and (min-width : 1200px)',
			'method'        => 'inject-style',
			'element'       => '.main-column.col-lg-1, .main-column.col-lg-10, .main-column.col-lg-11, .main-column.col-lg-12, .main-column.col-lg-2, .main-column.col-lg-3, .main-column.col-lg-4, .main-column.col-lg-5, .main-column.col-lg-6, .main-column.col-lg-7, .main-column.col-lg-8, .main-column.col-lg-9, .main-column.col-md-1, .main-column.col-md-10, .main-column.col-md-11, .main-column.col-md-12, .main-column.col-md-2, .main-column.col-md-3, .main-column.col-md-4, .main-column.col-md-5, .main-column.col-md-6, .main-column.col-md-7, .main-column.col-md-8, .main-column.col-md-9, .main-column.col-sm-1, .main-column.col-sm-10, .main-column.col-sm-11, .main-column.col-sm-12, .main-column.col-sm-2, .main-column.col-sm-3, .main-column.col-sm-4, .main-column.col-sm-5, .main-column.col-sm-6, .main-column.col-sm-7, .main-column.col-sm-8, .main-column.col-sm-9, .main-column.col-xs-1, .main-column.col-xs-10, .main-column.col-xs-11, .main-column.col-xs-12, .main-column.col-xs-2, .main-column.col-xs-3, .main-column.col-xs-4, .main-column.col-xs-5, .main-column.col-xs-6, .main-column.col-xs-7, .main-column.col-xs-8, .main-column.col-xs-9',
			'property'      => 'padding-right',
			'units'         => 'px',
		)
	)
);







$options[] = array(
	'id'            => 'epic-ne[site_padding]',
	'option_type'   => 'option',
	'transport'     => 'refresh',
	'default'       => '40',
	'type'          => 'jeg-range-slider',
	'label'         => esc_html__('Site Padding', 'Stories'),
	'description'   => esc_html__('Set padding. Default: 40px', 'Stories'),
	'choices'     => array(
		'min'       => '0',
		'max'       => '120',
		'step'      => '5',
	),
	'postvar'         => array(
		array(
			'redirect' => 'category_tag',
			'refresh'  => true
		)
	),
	'output'     => array(
		array(
			'mediaquery'    => '@media only screen and (min-width : 1200px)',
			'method'        => 'inject-style',
			'element'       => '.site',
			'property'      => 'padding-left',
			'units'         => 'px',
        ),
		array(
			'mediaquery'    => '@media only screen and (min-width : 1200px)',
			'method'        => 'inject-style',
			'element'       => '.site',
			'property'      => 'padding-right',
			'units'         => 'px',
        )
	)
);



$options[] = array(
    'id'            => 'epic-ne[site_sidebar]',
    'type'          => 'jeg-header',
    'label'         => esc_html__('Site Sidebar','Stories' ),
);

$options[] = array(
	'id'            => 'epic-ne[site_layout]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'right-sidebar',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('Index Page Layout','jnews' ),
    'description'   => esc_html__('Choose index page layout.','jnews' ),
    'choices'       => array( 
        'left-sidebar' => array (  
			// burak her zaman tum url escaping olcak.
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-left-sidebar.jpg')
        ),
        'right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-right-sidebar.jpg' )
        ),
        '2-left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-left-sidebar.jpg')
		),
        '2-right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-right-sidebar.jpg')
		),
        'both-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-both-sidebar.jpg')
        ),
        'no-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-no-sidebar.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'home_tag',
            'refresh'   => true
        )
    )
);

$options[] = array(
    'id'            => 'epic-ne[site_sidebar_first]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Site Sidebar','jnews'),
    'description'   => wp_kses(__("Select site sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[site_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
        )
    ),
);


$options[] = array(
    'id'            => 'epic-ne[site_sidebar_second]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Second Sidebar','jnews'),
    'description'   => wp_kses(__("Select site sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,

    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[site_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
        )
    ),
);





return $options;
