<?php

$options = array();

$options[] = array(
    'id'            => 'epic-ne[module_loader]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'dot',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Module Loader Style', 'Stories'),
    'description'   => esc_html__('Choose loader style for general module element.','Stories'),
    'choices'       => array(
        'dot'		    => esc_html__('Dot', 'Stories'),
        'circle'		=> esc_html__('Circle', 'Stories'),
        'square'		=> esc_html__('Square', 'Stories'),
    ),
    'output'     => array(
        array(
            'method'        => 'class-masking',
            'element'       => '.module-overlay .preloader_type',
            'property'      => array(
                'dot'           => 'preloader_dot',
                'circle'        => 'preloader_circle',
                'square'        => 'preloader_square',
            ),
        ),
    )
);

return $options;
