<?php

$options = array();

$options[] = array(
    'id'            => 'epic-ne[body_color]',
    'option_type' => 'option',
    'transport'     => 'postMessage',
    'type'          => 'jeg-color', 
    'default'       => '#333',
    'label'         => esc_html__('Body Color', 'Stories'),
    'description'   => esc_html__('Set main color.', 'Stories'),    
    'output'     => array(
        array(
            'method'        => 'inject-style',
            'element'       => 'body',
            'property'      => 'color',
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[main_color]',
    'option_type' => 'option',
    'transport'     => 'postMessage',
    'type'          => 'jeg-color', 
    'default'       => '#EF4B00',
    'label'         => esc_html__('Main Color', 'Stories'),
    'description'   => esc_html__('Set main color.', 'Stories'),    
    'output'     => array(
        array(
            'method'        => 'inject-style',
            'element'       => '.single .jeg_meta_category a',
            'property'      => 'color',
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[main_rrrrrcolor]',
	'transport'     => 'postMessage',
	'type'          => 'jeg-color',
	'default'       => '#000',
	'label'         => esc_html__('Entry Link Color', 'jnews'),
	'description'   => esc_html__('General text link color of page and post entry content.', 'jnews'),

);

$options[] = array(
    'id'            => 'epic-ne[sidebar_shadow]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Sidebar Shadow','Stories'),
    'description'   => esc_html__('Shadow for sidebar','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[sidebar_size]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Sidebar Size','Stories'),
    'description'   => esc_html__('f you want to use big sidebar, enable this.','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[sticky_sidebar]',
    'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Sticky Sidebar','Stories'),
    'description'   => esc_html__('If you want to use big sidebar, enable this.','Stories'),
);

$options[] = array(
	'id'            => 'epic-ne[widget_title_styles]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'stories_block_heading_6',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('Widget Title Styles','jnews' ),
    'choices'       => array( 
        'stories_block_heading_1' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-1-thumbnail.jpg')
        ),
        'stories_block_heading_2' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-2-thumbnail.jpg')
        ),
        'stories_block_heading_3' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-3-thumbnail.jpg')
        ),
        'stories_block_heading_4' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-4-thumbnail.jpg')
        ),
        'stories_block_heading_5' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-5-thumbnail.jpg')
        ),
        'stories_block_heading_6' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-6-thumbnail.jpg')
        ),
        'stories_block_heading_7' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-7-thumbnail.jpg')
        ),
        'stories_block_heading_8' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-8-thumbnail.jpg')
        ),
        'stories_block_heading_9' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-9-thumbnail.jpg')
        ),
        'stories_block_heading_10' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-10-thumbnail.jpg')
        ),
        'stories_block_heading_11' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-11-thumbnail.jpg')
        ),
        'stories_block_heading_12' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-12-thumbnail.jpg')
        ),
        'stories_block_heading_13' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-13-thumbnail.jpg')
        ),
        'stories_block_heading_14' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-14-thumbnail.jpg')
        ),
        'stories_block_heading_15' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/title-styles/widget-title-styles/title-style-15-thumbnail.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'home_tag',
            'refresh'   => true
        )
        ),
);

return $options;
