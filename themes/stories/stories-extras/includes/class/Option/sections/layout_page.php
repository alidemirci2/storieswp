<?php

$options = array();

$all_sidebar = $GLOBALS['wp_registered_sidebars'];
foreach ( $all_sidebar as $sidebar ){
   $sidebar_options[$sidebar['id']] = $sidebar['name'];
}

$options[] = array(
    'id'            => 'epic-ne[page_post_template_width_option]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Page Width','Stories'),
    'description'   => esc_html__('Set custom width','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[main_colwor]',
    'transport'     => 'postMessage',
    'type'          => 'jeg-color', 
    'default'       => '#EF4B00',
    'label'         => esc_html__('Main Color', 'Stories'),
    'description'   => esc_html__('Set main color.', 'Stories'),    
    'output'     => array(
        array(
            'method'        => 'inject-style',
            'element'       => 'body',
            'property'      => 'color',
        )
    ),
);


$options[] = array(
	'id'            => 'epic-ne[page_post_template_dekstop_container]',
	'option_type'   => 'option',
	'transport'     => 'postMessage',
	'default'       => '1170',
	'type'          => 'jeg-range-slider',
	'label'         => esc_html__('Page Width', 'Stories'),
	'description'   => esc_html__('Set your Page MAX Width Size', 'Stories'),
	'choices'     => array(
		'min'       => '960',
		'max'       => '1440',
		'step'      => '10',
	),
	'active_callback'  => array(
		array(
			'setting'  => 'epic-ne[page_post_template_width_option]',
			'operator' => '==',
			'value'    => true,
        )
	),
	'postvar'       => array(
		array(
			'redirect'  => 'page_post_tag',
			'refresh'   => true
		)
	),
	'output'     => array(
		array(
			'mediaquery'    => '@media only screen and (min-width : 1200px)',
			'method'        => 'inject-style',
			'element'       => '.single .elementor-section.elementor-section-boxed > .elementor-container, .stories_single_viewport .container',
			'property'      => 'max-width',
			'units'         => 'px',
		)
	)
);


$options[] = array(
    'id'            => 'epic-ne[page_sidebar_option]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => false,
    'type'          => 'jeg-toggle',
    'label'         => esc_html__('Custom Layout','Stories'),
    'description'   => esc_html__('Set custom layout/sidebar for Single','Stories'),
);

$options[] = array(
    'id'            => 'epic-ne[page_sidebar]',
    'type'          => 'jeg-header',
    'label'         => esc_html__('Page Sidebar','Stories' ),
     'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[page_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
	'id'            => 'epic-ne[page_layout]',
	'option_type'   => 'option',
    'transport'     => 'refresh',
    'default'       => 'right-sidebar',
    'type'          => 'jeg-radio-image',
    'label'         => esc_html__('Page Layout','jnews' ),
    'description'   => esc_html__('Choose page layout.','jnews' ),
    'choices'       => array( 
        'left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-left-sidebar.jpg')
        ),
        'right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-right-sidebar.jpg' )
        ),
        '2-left-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-left-sidebar.jpg')
		),
        '2-right-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-2-right-sidebar.jpg')
		),
        'both-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-both-sidebar.jpg')
        ),
        'no-sidebar' => array (  
            'img' => esc_url(EPIC_URL . '/assets/img/admin/layouts/layout-no-sidebar.jpg')
        ),
    ) ,
    'postvar'       => array(
        array(
            'redirect'  => 'page_post_tag',
            'refresh'   => true
        )
        ),
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[page_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);

$options[] = array(
    'id'            => 'epic-ne[page_sidebar_first]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Page Sidebar','jnews'),
    'description'   => wp_kses(__("Select page sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[page_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[page_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


$options[] = array(
    'id'            => 'epic-ne[page_sidebar_second]',
    'option_type'   => 'option',
    'transport'     => 'postMessage',
    'default'       => 'default-sidebar',
    'type'          => 'jeg-select',
    'label'         => esc_html__('Second Sidebar','jnews'),
    'description'   => wp_kses(__("Select Page sidebar. If you want to create a new, Install <a href=\"https://wordpress.org/plugins/custom-sidebars/\">Custom Sidebar</a> plugin ", 'jnews'), wp_kses_allowed_html()),
    'multiple'      => 1,
    'choices'       => $sidebar_options,
    'active_callback'  => array(
        array(
            'setting'  => 'epic-ne[page_layout]',
            'operator' => '!=',
            'value'    => 'no-sidebar',
		),
        array(
            'setting'  => 'epic-ne[page_sidebar_option]',
            'operator' => '==',
            'value'    => true,
        )
    ),
);


return $options;
