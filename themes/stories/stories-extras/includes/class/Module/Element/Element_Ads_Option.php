<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Element;

use EPIC\Module\ModuleOptionAbstract;

Class Element_Ads_Option extends ModuleOptionAbstract
{
    protected function get_ad_size()
    {
        return array(
            esc_attr__('Auto', 'Stories')              => 'auto',
            esc_attr__('Hide', 'Stories')              => 'hide',
            esc_attr__('120 x 90', 'Stories')          => '120x90',
            esc_attr__('120 x 240', 'Stories')         => '120x240',
            esc_attr__('120 x 600', 'Stories')         => '120x600',
            esc_attr__('125 x 125', 'Stories')         => '125x125',
            esc_attr__('160 x 90', 'Stories')          => '160x90',
            esc_attr__('160 x 600', 'Stories')         => '160x600',
            esc_attr__('180 x 90', 'Stories')          => '180x90',
            esc_attr__('180 x 150', 'Stories')         => '180x150',
            esc_attr__('200 x 90', 'Stories')          => '200x90',
            esc_attr__('200 x 200', 'Stories')         => '200x200',
            esc_attr__('234 x 60', 'Stories')          => '234x60',
            esc_attr__('250 x 250', 'Stories')         => '250x250',
            esc_attr__('320 x 100', 'Stories')         => '320x100',
            esc_attr__('300 x 250', 'Stories')         => '300x250',
            esc_attr__('300 x 600', 'Stories')         => '300x600',
            esc_attr__('320 x 50', 'Stories')          => '320x50',
            esc_attr__('336 x 280', 'Stories')         => '336x280',
            esc_attr__('468 x 15', 'Stories')          => '468x15',
            esc_attr__('468 x 60', 'Stories')          => '468x60',
            esc_attr__('728 x 15', 'Stories')          => '728x15',
            esc_attr__('728 x 90', 'Stories')          => '728x90',
            esc_attr__('970 x 90', 'Stories')          => '970x90',
            esc_attr__('240 x 400', 'Stories')         => '240x400',
            esc_attr__('250 x 360', 'Stories')         => '250x360',
            esc_attr__('580 x 400', 'Stories')         => '580x400',
            esc_attr__('750 x 100', 'Stories')         => '750x100',
            esc_attr__('750 x 200', 'Stories')         => '750x200',
            esc_attr__('750 x 300', 'Stories')         => '750x300',
            esc_attr__('980 x 120', 'Stories')         => '980x120',
            esc_attr__('930 x 180', 'Stories')         => '930x180',
        );
    }

	public function get_category()
	{
		return esc_html__('EPIC - Element', 'Stories');
	}

    public function compatible_column()
    {
        return array( 1,2,3,4,5,6,7,8,9,10,11,12 );
    }

    public function get_module_name()
    {
        return esc_html__('EPIC - Ads Block', 'Stories');
    }

    public function set_options()
    {
        $this->get_ads_option();
        $this->set_style_option();
    }

    public function get_ads_option()
    {
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'ads_type',
            'heading'       => esc_html__('Ads Type', 'Stories'),
            'description'   => esc_html__('Choose which ads type you want to use.', 'Stories'),
            'std'           => 'googleads',
            'value'         => array(
                esc_html__('Image Ads', 'Stories')     => 'image',
                esc_html__('Script Code', 'Stories')   => 'code',
                esc_html__('Google Ads', 'Stories')    => 'googleads',
            ),
        );
        // IMAGE
        $this->options[] = array(
            'type'          => 'attach_image',
            'param_name'    => 'ads_image',
            'heading'       => esc_html__('[Image Ads] Ads Image Desktop', 'Stories'),
            'description'   => esc_html__('Upload your ads image that will be shown on the desktop view.', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
	    $this->options[] = array(
		    'type'          => 'attach_image',
		    'param_name'    => 'ads_image_tablet',
		    'heading'       => esc_html__('[Image Ads] Ads Image Tablet', 'Stories'),
		    'description'   => esc_html__('Upload your ads image that will be shown on the tablet view.', 'Stories'),
		    'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
	    );
	    $this->options[] = array(
		    'type'          => 'attach_image',
		    'param_name'    => 'ads_image_phone',
		    'heading'       => esc_html__('[Image Ads] Ads Image Phone', 'Stories'),
		    'description'   => esc_html__('Upload your ads image that will be shown on the phone view.', 'Stories'),
		    'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
	    );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'ads_image_link',
            'heading'       => esc_html__('[Image Ads] Ads Image Link', 'Stories'),
            'description'   => esc_html__('Insert link of your image ads.','Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'ads_image_alt',
            'heading'       => esc_html__('[Image Ads] Image Alternate Text','Stories'),
            'description'   => esc_html__('Insert alternate of your ads image.','Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
        $this->options[] = array(
            'type'          => 'checkbox',
            'param_name'    => 'ads_image_new_tab',
            'heading'       => esc_html__('[Image Ads] Open New Tab','Stories'),
            'value'         => array( "Open in new tab when ads image clicked." => 'yes' ),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
        // GOOGLE
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'google_publisher_id',
            'heading'       => esc_html__('[Google Ads] Publisher ID','Stories'),
            'description'   => esc_html__('Insert data-ad-client / google_ad_client content.','Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads'))
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'google_slot_id',
            'heading'       => esc_html__('[Google Ads] Ads Slot ID','Stories'),
            'description'   => esc_html__('Insert data-ad-slot / google_ad_slot content.','Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads'))
        );
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'google_desktop',
            'heading'       => esc_html__('[Google Ads] Desktop Ads Size','Stories'),
            'description'   => esc_html__('Choose ads size to show on desktop.','Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads')),
            'std'           => 'auto',
            'value'         => $this->get_ad_size()
        );
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'google_tab',
            'heading'       => esc_html__('[Google Ads] Tab Ads Size','Stories'),
            'description'   => esc_html__('Choose ads size to show on tab.','Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads')),
            'std'           => 'auto',
            'value'         => $this->get_ad_size()
        );
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'google_phone',
            'heading'       => esc_html__('[Google Ads] Phone Ads Size','Stories'),
            'description'   => esc_html__('Choose ads size to show on phone.','Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads')),
            'std'           => 'auto',
            'value'         => $this->get_ad_size()
        );
        // CODE
        $this->options[] = array(
            'type'          => 'textarea_html',
            'param_name'    => 'content',
            'heading'       => esc_html__('[Script Code] Ads Code','Stories'),
            'description'   => esc_html__('Put your full ads script right here.','Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('code'))
        );
        $this->options[] = array(
            'type'          => 'checkbox',
            'param_name'    => 'ads_bottom_text',
            'heading'       => esc_html__('Show Advertisement Text', 'Stories'),
            'description'   => esc_html__('Show Advertisement Text on bottom of advertisement', 'Stories'),
        );
    }
}
