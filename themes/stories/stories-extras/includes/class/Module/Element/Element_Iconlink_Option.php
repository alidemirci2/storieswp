<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Element;

use EPIC\Module\ModuleOptionAbstract;

Class Element_Iconlink_Option extends ModuleOptionAbstract
{
    public function compatible_column()
    {
        return array(1,2,3,4,5,6,7,8,9,10,11,12);
    }

    public function get_module_name()
    {
        return esc_html__('EPIC - Icon Link', 'Stories');
    }

    public function get_category()
    {
	    return esc_html__('EPIC - Element', 'Stories');
    }

	public function set_options()
    {
        $this->set_icon_option();
        $this->set_style_option();
    }

    public function set_icon_option()
    {
        $this->options[] = array(
            'type'          => 'iconpicker',
            'param_name'    => 'icon',
            'heading'       => esc_html__('Icon', 'Stories'),
            'description'   => esc_html__('Choose icon for this icon link', 'Stories'),
            'std'         => 'fa fa-bolt',
            'settings'      => array(
                'emptyIcon'     => false,
                'iconsPerPage'  => 100,
            )
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'title',
            'heading'       => esc_html__('Title', 'Stories'),
            'description'   => esc_html__('Insert a text for block link title.', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'subtitle',
            'heading'       => esc_html__('Subtitle', 'Stories'),
            'description'   => esc_html__('Sub title or short description.', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'title_url',
            'heading'       => esc_html__('Title URL', 'Stories'),
            'description'   => esc_html__('Url of block link title.', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'checkbox',
            'param_name'    => 'newtab',
            'heading'       => esc_html__('Open New Tab', 'Stories'),
            'description'   => esc_html__('Check this option to open link on new tab.', 'Stories'),
        );
    }

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'title_typography',
				'label'       => __( 'Title Typography', 'Stories' ),
				'description' => __( 'Set typography for title', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_block_icon_title h3',
			]
		);

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'meta_typography',
				'label'       => __( 'Second Title Typography', 'Stories' ),
				'description' => __( 'Set typography for second title', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_block_icon_desc_span span',
			]
		);
	}
}
