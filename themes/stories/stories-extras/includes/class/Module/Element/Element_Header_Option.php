<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Element;

use EPIC\Module\ModuleOptionAbstract;

Class Element_Header_Option extends ModuleOptionAbstract
{
    public function compatible_column()
    {
        return array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
    }

    public function get_module_name()
    {
        return esc_html__('EPIC - Header Module', 'Stories');
    }

    public function get_category()
    {
	    return esc_html__('EPIC - Element', 'Stories');
    }

	public function set_options()
    {
        $this->set_header_option();
        $this->set_style_option();
    }

    public function set_header_option()
    {
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'first_title',
            'holder'        => 'span',
            'heading'       => esc_html__('Title', 'Stories'),
            'description'   => esc_html__('Main title of Module Block.', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'second_title',
            'holder'        => 'span',
            'heading'       => esc_html__('Second Title', 'Stories'),
            'description'   => esc_html__('Secondary title of Module Block.', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'url',
            'heading'       => esc_html__('Title URL', 'Stories'),
            'description'   => esc_html__('Insert URL of heading title.', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'radioimage',
            'param_name'    => 'header_type',
            'std'           => 'heading_1',
            'value'         => array(
                EPIC_URL . '/assets/img/admin/heading-1.png'  => 'heading_1',
                EPIC_URL . '/assets/img/admin/heading-2.png'  => 'heading_2',
                EPIC_URL . '/assets/img/admin/heading-3.png'  => 'heading_3',
                EPIC_URL . '/assets/img/admin/heading-4.png'  => 'heading_4',
                EPIC_URL . '/assets/img/admin/heading-5.png'  => 'heading_5',
                EPIC_URL . '/assets/img/admin/heading-6.png'  => 'heading_6',
                EPIC_URL . '/assets/img/admin/heading-7.png'  => 'heading_7',
                EPIC_URL . '/assets/img/admin/heading-8.png'  => 'heading_8',
                EPIC_URL . '/assets/img/admin/heading-9.png'  => 'heading_9',
            ),
            'heading'       => esc_html__('Header Type', 'Stories'),
            'description'   => esc_html__('Choose which header type fit with your content design.', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'header_align',
            'heading'       => esc_html__('Header Align', 'Stories'),
            'description'   => esc_html__('Choose which header alignment you want to use.', 'Stories'),
            'std'           => 'left',
            'value'         => array(
                esc_html__('Left', 'Stories')              => 'left',
                esc_html__('Center', 'Stories')            => 'center',
            )
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_background',
            'heading'       => esc_html__('Header Background', 'Stories'),
            'description'   => esc_html__('This option may not work for all of heading type.', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_2', 'heading_3', 'heading_4', 'heading_5')),
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_secondary_background',
            'heading'       => esc_html__('Header Secondary Background', 'Stories'),
            'description'   => esc_html__('change secondary background', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_2'))
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_text_color',
            'heading'       => esc_html__('Header Text Color', 'Stories'),
            'description'   => esc_html__('Change color of your header text', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_line_color',
            'heading'       => esc_html__('Header line Color', 'Stories'),
            'description'   => esc_html__('Change line color of your header', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_5', 'heading_6', 'heading_9'))
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_accent_color',
            'heading'       => esc_html__('Header Accent', 'Stories'),
            'description'   => esc_html__('Change Accent of your header', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_6', 'heading_7'))
        );
    }

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'title_typography',
				'label'       => __( 'Title Typography', 'Stories' ),
				'description' => __( 'Set typography for post title', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_block_title span,{{WRAPPER}} .jeg_block_title strong',
			]
		);
	}
}
