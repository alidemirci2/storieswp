<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Block;

Class Block_15_Option extends BlockOptionAbstract
{
    protected $default_number_post = 5;


    protected $show_meta_categories = false;
    protected $show_meta_reading_time = false;

    protected $show_meta_author = 'yes';
    protected $show_meta_date = 'yes'; 
    protected $show_meta_comments = false;

    protected $show_meta_share = 'yes';
    protected $small_share = 'yes';

    protected $show_excerpt = false;



    protected $show_ads = true;
    protected $default_ajax_post = 5;






    

    public function get_module_name()
    {
        return esc_html__('EPIC - Module 15', 'Stories');
    }

    public function compatible_column()
    {
        // Kullanıcı Hangi Kolon Yapılarını Seçebiliri belirliyoruz. BURAK*
        return array(  3, 4, 5 );
    }
    public function additional_style()
    {
        parent::additional_style();

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'block_background',
            'group'         => esc_html__('Design', 'Stories'),
            'heading'       => esc_html__('Block Background', 'Stories'),
            'description'   => esc_html__('This option will change your Block Background', 'Stories'),
        ); 
    }
}
