<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Block;

use EPIC\Module\ModuleOptionAbstract;
use Jeg\Customizer\Customizer;


abstract Class BlockOptionAbstract extends ModuleOptionAbstract
{
    protected $default_number_post = 5;
    public $ali = 4;
    protected $show_meta_categories = 'yes';
    protected $show_meta_reading_time = false;

    protected $show_meta_author = 'yes';
    protected $show_meta_date = 'yes'; 
    protected $show_meta_comments = 'yes';

    protected $show_meta_share = 'yes';
    protected $small_share = false;

    protected $show_excerpt = false;

    protected $show_ads = false;
    protected $show_meta_view = false;
    protected $show_read_more = false;

     
    

    
    protected $show_post_format = true;
    protected $show_meta_rating = true;
    protected $default_ajax_post = 5;

    public function compatible_column()
    {
        return array( 4, 8 , 12 );
    }

    public function set_options()
    {
        $this->set_header_option();
        $this->set_header_style_option();
        $this->set_header_filter_option();
        $this->set_header_filter_style_option();
        $this->set_content_filter_option($this->default_number_post);
        $this->set_ajax_filter_option($this->default_ajax_post);
        $this->set_ads_setting_option($this->show_ads);
        $this->set_style_option();
        $this->set_content_style_option($this->$ali);
        $this->set_meta_setting_option(
            $this->show_excerpt,
            $this->show_meta_author, 
            $this->show_meta_date, 
            $this->show_meta_comments, 
            $this->show_meta_categories, 
            $this->show_meta_reading_time, 
            $this->show_meta_share, 
            $this->small_share, 
            $this->show_meta_view, 
            $this->show_read_more, 
            $this->show_post_format, 
            $this->show_meta_rating);
    }

    protected function get_ad_size()
    {
        return array(
            esc_attr__('Auto', 'Stories')              => 'auto',
            esc_attr__('Hide', 'Stories')              => 'hide',
            esc_attr__('120 x 90', 'Stories')          => '120x90',
            esc_attr__('120 x 240', 'Stories')         => '120x240',
            esc_attr__('120 x 600', 'Stories')         => '120x600',
            esc_attr__('125 x 125', 'Stories')         => '125x125',
            esc_attr__('160 x 90', 'Stories')          => '160x90',
            esc_attr__('160 x 600', 'Stories')         => '160x600',
            esc_attr__('180 x 90', 'Stories')          => '180x90',
            esc_attr__('180 x 150', 'Stories')         => '180x150',
            esc_attr__('200 x 90', 'Stories')          => '200x90',
            esc_attr__('200 x 200', 'Stories')         => '200x200',
            esc_attr__('234 x 60', 'Stories')          => '234x60',
            esc_attr__('250 x 250', 'Stories')         => '250x250',
            esc_attr__('320 x 100', 'Stories')         => '320x100',
            esc_attr__('300 x 250', 'Stories')         => '300x250',
            esc_attr__('300 x 600', 'Stories')         => '300x600',
            esc_attr__('320 x 50', 'Stories')          => '320x50',
            esc_attr__('336 x 280', 'Stories')         => '336x280',
            esc_attr__('468 x 15', 'Stories')          => '468x15',
            esc_attr__('468 x 60', 'Stories')          => '468x60',
            esc_attr__('728 x 15', 'Stories')          => '728x15',
            esc_attr__('728 x 90', 'Stories')          => '728x90',
            esc_attr__('970 x 90', 'Stories')          => '970x90',
            esc_attr__('240 x 400', 'Stories')         => '240x400',
            esc_attr__('250 x 360', 'Stories')         => '250x360',
            esc_attr__('580 x 400', 'Stories')         => '580x400',
            esc_attr__('750 x 100', 'Stories')         => '750x100',
            esc_attr__('750 x 200', 'Stories')         => '750x200',
            esc_attr__('750 x 300', 'Stories')         => '750x300',
            esc_attr__('980 x 120', 'Stories')         => '980x120',
            esc_attr__('930 x 180', 'Stories')         => '930x180',
        );
    }

   


    /**
     * @return array
     */
    public function set_header_option()
    {
        $this->options[] = array(
            'type'          => 'iconpicker',
            'param_name'    => 'header_icon',
            'heading'       => esc_html__('Header Icon', 'Stories'),
            'description'   => esc_html__('Choose icon for this block icon.', 'Stories'),
            'group'         => esc_html__('Module Header', 'Stories'),
            'std'         => '',
            'settings'      => array(
                'emptyIcon'     => true,
                'iconsPerPage'  => 100,
            )
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'first_title',
            'holder'        => 'span',
            'heading'       => esc_html__('Title', 'Stories'),
            'description'   => esc_html__('Main title of Module Block.', 'Stories'),
            'group'         => esc_html__('Module Header', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'second_title',
            'holder'        => 'span',
            'heading'       => esc_html__('Second Title', 'Stories'),
            'description'   => esc_html__('Secondary title of Module Block.', 'Stories'),
            'group'         => esc_html__('Module Header', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'third_title',
            'holder'        => 'span',
            'heading'       => esc_html__('Third Title', 'Stories'),
            'description'   => esc_html__('Third title of Module Block.', 'Stories'),
            'group'         => esc_html__('Module Header', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_2', 'heading_3', 'heading_5', 'heading_6', 'heading_7', 'heading_8', 'heading_9','heading_10','heading_11','heading_12','heading_13','heading_14'))
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'url',
            'heading'       => esc_html__('Title URL', 'Stories'),
            'description'   => esc_html__('Insert URL of heading title.', 'Stories'),
            'group'         => esc_html__('Module Header', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'radioimage',
            'param_name'    => 'header_type',
            'std'           => 'heading_6',
            'value'         => array(
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-1-thumbnail.jpg')  => 'heading_1',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-2-thumbnail.jpg')  => 'heading_2',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-3-thumbnail.jpg')  => 'heading_3',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-4-thumbnail.jpg')  => 'heading_4',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-5-thumbnail.jpg')  => 'heading_5',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-6-thumbnail.jpg')  => 'heading_6',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-7-thumbnail.jpg')  => 'heading_7',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-8-thumbnail.jpg')  => 'heading_8',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-9-thumbnail.jpg')  => 'heading_9',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-10-thumbnail.jpg')  => 'heading_10',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-11-thumbnail.jpg')  => 'heading_11',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-12-thumbnail.jpg')  => 'heading_12',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-13-thumbnail.jpg')  => 'heading_13',
                esc_url(EPIC_URL . '/assets/img/admin/title-styles/title-style-14-thumbnail.jpg')  => 'heading_14',
            ),
            'heading'       => esc_html__('Header Type', 'Stories'),
            'description'   => esc_html__('Choose which header type fit with your content design.', 'Stories'),
            'group'         => esc_html__('Module Header', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_background',
            'heading'       => esc_html__('Header Title Background', 'Stories'),
            'description'   => esc_html__('This option may not work for all of heading type.', 'Stories'),
            'group'         => esc_html__('Module Header', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_2', 'heading_3', 'heading_4', 'heading_5'))
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_secondary_background',
            'heading'       => esc_html__('Header Title Secondary Background', 'Stories'),
            'description'   => esc_html__('change secondary background', 'Stories'),
            'group'         => esc_html__('Module Header', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_2'))
        );
    }

    /**
     * @return array
     */
    public function set_header_filter_option()
    {
        $this->options[] = array(
            'type'          => 'select',
            'multiple'      => PHP_INT_MAX,
            'ajax'          => 'epic_find_category',
            'options'       => 'epic_get_category_option',
            'nonce'         => wp_create_nonce('epic_find_category'),
            'param_name'    => 'header_filter_category',
            'heading'       => esc_html__('Category', 'Stories'),
            'description'   => esc_html__('Add category filter for heading module.', 'Stories'),
            'group'         => esc_html__('Header Navigation', 'Stories'),
            'std'           => '',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_2', 'heading_3', 'heading_5', 'heading_6', 'heading_7', 'heading_8', 'heading_9','heading_10','heading_11','heading_12','heading_13','heading_14','heading_14'))
        );
        $this->options[] = array(
	        'type'          => 'select',
	        'multiple'      => PHP_INT_MAX,
	        'ajax'          => 'epic_find_author',
	        'options'       => 'epic_get_author_option',
	        'nonce'         => wp_create_nonce('epic_find_author'),
            'param_name'    => 'header_filter_author',
            'heading'       => esc_html__('Author', 'Stories'),
            'description'   => esc_html__('Add author filter for heading module.', 'Stories'),
            'group'         => esc_html__('Header Navigation', 'Stories'),
            'std'           => '',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_2', 'heading_3', 'heading_5', 'heading_6', 'heading_7', 'heading_8', 'heading_9','heading_10','heading_11','heading_12','heading_13','heading_14','heading_14'))
        );

       

        $this->options[] = array(
	        'type'          => 'select',
	        'multiple'      => PHP_INT_MAX,
	        'ajax'          => 'epic_find_tag',
	        'options'       => 'epic_get_tag_option',
	        'nonce'         => wp_create_nonce('epic_find_tag'),
            'param_name'    => 'header_filter_tag',
            'heading'       => esc_html__('Tags', 'Stories'),
            'description'   => esc_html__('Add tag filter for heading module.', 'Stories'),
            'group'         => esc_html__('Header Navigation', 'Stories'),
            'std'           => '',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_2', 'heading_3', 'heading_5', 'heading_6', 'heading_7', 'heading_8', 'heading_9','heading_10','heading_11','heading_12','heading_13','heading_14'))
        );

        
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'header_filter_text',
            'heading'       => esc_html__('Default Text', 'Stories'),
            'description'   => esc_html__('First item text on heading filter.', 'Stories'),
            'group'         => esc_html__('Header Navigation', 'Stories'),
            'std'           => esc_html__('All','Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_2', 'heading_3', 'heading_5', 'heading_6', 'heading_7', 'heading_8', 'heading_9','heading_10','heading_11','heading_12','heading_13','heading_14'))
        );
         //!Improve TR: Heading 4 seçildiğinde notice ekle.
    }




    public function set_ajax_filter_option($number = 10)
    {
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'pagination_mode',
            'heading'       => esc_html__('Choose Pagination Mode', 'Stories'),
            'description'   => esc_html__('Choose which pagination mode that fit with your block.', 'Stories'),
            'group'         => esc_html__('Pagination', 'Stories'),
            'std'           => 'disable',
            'value'         => array(
                esc_html__('No Pagination', 'Stories')             => 'disable',
                esc_html__('Next Prev', 'Stories')                 => 'nextprev',
                esc_html__('Load More', 'Stories')                 => 'loadmore',
                esc_html__('Auto Load on Scroll', 'Stories')       => 'scrollload',
            )
        );
        $this->options[] = array(
            'type'          => 'checkbox',
            'param_name'    => 'pagination_nextprev_showtext',
            'heading'       => esc_html__('Show Navigation Text', 'Stories'),
            'value'         => array( esc_html__('Show Next/Prev text in the navigation controls.', 'Stories') => 'no' ),
            'group'         => esc_html__('Pagination', 'Stories'),
            'dependency'    => array('element' => "pagination_mode", 'value' => array('nextprev'))
        );
        $this->options[] = array(
            'type'          => 'slider',
            'param_name'    => 'pagination_number_post',
            'heading'       => esc_html__('Pagination Post', 'Stories'),
            'description'   => esc_html__('Number of Post loaded during pagination request.', 'Stories'),
            'group'         => esc_html__('Pagination', 'Stories'),
            'min'           => 1,
            'max'           => 30,
            'step'          => 1,
            'std'           => $number,
            'dependency'    => array('element' => "pagination_mode", 'value' => array('nextprev','loadmore','scrollload'))
        );
        $this->options[] = array(
            'type'          => 'number',
            'param_name'    => 'pagination_scroll_limit',
            'heading'       => esc_html__('Auto Load Limit', 'Stories'),
            'description'   => esc_html__('Limit of auto load when scrolling, set to zero to always load until end of content.', 'Stories'),
            'group'         => esc_html__('Pagination', 'Stories'),
            'min'           => 0,
            'max'           => 9999,
            'step'          => 1,
            'std'           => 0,
            'dependency'    => array('element' => "pagination_mode", 'value' => array('scrollload'))
        );
    }

    public function set_ads_setting_option( $show_ads = false )
    {
        if ( ! $show_ads ) return false;

        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'ads_type',
            'heading'       => esc_html__('Ads Type', 'Stories'),
            'description'   => esc_html__('Choose which ads type you want to use.', 'Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'std'           => 'disable',
            'value'         => array(
                esc_html__('Disable Ads', 'Stories')   => 'disable',
                esc_html__('Image Ads', 'Stories')     => 'image',
                esc_html__('Google Ads', 'Stories')    => 'googleads',
                esc_html__('Script Code', 'Stories')   => 'code'
            )
        );
        $this->options[] = array(
            'type'          => 'slider',
            'param_name'    => 'ads_position',
            'heading'       => esc_html__('Ads Position', 'Stories'),
            'description'   => esc_html__('Set after certain number of post you want this advertisement to show.', 'Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image', 'code', 'googleads')),
            'min'           => 1,
            'max'           => 10,
            'step'          => 1,
            'std'           => 1,
        );
        $this->options[] = array(
            'type'          => 'checkbox',
            'param_name'    => 'ads_random',
            'heading'       => esc_html__('Random Ads Position', 'Stories'),
            'value'         => array( esc_html__("Set after random certain number of post you want this advertisement to show.", 'Stories') => 'yes' ),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image', 'code', 'googleads'))
        );
        // IMAGE
        $this->options[] = array(
            'type'          => 'attach_image',
            'param_name'    => 'ads_image',
            'heading'       => esc_html__('Ads Image', 'Stories'),
            'description'   => esc_html__('Upload your ads image.', 'Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
	    $this->options[] = array(
		    'type'          => 'attach_image',
		    'param_name'    => 'ads_image_tablet',
		    'heading'       => esc_html__('Ads Image Tablet', 'Stories'),
		    'description'   => esc_html__('Upload your ads image that will be shown on the tablet view.', 'Stories'),
		    'group'         => esc_html__('Ads', 'Stories'),
		    'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
	    );
	    $this->options[] = array(
		    'type'          => 'attach_image',
		    'param_name'    => 'ads_image_phone',
		    'heading'       => esc_html__('Ads Image Phone', 'Stories'),
		    'description'   => esc_html__('Upload your ads image that will be shown on the phone view.', 'Stories'),
		    'group'         => esc_html__('Ads', 'Stories'),
		    'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
	    );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'ads_image_link',
            'heading'       => esc_html__('Ads Image Link', 'Stories'),
            'description'   => esc_html__('Insert link of your image ads.','Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'ads_image_alt',
            'heading'       => esc_html__('Image Alternate Text','Stories'),
            'description'   => esc_html__('Insert alternate of your ads image.','Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
        $this->options[] = array(
            'type'          => 'checkbox',
            'param_name'    => 'ads_image_new_tab',
            'heading'       => esc_html__('Open New Tab','Stories'),
            'value'         => array( esc_html__("Open in new tab when ads image clicked.", 'Stories') => 'yes' ),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
        // GOOGLE
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'google_publisher_id',
            'heading'       => esc_html__('Publisher ID','Stories'),
            'description'   => esc_html__('Insert data-ad-client / google_ad_client content.','Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads'))
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'google_slot_id',
            'heading'       => esc_html__('Ads Slot ID','Stories'),
            'description'   => esc_html__('Insert data-ad-slot / google_ad_slot content.','Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads'))
        );
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'google_desktop',
            'heading'       => esc_html__('Desktop Ads Size','Stories'),
            'description'   => esc_html__('Choose ads size to show on desktop.','Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads')),
            'std'           => 'auto',
            'value'         => $this->get_ad_size()
        );
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'google_tab',
            'heading'       => esc_html__('Tab Ads Size','Stories'),
            'description'   => esc_html__('Choose ads size to show on tab.','Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads')),
            'std'           => 'auto',
            'value'         => $this->get_ad_size()
        );
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'google_phone',
            'heading'       => esc_html__('Phone Ads Size','Stories'),
            'description'   => esc_html__('Choose ads size to show on phone.','Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('googleads')),
            'std'           => 'auto',
            'value'         => $this->get_ad_size()
        );
        // CODE
        $this->options[] = array(
            'type'          => 'textarea_html',
            'param_name'    => 'content',
            'heading'       => esc_html__('Script Ads Code','Stories'),
            'description'   => esc_html__('Put your full ads script right here.','Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('code'))
        );
        $this->options[] = array(
            'type'          => 'checkbox',
            'param_name'    => 'ads_bottom_text',
            'heading'       => esc_html__('Show Advertisement Text', 'Stories'),
            'description'   => esc_html__('Show Advertisement Text on bottom of advertisement', 'Stories'),
            'group'         => esc_html__('Ads', 'Stories'),
            'dependency'    => Array('element' => "ads_type", 'value' => array('image'))
        );
    }

    protected function set_boxed_option()
    {
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'boxed',
		    'group'         => esc_html__('Design', 'Stories'),
		    'heading'       => esc_html__('Enable Boxed', 'Stories'),
		    'value'         => array( esc_html__("This option will turn the module into boxed.", 'Stories') => 'true' ),
	    );

	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'boxed_shadow',
		    'group'         => esc_html__('Design', 'Stories'),
		    'heading'       => esc_html__('Enable Shadow', 'Stories'),
		    'dependency'    => array('element' => "boxed", 'value' => 'true')
	    );
    }

    public function set_typography_option( $instance ) {
        

	    $instance->add_control(
            'meta1',
            [
				'label' => __( 'Content Typography', 'Stories' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
			 
		);
	  
	    $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'post_title_typography',
			    'label'       => __( 'Title', 'Stories' ),
			    'description' => __( 'Set typography for post title', 'Stories' ),
			    'selector'    => '{{WRAPPER}} .jeg_post_title a',
		    ]
        );
        
	  

	    $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'content_typography',
			    'label'       => __( 'Content (Excerpt)', 'Stories' ),
			    'description' => __( 'Set typography for post content', 'Stories' ),
			    'selector'    => '{{WRAPPER}} .jeg_post_excerpt, {{WRAPPER}} .jeg_readmore',
		    ]
        );
   

        $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'meta_category_typography',
			    'label'       => __( 'Category', 'Stories' ),
			    'description' => __( 'Set typography for post meta category', 'Stories' ),
			    'selector'    => '  {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content  .jeg_post_category .jeg_post_cat a',
		    ]
        );


	     
	    $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'meta_author_text_typography',
			    'label'       => __( 'Meta Bottom (Author, Date…)', 'Stories' ),
			    'description' => __( 'Set typography for post meta author', 'Stories' ),
                'selector'    => ' 
                 {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content .jeg_post_meta a, 
                 {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content  .jeg_post_meta a .icon',
		    ]
        );
        
    
        
	    $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'meta_view_typography',
			    'label'       => __( 'View & Rating', 'Stories' ),
			    'description' => __( 'Set typography for post meta view and rating', 'Stories' ),
			    'selector'    => '  {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_thumb .meta_top_left .jeg_post_rating, {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_thumb .meta_top_left .jeg_post_view',
		    ]
        ); 
   
	    $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'meta_readmore_typography',
			    'label'       => __( 'Read More', 'Stories' ),
			    'description' => __( 'Set typography for post read more button', 'Stories' ),
			    'selector'    => '  {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content .jeg_post_excerpt .jeg_readmore',
		    ]
        );

	    $instance->add_control(
            'meta7',
            [
				'label' => __( 'Content Spacing', 'Stories' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
			 
		);


        $instance->add_control(
            'meta_title_padding', 
            [
                'label' => __( 'Title', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    ' {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content  .jeg_post_title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );


        $instance->add_control(
            'meta_excerpt_padding', 
            [
                'label' => __( 'Content(Excerpt)', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    ' {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content .jeg_post_excerpt p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );


        $instance->add_control(
            'meta_category_padding', 
            [
                'label' => __( 'Category', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    ' {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content  .jeg_post_category .jeg_post_cat' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );


        $instance->add_control(
            'meta_readmore_padding',
            [
                'label' => __( 'Read More', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content .jeg_post_excerpt .jeg_readmore' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]    
        );
    $instance->add_control( 
            'meta_author_padding',
             [
                'label' => __( 'Bottom Meta (Author - Date…)', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    ' {{WRAPPER}}  .jeg_posts .jeg_post .jeg_inner_post .jeg_postblock_content .jeg_post_meta' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        


    }

    public function set_header_style_options( $instance ) {
        


        $instance->add_control(
            'heading_typo',
            [
				'label' => __( 'Typography - Title', 'Stories' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
			 
		);



	    $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'title_typography',
			    'label'       => __( 'Title Typography', 'Stories' ),
			    'description' => __( 'Set typography for title', 'Stories' ),
                'selector'    => '{{WRAPPER}} .jeg_block_title span',
		    ]
        );
        

        $instance->add_responsive_control(
            'title_margin',
            [
				'label' => __( 'Title Margin', 'Stories' ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .stories_block_heading .jeg_block_title, {{WRAPPER}} .stories_block_heading_12 .third_title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
			 
		);
        
        $instance->add_responsive_control(
            'title_padding',
            [
				'label' => __( 'Title Padding', 'Stories' ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .stories_block_heading  .jeg_block_title span, {{WRAPPER}} .stories_block_heading_12 .third_title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
			 
        );
        
        $instance->add_responsive_control(
            'title_border',
            [
				'label' => __( 'Title Border', 'Stories' ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .stories_block_heading  .jeg_block_title span' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important; border-style:solid;',
				],
			]
			 
        );
        $instance->add_control(
			'title_border_color',
			[
				'label' => __( 'Title Border Color', 'Stories' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
                ],
                'default' => '#333333',
				'selectors' => [
					'{{WRAPPER}} .stories_block_heading  .jeg_block_title span' => 'border-color: {{VALUE}}',
                ],
                'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_2'))
			]
        );
        


        $instance->add_control(
            'heading_typo2',
            [
				'label' => __( 'Typo - Second Title', 'Stories' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
			 
		);

        $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'second_title_typography',
			    'label'       => __( 'Second Title Typography', 'Stories' ),
			    'description' => __( 'Set typography for Second title', 'Stories' ),
                'selector'    => '{{WRAPPER}} .jeg_block_title span strong',
		    ]
        );

        $instance->add_control(
			'second_title_color',
			[
				'label'       => __( 'Second Title Color', 'Stories' ),
			    'description' => __( 'Set color for Second title', 'Stories' ),
				'type' => \Elementor\Controls_Manager::COLOR, 
                'selectors' => [
					'{{WRAPPER}} .jeg_block_title span strong' => 'color: {{VALUE}} !important;',
				],
			]
		);
 
        
        $instance->add_control(
            'heading_typo3',
            [
				'label' => __( 'Typo - Third Title', 'Stories' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
			 
		);
        
        $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'third_title_typography',
			    'label'       => __( 'Third Title Typography', 'Stories' ),
			    'description' => __( 'Set typography for Third title', 'Stories' ),
                'selector'    => '{{WRAPPER}} .main-header .third_title',
		    ]
        );

        $instance->add_control(
			'third_title_color',
			[
				'label'       => __( 'Third Title Color', 'Stories' ),
			    'description' => __( 'Set color for Third title', 'Stories' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#666666',
                'selectors' => [
					'{{WRAPPER}} .main-header .third_title' => 'color: {{VALUE}} !important;',
				],
			]
		);
	    
    }
    protected function set_header_style_option()
    { 

        $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'header_set_1',
            'description'   => esc_html__('Header Line', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'), 
            'std' =>'info',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_5'))
        );

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_middle_border_color',
            'heading'       => esc_html__('Header Line Color', 'Stories'),
            'description'   => esc_html__('Change line color for header right line', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_5'))
        );
 
        $this->options[] = array(
            'type'          => 'number',
            'param_name'    => 'header_middle_border_height',
            'heading'       => esc_html__('Header Line Height', 'Stories'),
            'description'   => esc_html__('Change line height  for header right line', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_5'))
        ); 


        $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'header_set_2',
            'description'   => esc_html__('Border Top', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'), 
            'std' =>'info',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_2','heading_3','heading_4', 'heading_5', 'heading_6','heading_7', 'heading_9','heading_13','heading_14'))
        );

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_line_top_color',
            'heading'       => esc_html__('Header Border Top Color', 'Stories'),
            'description'   => esc_html__('Change border top color of your header', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_2','heading_3','heading_4', 'heading_5', 'heading_6','heading_7', 'heading_9','heading_13','heading_14'))
        );
 
        $this->options[] = array(
            'type'          => 'number',
            'param_name'    => 'header_border_top',
            'heading'       => esc_html__('Header Border Top', 'Stories'),
            'description'   => esc_html__('Change Top line height of your header', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_2','heading_3','heading_4',  'heading_5', 'heading_6','heading_7', 'heading_9','heading_13','heading_14'))
        );

        $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'header_set_3',
            'description'   => esc_html__('Border Bottom', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'), 
            'std' =>'info',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_2','heading_3','heading_4', 'heading_5', 'heading_6','heading_7', 'heading_9','heading_13','heading_14'))
        );

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_line_bottom_color',
            'heading'       => esc_html__('Header Border bottom Color', 'Stories'),
            'description'   => esc_html__('Change bottom border color of your header', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_5', 'heading_6','heading_7', 'heading_9','heading_13','heading_14'))
        );

        $this->options[] = array(
            'type'          => 'number',
            'param_name'    => 'header_border_bottom',
            'heading'       => esc_html__('Header Border Bottom (px)', 'Stories'),
            'description'   => esc_html__('Change bottom border height of your header', 'Stories'),
            'min'           => '0',
            'max'           => '21',
            'group'         => esc_html__('Header Style', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_1', 'heading_5', 'heading_6','heading_7', 'heading_9','heading_13','heading_14'))
        ); 

        $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'header_set_4',
            'description'   => esc_html__('Border Bottom Main Color', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'), 
            'std' =>'info',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_6', 'heading_7'))
        );
        
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_accent_color',
            'heading'       => esc_html__('Header Title Border Main Color', 'Stories'),
            'description'   => esc_html__('Change Accent of your header', 'Stories'),
            'group'         => esc_html__('Header Style', 'Stories'),
            'dependency'    => array('element' => "header_type", 'value' => array('heading_6', 'heading_7'))
        );
    }
    protected function set_header_filter_style_option()
    { 

        $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'header_set_5',
            'description'   => esc_html__('Normal', 'Stories'),
            'group'         => esc_html__('Header Navigation Styles', 'Stories'), 
            'std' =>'info',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_6', 'heading_7'))
        );

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_filter_item_color',
            'heading'       => esc_html__('Nav Item Color', 'Stories'),
            'group'         => esc_html__('Header Navigation Styles', 'Stories') 
        );  
        $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'header_set_6',
            'description'   => esc_html__('Hover', 'Stories'),
            'group'         => esc_html__('Header Navigation Styles', 'Stories'), 
            'std' =>'info',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_6', 'heading_7'))
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_filter_item_color_hover',
            'heading'       => esc_html__('Nav Item Hover/Color', 'Stories'),
            'group'         => esc_html__('Header Navigation Styles', 'Stories') 
        ); 
        $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'header_set_7',
            'description'   => esc_html__('Active', 'Stories'),
            'group'         => esc_html__('Header Navigation Styles', 'Stories'), 
            'std' =>'info',
            'dependency'    => array('element' => "header_type", 'value' => array('heading_6', 'heading_7'))
        );
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_filter_active_item_color',
            'heading'       => esc_html__('Active Nav Item Color', 'Stories'),
            'group'         => esc_html__('Header Navigation Styles', 'Stories') 
        ); 
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'header_filter_active_item_border_color',
            'heading'       => esc_html__('Active Item Border Color', 'Stories'),
            'group'         => esc_html__('Header Navigation Styles', 'Stories') 
        );       
    }
    public function set_header_filter_style_options( $instance ) { 

       

        $instance->add_control(
            'heading_typo4',
            [
				'label' => __( 'Nav Typography', 'Stories' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
			 
		);

	    $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'header_filter_item_typography',
			    'label'       => __( 'Header Filter Typography', 'Stories' ),
			    'description' => __( 'Set typography for Header Filter Items', 'Stories' ),
                'selector'    => '{{WRAPPER}} .jeg_subcat a',
		    ]
        );   
    
        $instance->add_control(
            'heading_typo5',
            [
				'label' => __( 'Nav Spacing', 'Stories' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
			 
		);

        $instance->add_responsive_control(
            'header_filter_padding',
            [
                'label' => __( 'Header Filter Padding', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .stories_block_heading .jeg_subcat' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
                ],
            ]
            
        );
    
        $instance->add_responsive_control(
            'header_filter_margin',
            [
                'label' => __( 'Header Filter Margin', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .stories_block_heading .jeg_subcat' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
                ],
            ]
            
        );
    }
    
    public function set_content_style_option($ali = 16)
    { 
        
       $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'content11',
            'description'   => esc_html__('Content Color', 'Stories'),
            'group'         => esc_html__('Content Style', 'Stories'), 
            'std' =>'info',
        );
        
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'post_colored_category',
            'heading'       => esc_html__('Colored Category', 'Stories'),
            'description'   => esc_html__('Use color of Categories which is selected on category page', 'Stories'),
            'group'         => esc_html__('Content Style', 'epic-ne'), 
            'std'           => 'yes',
            'value'         => array(
                esc_html__('No', 'Stories')   => 'no',
                esc_html__('Yes', 'Stories')     => 'yes' 
            )
        );
        

        // improve ben ekeldim bunlari
            $customizer = Customizer::get_instance();

            $all_font = $customizer->load_all_font();


            foreach ( $all_font as $font ){
            $fonts[$font['family']] = $font['family'];
            }

        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'fff',
            'heading'       => esc_html__('Font Family', 'Stories'),
            'description'   => esc_html__('Select the Font you want to use', 'Stories'),
            'group'         => esc_html__('Content Style', 'epic-ne'), 
            'std'           => 'yes',
            'value'         => $fonts
        );

       
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_category_text_color',
            'heading'       => esc_html__('Category Text Color','epic-ne'),
            'description'   => esc_html__('Choose Category Text Color.','epic-ne'),
            'group'         => esc_html__('Content Style', 'epic-ne'), 
            'dependency'    => Array('element' => "post_colored_category", 'value' => 'no'), 
            'std'           => ''
        ); 
         
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_category_bg_color',
            'heading'       => esc_html__('Category Background Color','epic-ne'),
            'description'   => esc_html__('Choose Category Background Color.','epic-ne'),
            'group'         => esc_html__('Content Style', 'epic-ne'), 
            'dependency'    => Array('element' => "post_colored_category", 'value' => 'no'), 
            'std'           => ''
        );   

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'excerpt_color',
            'group'         => esc_html__('Content Style', 'Stories'),
            'heading'       => esc_html__('Excerpt Color', 'Stories'),
        );

 


        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'title_color',
            'group'         => esc_html__('Content Style', 'Stories'),
            'heading'       => esc_html__('Post Title Color', 'Stories'),
        );  

         
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'accent_color',
            'group'         => esc_html__('Content Style', 'Stories'),
            'heading'       => esc_html__('Post Title Hover', 'Stories'),

        );

       $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'content21',
            'description'   => esc_html__('Author, Comment Color…', 'Stories'),
            'group'         => esc_html__('Content Style', 'Stories'), 
            'std' =>'info',
        );
        
        
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'meta_advanced_colors',
            'group'         => esc_html__('Content Style', 'Stories'),
		    'heading'       => esc_html__('Show Advanced Color Options', 'epic-ne'),
		    'value'         => array( esc_html__("This option will turn Advanced Color Options.", 'epic-ne') => 'true' ),
        );  



        
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_seperator_color',
            'heading'       => esc_html__('Meta Seperator Color','Stories'),
            'description'   => esc_html__('Choose Meta Seperator Color.','Stories'),
            'group'         => esc_html__('Content Style', 'Stories'),             
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        ); 
         
        
 
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_author_text_color',
            'heading'       => esc_html__('Author Color','epic-ne'),
            'description'   => esc_html__('Choose Author Text Color.','epic-ne'),
            'group'         => esc_html__('Content Style', 'epic-ne'),
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        );   
  
         
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_date_text_color',
            'heading'       => esc_html__('Date Color','Stories'),
            'description'   => esc_html__('Choose Date Text Color.','Stories'),
            'group'         => esc_html__('Content Style', 'Stories'), 
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        );  

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_comment_text_color',
            'heading'       => esc_html__('Comment Color','epic-ne'),
            'description'   => esc_html__('Choose Comment Text Color.','epic-ne'),
            'group'         => esc_html__('Content Style', 'epic-ne'), 
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        );  
          
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_reading_text_color',
            'heading'       => esc_html__('Reading Time Color','Stories'),
            'description'   => esc_html__('Choose Reading Time Text Color.','Stories'),
            'group'         => esc_html__('Content Style', 'Stories'), 
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        );  
          
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_view_text_color',
            'heading'       => esc_html__('View Count Color','Stories'),
            'description'   => esc_html__('Choose View Count Text Color.','Stories'),
            'group'         => esc_html__('Content Style', 'Stories'), 
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        );  
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_rating_text_color',
            'heading'       => esc_html__('Rating Color','Stories'),
            'description'   => esc_html__('Choose Rating Text Color.','Stories'),
            'group'         => esc_html__('Content Style', 'Stories'), 
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        ); 
 
        
          
        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_read_more_text_color',
            'heading'       => esc_html__('Read More Button Text Color','Stories'),
            'description'   => esc_html__('Choose Read More Button\'s  Text Color.','Stories'),
            'group'         => esc_html__('Content Style', 'Stories'), 
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        ); 

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_read_more_bg_color',
            'heading'       => esc_html__('Read More Button Background Color','Stories'),
            'description'   => esc_html__('Choose Read More Button\'s Background Color.','Stories'),
            'group'         => esc_html__('Content Style', 'Stories'), 
            'dependency'    => array('element' => "meta_advanced_colors", 'value' => 'true' ),
            'std'           => ''
        ); 
        

        // Seperator


        if($ali == 3){

            $this->options[] = array(
                'type'          => 'alert',
                'param_name'    => 'content8',
                'description'   => esc_html__('Vertical Seperator', 'Stories'),
                'group'         => esc_html__('Post Seperator', 'Stories'), 
                'std' =>'info',
            );

        }

        $this->options[] = array(
            'type'          => 'number',
            'param_name'    => 'post_vertical_line_width', 
            'min'           => '0',
            'max'           => '5',
            'heading'       => esc_html__('Post Vertical Seperator Width', 'epic-ne'),
            'description'   => esc_html__('Set Vertical Seperator width of your post', 'epic-ne'),
            'group'         => esc_html__('Post Seperator', 'epic-ne') ,
            'dependency'    => array('element' => "column_width", 'value' => array('auto','8','12'))
        );    

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'post_vertical_line_color',  
            'heading'       => esc_html__('Post Seperator Color', 'epic-ne'),
            'description'   => esc_html__('Set Seperator color of your post', 'epic-ne'),
            'group'         => esc_html__('Post Seperator', 'epic-ne') ,
            'dependency'    => array('element' => "column_width", 'value' => array('auto','8','12')),
            'std'           => '#dddddd'
        );  
        
        $this->options[] = array(
            'type'          => 'alert',
            'param_name'    => 'content5',
            'description'   => esc_html__('Post Bottom Seperator', 'Stories'),
            'group'         => esc_html__('Post Seperator', 'Stories'), 
            'std' =>'info',
        );
        
        
        $this->options[] = array(
            'type'          => 'number',
            'param_name'    => 'post_bottom_line_height', 
            'min'           => '0',
            'max'           => '5',
            'heading'       => esc_html__('Post Bottom Seperator Height', 'Stories'),
            'description'   => esc_html__('Set Bottom Seperator height of your post', 'Stories'),
            'group'         => esc_html__('Post Seperator', 'Stories') , 
           
        );  

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'post_bottom_line_color',  
            'heading'       => esc_html__('Post Bottom Line Color', 'Stories'),
            'description'   => esc_html__('Set bottom line color of your post', 'Stories'),
            'group'         => esc_html__('Post Seperator', 'Stories') , 
            'std'           => '#dddddd' 
        );  


     
    }

    public function set_meta_setting_option( $show_excerpt = true, $show_meta_author = true , $show_meta_date = true, $show_meta_comments = false, $show_meta_categories = true, $show_meta_reading_time = false, $show_meta_share = false, $small_share = false, $show_meta_view = false,$show_read_more = false, $show_post_format = false, $show_meta_rating = false)
    {    

	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'post_number',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
		    'heading'       => esc_html__('Show Post Number', 'epic-ne'),
		    'value'         => array( esc_html__("This option will turn post number on.", 'epic-ne') => 'true' ),
        );  
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_author',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
		    'heading'       => esc_html__('Show Author', 'epic-ne'), 
            'std'           => $show_meta_author
        ); 
        
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_date',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
		    'heading'       => esc_html__('Show Date', 'epic-ne'),
            'std'           => $show_meta_date,
            'value'         => array( esc_html__("This option will turn the date.", 'Stories') => 'true' ),

        ); 
        
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'date_format',
            'heading'       => esc_html__('Content Date Format', 'Stories'),
            'description'   => esc_html__('Choose which date format you want to use.', 'Stories'),
            'std'           => 'default',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'value'         => array(
                esc_html__('Relative Date/Time Format (ago)', 'Stories')               => 'ago',
                esc_html__('WordPress Default Format', 'Stories')      => 'default',
                esc_html__('Custom Format', 'Stories')                 => 'custom',
            ),
            'dependency'    => array('element' => 'show_meta_date', 'value' => 'true')
        );

        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'date_format_custom',
            'heading'       => esc_html__('Custom Date Format', 'Stories'),
            'description'   => wp_kses(sprintf(__('Please write custom date format for your module, for more detail about how to write date format, you can refer to this <a href="%s" target="_blank">link</a>.', 'Stories'), 'https://codex.wordpress.org/Formatting_Date_and_Time'), wp_kses_allowed_html()),
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'std'           => 'Y/m/d',
            'dependency'    => array('element' => 'date_format', 'value' => array('custom'))
        );

        $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_excerpt',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Excerpt', 'epic-ne'), 
            'std'           => $show_excerpt
	    ); 

       
            $this->options[] = array(
                'type'          => 'slider',
                'param_name'    => 'excerpt_length',
                'heading'       => esc_html__('Excerpt Length', 'Stories'),
                'description'   => esc_html__('Set word length of excerpt on post block.', 'Stories'),
                'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
                'min'           => 0,
                'max'           => 200,
                'step'          => 1,
                'std'           => 20,
                'dependency'    => array('element' => 'show_excerpt', 'value' => 'true')
            );


            $this->options[] = array(
                'type'          => 'textfield',
                'param_name'    => 'excerpt_ellipsis',
                'heading'       => esc_html__('Excerpt Ellipsis', 'Stories'),
                'description'   => esc_html__('Define excerpt ellipsis. Example: "..." ', 'Stories'),
                'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
                'std'           => '',
                'dependency'    => array('element' => 'show_excerpt', 'value' => 'true' )
            );



        $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_read_more',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Read More Button', 'epic-ne'), 
            'std'           => $show_read_more
	    ); 

	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_comments',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Comments Number', 'epic-ne'), 
            'std'           => $show_meta_comments
        ); 
        
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_categories',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Categories', 'epic-ne'), 
            'std'           => $show_meta_categories
	    ); 
        
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_reading_time',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Reading Time', 'epic-ne'), 
            'std'           => $show_meta_reading_time
	    ); 
          
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_share',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Share', 'epic-ne'), 
            'std'           => $show_meta_share
	    ); 
          
        
          
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'small_share',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Want Small Share?', 'epic-ne'), 
            'std'           => $small_share,
            'dependency'    => array('element' => "show_meta_share", 'value' => 'true' ),
	    ); 
     

	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_share',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Share', 'epic-ne'), 
            'std'           => $show_meta_share
	    ); 
          
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_view',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show View Count', 'epic-ne'), 
            'std'           => $show_meta_view
	    ); 
          
          
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_post_format',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Post Format', 'epic-ne'), 
            'std'           => $show_post_format
	    ); 
          
	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'show_meta_rating',
            'group'         => esc_html__('Meta Visibility-Excerpt, date…', 'Stories'),
            'heading'       => esc_html__('Show Rating', 'epic-ne'), 
            'std'           => $show_meta_rating
	    ); 
     
          
    }
    
 
}
