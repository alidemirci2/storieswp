<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Block;

Class Block_14_Option extends BlockOptionAbstract
{
    protected $default_number_post = 4;


    protected $show_meta_categories = 'yes';
    protected $show_meta_reading_time = false;

    protected $show_meta_author = 'yes';
    protected $show_meta_date = 'yes'; 
    protected $show_meta_comments = 'yes';

    protected $show_meta_share = 'yes';

    protected $show_excerpt = false;


    protected $show_ads = true;
    protected $default_ajax_post = 4;
   


    public function get_module_name()
    {
        return esc_html__('EPIC - Module 14', 'Stories');
    }

    public function compatible_column()
    {
        // Kullanıcı Hangi Kolon Yapılarını Seçebiliri belirliyoruz. BURAK*
        return array(  3, 4,  6, 12 );
    }
    public function additional_style()
    {
        parent::additional_style();

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'block_background',
            'group'         => esc_html__('Design', 'Stories'),
            'heading'       => esc_html__('Block Background', 'Stories'),
            'description'   => esc_html__('This option will change your Block Background', 'Stories'),
        ); 
    }
}
