<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Block;

Class Block_16_Option extends BlockOptionAbstract
{
    protected $default_number_post = 6;
 

    protected $show_meta_categories = false;


    protected $show_meta_author = false;
    protected $show_meta_date = 'yes'; 
    protected $show_meta_comments = false;

    protected $show_meta_share = 'yes';
    protected $small_share = 'yes';


    protected $show_excerpt = false;



    protected $show_ads = true;
    protected $default_ajax_post = 6;






    

    public function get_module_name()
    {
        return esc_html__('EPIC - Module 16', 'Stories');
    }

    public function compatible_column()
    {
        // Kullanıcı Hangi Kolon Yapılarını Seçebiliri belirliyoruz. BURAK*
        return array(  2,3,4,5,6 );
    }
    public function additional_style()
    {
        parent::additional_style();

   
    }
}
 