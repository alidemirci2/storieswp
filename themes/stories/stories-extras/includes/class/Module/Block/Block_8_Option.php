<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Block;
 
Class Block_8_Option extends BlockOptionAbstract
{
    protected $default_number_post = 4;
    protected $show_excerpt = false;
    protected $show_ads = true;
    protected $show_meta_comments = false;
    protected $default_ajax_post = 4;


    

    public function get_module_name()
    {
        return esc_html__('EPIC - Module 8', 'Stories');
    }


	public function set_style_option()
	{
		$this->set_boxed_option();
		parent::set_style_option();
    }
    
}
