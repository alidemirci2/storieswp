<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Block;
 
Class Block_7_Option extends BlockOptionAbstract
{
    protected $default_number_post = 4;
    protected $show_excerpt = false;
    protected $show_ads = true;
    protected $default_ajax_post = 4;


    

    public function get_module_name()
    {
        return esc_html__('EPIC - Module 7', 'Stories');
    }
    public function compatible_column()
    {
        // Kullanıcı Hangi Kolon Yapılarını Seçebiliri belirliyoruz. BURAK*
        return array(  12 );
    }
	public function set_style_option()
	{
		$this->set_boxed_option();
		parent::set_style_option();
	}
}
