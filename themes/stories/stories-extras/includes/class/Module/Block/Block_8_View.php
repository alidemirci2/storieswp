<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Block;

Class Block_8_View extends BlockViewAbstract
{
    public function render_block_type_1($post, $image_size, $no = 0)
    {
        $thumbnail          = $this->get_thumbnail($post->ID, $image_size);
        $primary_category   = epic_get_primary_category($post->ID); 
        $term = get_term($primary_category); 
        $post_share = epic_module_post_share( '', $post->ID, false, array() );  
        $post_format = epic_module_post_format(  $post->ID );   
        $reading_time = epic_module_post_reading_time(  $post->ID );  
        $post_view = epic_module_post_view ( $post->ID );  
        $post_rating = epic_module_post_rating ( $post->ID ); 
        $post_number = $no + 1;

        $output =
            "<article " . epic_post_class("jeg_post", $post->ID) . ">
                <div class=\"jeg_inner_post row\">
                    <div class=\"col-lg-4 col-sm-4\">
                        <div class=\"jeg_postblock_content show_post_number\">
                            <div class=\"jeg_post_number\">
                                <span>".$post_number."</span>
                            <!-- /.jeg_post_number --> </div>
                            <div class=\"jeg_postblock_container\">
                                <div class=\"jeg_post_category\"> 
                                    <span class=\"jeg_post_cat\"> 
                                        <a href=\"" . get_category_link($primary_category) . "\" class=\"category-".$term->slug."\">" . get_cat_name($primary_category) . "</a>
                                    </span>
                                    <span class=\"jeg_post_reading_time\">
                                        ".$reading_time."
                                    </span>
                                <!-- /.jeg_post_category --></div>
                                <h3 class=\"jeg_post_title\">
                                    <a href=\"" . get_the_permalink($post) . "\">" . get_the_title($post) . "</a>
                                </h3>
                                " . $this->post_meta_4($post) . "
                                <div class=\"jeg_post_excerpt\">
                                    <p>" . $this->get_excerpt($post) . "</p>
                                    <a href=\"" . get_the_permalink($post) . "\" class=\"jeg_readmore\">" . esc_html__('Read more','epic-ne') . "
                                    ".file_get_contents(THEME_URI  . '/assets/img/arrow-thing-right.svg')."</a>                            </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-lg-8 col-sm-8\">
                        <div class=\"jeg_thumb\">
                                " . epic_edit_post( $post->ID ) . "
                                <a href=\"" . get_the_permalink($post) . "\">" . $thumbnail . "</a>
                                ".$post_share."
                                ".$post_format." 
                                <div class=\"meta_top_left\">".$post_rating." ". $post_view."</div>
                        </div>
                    </div>
                </div>
            </article>";

        return $output;
    }




    public function build_column_1($results)
	{

        $first_block  = '';
        $ads_position = $this->random_ads_position(sizeof($results));

        for ( $i = 0; $i < sizeof($results); $i++ )
        {
            if ( $i == $ads_position )
            {
                $first_block .= $this->render_module_ads('jeg_ajax_loaded anim_' . $i);
            }

            $first_block .= $this->render_block_type_1($results[$i], 'epic-750x536');
        }

		$output ="
                <div class=\"jeg_posts jeg_load_more_flag\"> 
                    {$first_block}
                </div>
                ";

		return $output;
	}

    public function build_column_2($results)
	{

        $first_block  = '';
        $ads_position = $this->random_ads_position(sizeof($results));

        for ( $i = 0; $i < sizeof($results); $i++ )
        {
            if ( $i == $ads_position )
            {
                $first_block .= $this->render_module_ads('jeg_ajax_loaded anim_' . $i);
            }

            $first_block .= $this->render_block_type_1($results[$i], 'epic-750x536');
        }

		$output ="
                <div class=\"jeg_posts jeg_load_more_flag\"> 
                    {$first_block}
                </div>
                ";

		return $output;
	}

    public function build_column_1_alt($results)
    {
        $first_block  = '';
        $ads_position = $this->random_ads_position(sizeof($results));

        for ( $i = 0; $i < sizeof($results); $i++ )
        {
            if ( $i == $ads_position )
            {
                $first_block .= $this->render_module_ads('jeg_ajax_loaded anim_' . $i);
            }

            $first_block .= $this->render_block_type_1($results[$i], 'epic-750x536');
        }

        $output = $first_block;

        return $output;
    }

    public function render_output($attr, $column_class)
    {
	    if ( isset( $attr['results'] ) ) {
		    $results = $attr['results'];
	    } else {
		    $results = $this->build_query($attr);
	    }

	    $navigation = $this->render_navigation($attr, $results['next'], $results['prev'], $results['total_page']);

        if(!empty($results['result'])) {
            $content = $this->render_column($results['result'], $column_class);
        } else {
            $content = $this->empty_content();
        }

        return
            "<div class=\"jeg_block_container {$column_class}\">
                {$this->get_content_before($attr)}
                {$content}
                {$this->get_content_after($attr)}
            </div>
            <div class=\"jeg_block_navigation\">
                {$this->get_navigation_before($attr)}
                {$navigation}
                {$this->get_navigation_after($attr)}
            </div>";
    }

    public function render_column($result, $column_class)
    {
        switch($column_class)
        {
            case "big_post" :
                $content = $this->build_column_1($result);
            break;
            case "narrow_post" :
            case "medium_post" :
                $content = $this->build_column_2($result);
            break;
            default :
                $content = $this->build_column_2($result);
            break;
        }

        return $content;
    }

    public function render_column_alt($result, $column_class)
    {
        $content = $this->build_column_1_alt($result);
        return $content;
    }
}
