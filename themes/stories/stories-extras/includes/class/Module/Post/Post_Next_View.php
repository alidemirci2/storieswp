<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

Class Post_Next_View extends PostViewAbstract {
	public function render_module_back( $attr, $column_class ) {

		$style = $this->generate_style( $attr );

        if(isset($attr['show_title']) && $attr['show_title'] ){
        $title = "
        <div class=\"prev_text_title\">
            <a href=\"" . esc_url( get_permalink( $prev_post->ID ) ) . "\" class=\"post next-post\">
                <h5 class=\"post-title\">" . esc_html('Example Post Title Name','Stories') . "</h5>						
            </a>			
        </div>";
        }

		return
			"<div {$this->element_id($attr)} class='jeg_custom_next_wrapper center epic_next_container {$attr['scheme']} {$attr['el_class']}'>   
				{$style}
				<div class=\"jeg_prev_next_post jeg_next_post pos-center  box-shadow\">
					<div class=\"prev_text_icon\"> ". file_get_contents(THEME_URI  . '/assets/svg/arrow-thing-right.svg.php')  ."</div>
					{$title}
					<h6 class=\"caption\">" . esc_html__( 'Next', 'Stories' ) . "</h6>
                </div>
            </div>";
	}

	public function render_module_front( $attr, $column_class ) {
		$title = '';
		$next_post        = get_next_post();

		$style = $this->generate_style( $attr );

        if(isset($attr['show_title']) && $attr['show_title']){
        $title = "
        <div class=\"prev_text_title\">
            <a href=\"" . esc_url( get_permalink( $next_post->ID ) ) . "\" class=\"post next-post\">
                <h5 class=\"post-title\">" . wp_kses_post( $next_post->post_title ) . "</h5>						
            </a>			
        </div>";
        }

		if($next_post){
		return
			"<div {$this->element_id($attr)} class='jeg_custom_next_wrapper pos-center epic_next_container {$attr['scheme']} {$attr['el_class']}'>
				{$style}
				<div class=\"jeg_prev_next_post jeg_next_post pos-center  box-shadow\">
					<div class=\"prev_text_icon\"> ". file_get_contents(THEME_URI  . '/assets/svg/arrow-thing-right.svg.php')  ."</div>
					{$title}			
					<h6 class=\"caption\">" . esc_html__( 'Next', 'Stories' ) . "</h6>
				</div>                
			</div>";
		}
	}

	public function generate_style( $attr ) {

		$result = $title = '';


		if ( isset( $attr['nav_font_size'] ) && $attr['nav_font_size'] ) {
			$result .= $this->element_id( $attr ) . ' .jeg_prev_next_post .caption {font-size: ' . $attr['nav_font_size'] . ';}';
		}

		if ( isset( $attr['nav_color'] ) && $attr['nav_color'] ) {
			$result .= $this->element_id( $attr ) . ' .jeg_prev_next_post .caption {color: ' . $attr['nav_color'] . ';}';
		}

		if ( isset( $attr['title_font_size'] ) && $attr['title_font_size'] ) {
			$result .= $this->element_id( $attr ) . ' .jeg_prev_next_post a h5 {font-size: ' . $attr['title_font_size'] . ';}';
		}

		if ( isset( $attr['title_color'] ) && $attr['title_color'] ) {
			$result .= $this->element_id( $attr ) . ' .jeg_prev_next_post a h5 {color: ' . $attr['title_color'] . ';}';
		}

		if ( $result ) {
			$result = '<style>' . $result . '</style>';
		}

		return $result;
	}
}
