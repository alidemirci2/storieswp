<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Comment_Option extends ModuleOptionAbstract
{
    public function get_category()
    {
        return esc_html__('Stories - Post', 'Stories');
    }

    public function compatible_column()
    {
        return array( 1,2,3,4,5,6,7,8,9,10,11,12 );
    }

    public function get_module_name()
    {
        return esc_html__('Stories - Comment', 'Stories');
    }

    public function set_options()
    {
        $this->set_style_option();
    }

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'comment_typography',
				'label'       => __( 'Typography', 'Stories' ),
				'selector'    => '{{WRAPPER}} #comments *',
			]
		);
	}
}
