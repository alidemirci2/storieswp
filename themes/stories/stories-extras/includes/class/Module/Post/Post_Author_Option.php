<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Author_Option extends ModuleOptionAbstract
{
    public function get_category()
    {
        return esc_html__('Stories - Post', 'Stories');
    }

    public function compatible_column()
    {
        return array( 1,2,3,4,5,6,7,8,9,10,11,12 );
    }

    public function get_module_name()
    {
        return esc_html__('Stories - Post Author Box', 'Stories');
    }

    public function set_options()
    {
        $this->set_author_options();
        
    }

	public function set_author_options() {
 
        $this->options[] = array(
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'RSS Icon Visibility', 'Stories' ),
			'param_name' => 'rss_icon',
			'value'      => array( esc_html__( "Show RSS icon", 'Stories' ) => 'yes' ),
			'std'        => 'yes',
        );
 
        $this->options[] = array(
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'Author Other Posts', 'Stories' ),
			'param_name' => 'other_posts',
			'value'      => array( esc_html__( "Show Author Posts in Author Box ", 'Stories' ) => 'yes' ),
			'std'        => 'yes',
        );

        $this->set_style_option();

    }

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'author_typography',
				'label'       => __( 'Typography', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_authorbox *',
			]
		);
	}
}
