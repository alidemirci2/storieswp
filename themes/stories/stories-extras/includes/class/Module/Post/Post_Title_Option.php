<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Title_Option extends ModuleOptionAbstract {
	public function get_category() {
		return esc_html__( 'Stories - Post', 'Stories' );
	}

	public function compatible_column() {
		return array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
	}

	public function get_module_name() {
		return esc_html__( 'Stories - Post Title', 'Stories' );
	}


    public function set_options()
    { 
        $this->set_post_title_options1();
        $this->set_post_title_options2(); 
    }	

	public function set_post_title_options1() {

		$this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'font_size',
			'heading'     => esc_html__( 'Font Size', 'Stories' ),
			'description' => esc_html__( 'Set font size with unit (Ex: 36px or 4em).', 'Stories' ),
			'group'       => esc_html__( 'Post Title Typography', 'Stories' ),
		);

	}

    public function set_post_title_typography_option( $instance ) {
        $instance->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name'     => 'title_typography',
                'label'    => __( 'Typography', 'Stories' ),
                'selector' => '{{WRAPPER}} .stories_post_title', 
            ]
        );
	}

	public function set_post_title_options2() {

		$this->options[] = array(
			'type'        => 'colorpicker',
			'param_name'  => 'title_color',
			'heading'     => esc_html__( 'Title Color', 'Stories' ),
			'description' => esc_html__( 'Set title color.', 'Stories' ),
			'group'       => esc_html__( 'Design', 'Stories' ),
		);

		$this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'title_width',
			'heading'     => esc_html__( 'Width', 'Stories' ),
			'description' => esc_html__( 'Set Width with unit (Ex: 300px or 40%).', 'Stories' ),
			'group'       => esc_html__( 'Design', 'Stories' ),
		);

        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'title_align',
            'heading'     => esc_html__( 'Align', 'Stories' ),
            'std'     => 'center',
			'group'       => esc_html__( 'Design', 'Stories' ),
            'value'       => array(
                esc_html__( 'Left', 'Stories' )  => 'left',
                esc_html__( 'Center', 'Stories' )  => 'center',
                esc_html__( 'Right', 'Stories' )  => 'right',
            )
        );

		$this->set_style_option();
	}
	
}
