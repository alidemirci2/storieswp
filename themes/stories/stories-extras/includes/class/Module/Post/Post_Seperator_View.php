<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

Class Post_Seperator_View extends PostViewAbstract {

	public function render_module_back( $attr, $column_class ) {

		$style = $this->generate_style( $attr );

		$content = "";

		if($attr['seperator_type'] == "solid"){
		
		$content = "
			<span class=\"solid\"></span>
		";

		}else if($attr['seperator_type'] == "three-dot"){
			
		$content = "
			<span class=\"three-dot\"></span>
			<span class=\"three-dot\"></span>
			<span class=\"three-dot\"></span>
		";

		}else if($attr['seperator_type'] == "zigzag"){

		switch ($attr['seperator_size']) {
			case 'thin':
				$seperator_size = 2;
				break;
			case 'normal':
				$seperator_size = 3;
				break;
			case 'big':
				$seperator_size = 4;
				break;
			case 'bigger':
				$seperator_size = 5;
				break;

		}

		// improve: better SVG design. Width Support.
		$content = "
			<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"124\" height=\"12\" viewBox=\"0 0 124 12\">
				<g fill=\"none\" fill-rule=\"evenodd\">
					<polyline stroke=\"{$attr['seperator_color']}\" stroke-width=\"{$seperator_size}\" points=\"1 10 9.5 2.5 17 10 25.5 2 33 10\"></polyline>
					<polyline stroke=\"{$attr['seperator_color']}\" stroke-width=\"{$seperator_size}\" points=\"61 10 69.5 2.5 77 10 85.5 2 93 10\"></polyline>
					<polyline stroke=\"{$attr['seperator_color']}\" stroke-width=\"{$seperator_size}\" points=\"31 10 39.5 2.5 47 10 55.5 2 63 10\"></polyline>
					<polyline stroke=\"{$attr['seperator_color']}\" stroke-width=\"{$seperator_size}\" points=\"91 10 99.5 2.5 107 10 115.5 2 123 10\"></polyline>
				</g>
			</svg>
		";
		
		}

		return
			"<div {$this->element_id($attr)} class='stories-seperator  {$attr['scheme']} {$attr['el_class']}'>
				{$style}
				<div class=\"seperator-container seperator-{$attr['seperator_type']} {$attr['seperator_size']} \">
					{$content}
				</div>   
            </div>";

	}

	public function render_module_front( $attr, $column_class ) {

		$style = $this->generate_style( $attr );

		$content = "";

		if($attr['seperator_type'] == "solid"){
		
		$content = "
			<span class=\"solid\"></span>
		";

		}else if($attr['seperator_type'] == "three-dot"){
			
		$content = "
			<span class=\"three-dot\"></span>
			<span class=\"three-dot\"></span>
			<span class=\"three-dot\"></span>
		";

		}else if($attr['seperator_type'] == "zigzag"){
	

		switch ($attr['seperator_size']) {
			case 'thin':
				$seperator_size = 2;
				break;
			case 'normal':
				$seperator_size = 3;
				break;
			case 'big':
				$seperator_size = 4;
				break;
			case 'bigger':
				$seperator_size = 5;
				break;

		}


		$content = "
			<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"124\" height=\"12\" viewBox=\"0 0 124 12\">
				<g fill=\"none\" fill-rule=\"evenodd\">
					<polyline stroke=\"{$attr['seperator_color']}\" stroke-width=\"{$seperator_size}\" points=\"1 10 9.5 2.5 17 10 25.5 2 33 10\"></polyline>
					<polyline stroke=\"{$attr['seperator_color']}\" stroke-width=\"{$seperator_size}\" points=\"61 10 69.5 2.5 77 10 85.5 2 93 10\"></polyline>
					<polyline stroke=\"{$attr['seperator_color']}\" stroke-width=\"{$seperator_size}\" points=\"31 10 39.5 2.5 47 10 55.5 2 63 10\"></polyline>
					<polyline stroke=\"{$attr['seperator_color']}\" stroke-width=\"{$seperator_size}\" points=\"91 10 99.5 2.5 107 10 115.5 2 123 10\"></polyline>
				</g>
			</svg>
		";

		
		}

		return
			"<div {$this->element_id($attr)} class='stories-seperator  {$attr['scheme']} {$attr['el_class']}'>
				{$style}
				<div class=\"seperator-container seperator-{$attr['seperator_type']} {$attr['seperator_size']} \">
					{$content}
				</div>   
            </div>";
	}

	public function generate_style( $attr ) {

		$result = $container = $zigzag_before = $zigzag_after = '';

		
		if ( isset( $attr['seperator_color'] ) && $attr['seperator_color'] ) {
			$result .= 'background-color: ' . $attr['seperator_color'] . ';';
		}
		

		if($attr['seperator_type'] != "three-dot"){
			if ( isset( $attr['seperator_width'] ) && $attr['seperator_width'] ) {
				$result .= 'width: ' . $attr['seperator_width']. ';';
			}
		}


		if ( isset( $attr['seperator_align'] ) && $attr['seperator_align'] ) {
			if($attr['seperator_align'] == 'center'){
				$container .= 'justify-content: center;';
			}else if($attr['seperator_align'] == 'right'){
				$container .= 'justify-content: flex-end;';
			}
		}

		


		if ( $result ) {
			$result = '<style>' . $this->element_id( $attr ) . ' 

			.'.$attr['seperator_type'].' {' . $result . '} 
			.seperator-container { ' . $container . ' }
			.zigzag:before { ' . $zigzag_before . ' } 
			.zigzag:after { ' . $zigzag_after . ' }
			</style>';
		}

		return $result;
	}
}
