<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

Class Post_Author_View extends PostViewAbstract {

	public function render_module_back( $attr, $column_class ) {
		global $post;

			$author_socials = $other_posts = '';
			
			if(get_the_author_meta( 'url', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'url', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">" . file_get_contents(THEME_URI  . '/assets/img/globe.svg') . "</a>";
			}
			if(isset($attr['rss_icon']) && $attr['rss_icon'] ){
			$author_socials .= "<a href='" . get_the_author_meta( 'rss', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">" . file_get_contents(THEME_URI  . '/assets/img/rss.svg') . "</a>";
			}

			if(get_the_author_meta( 'behance', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'behance', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/behance.svg') ."</a>";
			}

			if(get_the_author_meta( 'dribbble', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'dribbble', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/dribbble.svg') ."</a>";
			}

			if(get_the_author_meta( 'facebook', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'facebook', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/facebook.svg') ."</a>";
			}

			if(get_the_author_meta( 'facebook-messenger', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'facebook-messenger', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/facebook-messenger.svg') ."</a>";
			}

			if(get_the_author_meta( 'foursquare', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'foursquare', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/foursquare.svg') ."</a>";
			}

			if(get_the_author_meta( 'instagram', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'instagram', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/instagram.svg') ."</a>";
			}

			if(get_the_author_meta( 'linkedin', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'linkedin', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/linkedin.svg') ."</a>";
			}

			if(get_the_author_meta( 'medium', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'medium', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/medium.svg') ."</a>";
			}

			if(get_the_author_meta( 'patreon', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'patreon', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/patreon.svg') ."</a>";
			}

			if(get_the_author_meta( 'periscope', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'periscope', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/periscope.svg') ."</a>";
			}

			if(get_the_author_meta( 'pinterest', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'pinterest', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/pinterest.svg') ."</a>";
			}

			if(get_the_author_meta( 'skype', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'skype', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/skype.svg') ."</a>";
			}

			if(get_the_author_meta( 'slack', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'slack', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/slack.svg') ."</a>";
			}

			if(get_the_author_meta( 'tumblr', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'tumblr', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/tumblr.svg') ."</a>";
			}

			if(get_the_author_meta( 'twitch', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'twitch', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/twitch.svg') ."</a>";
			}

			if(get_the_author_meta( 'twitter', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'twitter', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/twitter.svg') ."</a>";
			}

			if(get_the_author_meta( 'vimeo', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'vimeo', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/vimeo.svg') ."</a>";
			}

			if(get_the_author_meta( 'whatsapp', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'whatsapp', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/whatsapp.svg') ."</a>";
			}

			if(get_the_author_meta( 'youtube', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'youtube', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/youtube.svg') ."</a>";
			}


			// improve, burak: Bu adamlarinin class'larinin icinde wp_query nasil olusturcaz biliyorsan bu maddeyi yapabilirsin.
			// bilmiyorsan simdilik bu maddeyi erteleyelim belki kurcalarken ogreniriz kolayca yapariz.

			if( isset($attr['other_posts']) && $attr['other_posts'] ){
			$other_posts = "
				<div class=\"jeg_authorbox_bottom\"><h6 class='authorbox_segment_title'>". 
					sprintf(esc_html__( '%s\'s Posts ','fastcast' ), get_the_author_meta( 'display_name', $post->post_author )) . "</h6>
					<div class=\"author_other_post title_font\">
						<ul>
							<li><a href=\"#\">Already Master Camp on the Red monetize</a></li>
							<li><a href=\"#\">But according to the creator of one parents go them you enginner</a></li>
							<li><a href=\"#\">Of the world’s most</a></li>
							<li><a href=\"#\">Serdar Teacher Red monetize</a></li>
						</ul>
					</div>
				</div>";
			}


		return
			"<div {$this->element_id($attr)} class='jeg_custom_author_wrapper epic_author_box_container {$attr['scheme']} {$attr['el_class']}'>
				<div class=\"jeg_authorbox box-shadow\">
					<h6 class='authorbox_segment_title'>". esc_html('AUTHOR','Stories') ."</h6>
					<div class=\"jeg_authorbox_top clearfix\">
						<div class=\"jeg_author_image\">
							" . get_avatar( get_the_author_meta( 'ID', $post->post_author ), 67, null, get_the_author_meta( 'display_name', $post->post_author ) ) . "
							<div class=\"jeg_author_socials\">
								{$author_socials}
							</div>
							</div>
						<div class=\"jeg_author_content\">
							<h5 class=\"jeg_author_name\">
								" . epic_the_author_link( $post->post_author, false ) . "
							</h5>
							<p class=\"jeg_author_desc\">
								" . get_the_author_meta( 'description', $post->post_author ) ." 
								<span class='more_link'><strong>". esc_html('more','Stories') . "</strong></span>
							</p>
						</div>
					</div>
					{$other_posts}
				</div>
			</div>";
	}
	

	public function render_module_front( $attr, $column_class ) {
		return $this->render_module_back( $attr, $column_class );
	}
}
