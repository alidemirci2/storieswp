<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Seperator_Option extends ModuleOptionAbstract {
	public function get_category() {
		return esc_html__( 'Stories - Post', 'Stories' );
	}

	public function compatible_column() {
		return array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
	}

	public function get_module_name() {
		return esc_html__( 'Stories - Seperator Vertical', 'Stories' );
	}


	public function set_options() {

        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'seperator_type',
            'heading'     => esc_html__( 'Seperator Type', 'Stories' ),
            'std'     => 'solid',
            'value'       => array(
                esc_html__( 'Solid', 'Stories' )  => 'solid',
                esc_html__( 'Three Dot', 'Stories' )  => 'three-dot',
                esc_html__( 'Zigzag', 'Stories' )  => 'zigzag',
            )
        );
 
		$this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'seperator_width',
			'heading'     => esc_html__( 'Width', 'Stories' ),
            'description' => esc_html__( 'Set Width with unit (Ex: 36px or 10%).', 'Stories' ),            
            'dependency'    => array('element' => 'seperator_type', 'value' => array('solid')),
        );

        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'seperator_size',
            'heading'     => esc_html__( 'Seperator Size', 'Stories' ),
            'std'     => 'thin',
            'value'       => array(
                esc_html__( 'Thin', 'Stories' )  => 'thin',
                esc_html__( 'Normal', 'Stories' )  => 'normal',
                esc_html__( 'Big', 'Stories' )  => 'big',
                esc_html__( 'Bigger', 'Stories' )  => 'bigger',
            )
        );

		$this->options[] = array(
			'type'        => 'colorpicker',
			'param_name'  => 'seperator_color',
			'heading'     => esc_html__( 'Seperator Color', 'Stories' ),
			'description' => esc_html__( 'Set Seperator color.', 'Stories' ),
            'group'       => esc_html__( 'Design', 'Stories' ),
            'std'       => '#888',
        );
        



        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'seperator_align',
            'heading'     => esc_html__( 'Align', 'Stories' ),
            'std'     => 'center',
            'value'       => array(
                esc_html__( 'Left', 'Stories' )  => 'left',
                esc_html__( 'Center', 'Stories' )  => 'center',
                esc_html__( 'Right', 'Stories' )  => 'right',
            )
        );


		$this->set_style_option();
	}


}
