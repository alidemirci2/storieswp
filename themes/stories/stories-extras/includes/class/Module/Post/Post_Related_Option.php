<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Related_Option extends ModuleOptionAbstract
{
    public function get_category()
    {
        return esc_html__('Stories - Post', 'Stories');
    }

    public function compatible_column()
    {
        return array( 1,2,3,4,5,6,7,8,9,10,11,12 );
    }

    public function get_module_name()
    {
        return esc_html__('Stories - Related Post', 'Stories');
    }

    public function set_options()
    {
        $this->set_post_option();
        $this->set_style_option();
    }

    public function set_post_option()
    {
	    $this->options[] = array(
		    'type'          => 'textfield',
		    'param_name'    => 'first_title',
		    'heading'       => esc_html__('First Title', 'Stories'),
		    'description'   => esc_html__('Insert text for first title.', 'Stories'),
		    'std'           => esc_html__( 'Related', 'Stories' )
	    );

	    $this->options[] = array(
		    'type'          => 'textfield',
		    'param_name'    => 'second_title',
		    'heading'       => esc_html__('Second Title', 'Stories'),
		    'description'   => esc_html__('Insert text for second title.', 'Stories'),
		    'std'           => esc_html__( ' Posts', 'Stories' )
	    );

        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'match',
            'heading'       => esc_html__('Related Post Filter', 'Stories'),
            'description'   => esc_html__('Select how related post will filter article.','Stories'),
            'std'           => '',
            'value'         => array(
                esc_html__('Category', 'Stories') => 'category',
                esc_html__('Tag', 'Stories')      => 'tag',
            )
        );

        $this->options[] = array(
            'type'          => 'radioimage',
            'param_name'    => 'header_type',
            'std'           => 'heading_6',
            'value'         => array(
                EPIC_URL . '/assets/img/admin/heading-1.png'  => 'heading_1',
                EPIC_URL . '/assets/img/admin/heading-2.png'  => 'heading_2',
                EPIC_URL . '/assets/img/admin/heading-3.png'  => 'heading_3',
                EPIC_URL . '/assets/img/admin/heading-4.png'  => 'heading_4',
                EPIC_URL . '/assets/img/admin/heading-5.png'  => 'heading_5',
                EPIC_URL . '/assets/img/admin/heading-6.png'  => 'heading_6',
                EPIC_URL . '/assets/img/admin/heading-7.png'  => 'heading_7',
                EPIC_URL . '/assets/img/admin/heading-8.png'  => 'heading_8',
                EPIC_URL . '/assets/img/admin/heading-9.png'  => 'heading_9',
            ),
            'heading'       => esc_html__('Header Type', 'Stories'),
            'description'   => esc_html__('Choose which header type fit with your content design.', 'Stories'),
        );

        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'pagination',
            'heading'       => esc_html__('Related Pagination Style', 'Stories'),
            'description'   => esc_html__('Adjust how related post will shown.','Stories'),
            'std'           => '',
            'value'         => array(
                esc_html__('No Pagination', 'Stories')        => 'disable',
                esc_html__('Next Prev', 'Stories')            => 'nextprev',
                esc_html__('Load More', 'Stories')            => 'loadmore',
                esc_html__('Auto Load on Scroll', 'Stories')  => 'scrollload',
            )
        );

        $this->options[] = array(
            'type'          => 'slider',
            'param_name'    => 'number',
            'heading'       => esc_html__('Number of Post', 'Stories'),
            'description'   => esc_html__('Set the number of post each related post load.', 'Stories'),
            'min'           => 2,
            'max'           => 10,
            'step'          => 1,
            'std'           => 5,
        );

        $this->options[] = array(
            'type'          => 'slider',
            'param_name'    => 'auto_load',
            'heading'       => esc_html__('Auto Load Limit', 'Stories'),
            'description'   => esc_html__('Limit of auto load when scrolling, set to zero to always load until end of content.', 'Stories'),
            'min'           => 0,
            'max'           => 500,
            'step'          => 1,
            'std'           => 3,
            'dependency'    => array('element' => 'pagination', 'value' => array('nextprev', 'loadmore', 'scrollload'))
        );

        $this->options[] = array(
            'type'          => 'radioimage',
            'param_name'    => 'template',
            'std'           => '9',
            'value'         => array(
                EPIC_URL . '/assets/img/admin/content-1.png' => '1' ,
                EPIC_URL . '/assets/img/admin/content-2.png' => '2' ,
                EPIC_URL . '/assets/img/admin/content-3.png' => '3' ,
                EPIC_URL . '/assets/img/admin/content-4.png' => '4' ,
                EPIC_URL . '/assets/img/admin/content-5.png' => '5' ,
                EPIC_URL . '/assets/img/admin/content-6.png' => '6' ,
                EPIC_URL . '/assets/img/admin/content-7.png' => '7' ,
                EPIC_URL . '/assets/img/admin/content-8.png' => '8' ,
                EPIC_URL . '/assets/img/admin/content-9.png' => '9' ,
                EPIC_URL . '/assets/img/admin/content-10.png' => '10',
                EPIC_URL . '/assets/img/admin/content-11.png' => '11',
                EPIC_URL . '/assets/img/admin/content-12.png' => '12',
                EPIC_URL . '/assets/img/admin/content-13.png' => '13',
                EPIC_URL . '/assets/img/admin/content-14.png' => '14',
                EPIC_URL . '/assets/img/admin/content-15.png' => '15',
                EPIC_URL . '/assets/img/admin/content-16.png' => '16',
                EPIC_URL . '/assets/img/admin/content-17.png' => '17',
                EPIC_URL . '/assets/img/admin/content-18.png' => '18',
                EPIC_URL . '/assets/img/admin/content-19.png' => '19',
                EPIC_URL . '/assets/img/admin/content-20.png' => '20',
                EPIC_URL . '/assets/img/admin/content-21.png' => '21',
                EPIC_URL . '/assets/img/admin/content-22.png' => '22',
                EPIC_URL . '/assets/img/admin/content-23.png' => '23',
                EPIC_URL . '/assets/img/admin/content-24.png' => '24',
                EPIC_URL . '/assets/img/admin/content-25.png' => '25',
                EPIC_URL . '/assets/img/admin/content-26.png' => '26',
                EPIC_URL . '/assets/img/admin/content-27.png' => '27',
            ),
            'heading'       => esc_html__('Related PostTemplate', 'Stories'),
            'description'   => esc_html__('Choose your related post template.', 'Stories'),
        );

        $this->options[] = array(
            'type'          => 'slider',
            'param_name'    => 'excerpt',
            'heading'       => esc_html__('Excerpt Length', 'Stories'),
            'description'   => esc_html__('Set word length of excerpt on related post.', 'Stories'),
            'min'           => 0,
            'max'           => 200,
            'step'          => 1,
            'std'           => 20,
        );

        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'date',
            'heading'       => esc_html__('Related Post Date Format', 'Stories'),
            'description'   => esc_html__('Choose which date format you want to use for archive content.','Stories'),
            'std'           => 'default',
            'value'         => array(
                 esc_attr__( 'Relative Date/Time Format (ago)', 'Stories' ) => 'ago',
                 esc_attr__( 'WordPress Default Format', 'Stories' ) => 'default',
                 esc_attr__( 'Custom Format', 'Stories' ) => 'custom',
            )
        );

        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'date_custom',
            'heading'       => esc_html__('Custom Date Format for Related Post', 'Stories'),
            'description'   => wp_kses(sprintf(__("Please set your date format for related post content, for more detail about this format, please refer to
                        <a href='%s' target='_blank'>Developer Codex</a>.", 'Stories'), "https://developer.wordpress.org/reference/functions/current_time/"),
                wp_kses_allowed_html()),
            'std'       => 'Y/m/d',
        );
    }

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'title_typography',
				'label'       => __( 'Title Typography', 'Stories' ),
				'description' => __( 'Set typography for post title', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_title > a,{{WRAPPER}} .jeg_block_title',
			]
		);

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'meta_typography',
				'label'       => __( 'Meta Typography', 'Stories' ),
				'description' => __( 'Set typography for post meta', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_meta, {{WRAPPER}} .jeg_post_meta .fa, {{WRAPPER}}.jeg_postblock .jeg_subcat_list > li > a:hover, {{WRAPPER}} .jeg_pl_md_card .jeg_post_category a, {{WRAPPER}}.jeg_postblock .jeg_subcat_list > li > a.current, {{WRAPPER}} .jeg_pl_md_5 .jeg_post_meta, {{WRAPPER}} .jeg_pl_md_5 .jeg_post_meta .fa, {{WRAPPER}} .jeg_post_category a',
			]
		);
	}
}
