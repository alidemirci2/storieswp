<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

Class Post_Meta_View extends PostViewAbstract {

	/** Backend */
	public function render_module_back( $attr, $column_class ) {
		$left_html = $right_html = '';

		$style = $this->generate_style( $attr );


		$lefts = is_array( $attr['meta_left'] ) ? $attr['meta_left'] : explode( ',', $attr['meta_left'] );
		foreach ( $lefts as $left ) {
			$left_html .= $this->render_meta_back( $left, $attr );
		}
		$left_html = "<div class='meta_left'>{$left_html}</div>";

		$rights = is_array( $attr['meta_right'] ) ? $attr['meta_right'] : explode( ',', $attr['meta_right'] );
		foreach ( $rights as $right ) {
			$right_html .= $this->render_meta_back( $right, $attr );
		}
		$right_html = "<div class='meta_right'>{$right_html}</div>";

		if ( isset( $attr['vertical_line'] ) && $attr['vertical_line'] == "yes") {
			$attr['vertical_line'] = "line";
		}

			return
				"<div {$this->element_id($attr)} class='jeg_custom_meta_wrapper {$attr['meta_style']} {$attr['vertical_line']} {$attr['align']} {$attr['scheme']} {$attr['el_class']}'>
					{$style}
					<div class='jeg_post_meta'>
						" . $left_html . $right_html . "
					</div>
				</div>";
	}

	public function render_meta_back( $meta, $attr ) {
		if ( ! empty( $meta ) ) {
			$func = "render_" . $meta . "_back";

			return $this->$func( $attr );
		}
	}

	public function render_date_back( $attr ) {

		if($attr['meta_style'] == "vertical" ){

				if($attr['vertical_text'] == "yes"){
					$meta_label1 = "<span class=\"meta_label\">". esc_html__('CREATED','Stories') ."</span>";				
					$meta_label2 = "<span class=\"meta_label\">". esc_html__('UPDATED','Stories') ."</span>";
				}

				$publish_date = "
				<div class=\"jeg_meta_date\">
				{$meta_label1}
				<a href=\"" . get_the_permalink() . "\">" . get_the_date() . "</a>
				</div>
				";
			
				$modified_date = "
				<div class=\"jeg_meta_update\">
				{$meta_label2}
				<a href=\"" . get_the_permalink() . "\">" . get_the_modified_date() . "</a>
				</div>
				";
				

			if($attr['post_date'] == "publish" ){

				return $publish_date;

			}else if($attr['post_date'] == "modified" ){
				
				return $modified_date;

			}else if($attr['post_date'] == "both" ){
				
				return $publish_date.$modified_date;

			}

		}else if($attr['meta_style'] == "line" ){

			if($attr['post_date'] == "publish" ){
				$date = get_the_date();
			}else if($attr['post_date'] == "modified" ){
				$date = get_the_modified_date();
			}else{
				$date = get_the_date()." - " . esc_html__("Updated: ","Stories") . get_the_modified_date();
			}
		
			return
			"<div class=\"jeg_meta_date\">
				<a href=\"" . get_the_permalink() . "\">" . esc_html( $date ) . "</a>
			</div>
			";

		}
		
	}



	public function render_category_back( $attr ) {

	if ( isset( $attr['vertical_text'] ) && $attr['vertical_text'] == "yes"  && $attr['meta_style'] == "vertical" ) {
	$meta_label = "<span class=\"meta_label\">". esc_html__('CATEGORY','Stories') ."</span>";
	}

		return
			"<div class=\"jeg_meta_category\">
				{$meta_label}
                <span>
                    <a href=\"#\" rel=\"category tag\">Dummy</a>, 
                    <a href=\"#\" rel=\"category tag\">Another</a>, 
                    <a href=\"#\" rel=\"category tag\">Category</a>            
                </span>
            </div>";
	}

	public function render_comment_back( $attr ) {

		if ( isset( $attr['vertical_text'] ) && $attr['vertical_text'] == "yes"  && $attr['meta_style'] == "vertical" ) {
		$meta_label = "<span class=\"meta_label\">". esc_html__('COMMENT','Stories') ."</span>";
		}

		// improve: svg text vertial align

		return "
			<div class=\"jeg_meta_comment\">
				{$meta_label}
				<a href=\"/#respond\">" . file_get_contents(THEME_URI  . '/assets/img/comment-alt-regular.svg') ." 0</a>
			</div>";
	}

	public function render_author_back( $attr ) {
		$avatar = '';
		if ( $attr['show_avatar'] ) {
			$avatar = "<img alt=\"admin\" src=\"http://0.gravatar.com/avatar/c11de68b2a02b55b9a09abb3323eb852?s=100&d=mm&r=g\" class=\"avatar avatar-80 photo\" data-pin-no-hover=\"true\" width=\"80\" height=\"80\">";
		}

		if ( isset( $attr['vertical_text'] ) && $attr['vertical_text'] == "yes" && $attr['meta_style'] == "vertical" ) {
		$meta_label = "<span class=\"meta_label\">". esc_html__('AUTHOR','Stories') ."</span>";
		}

		return
			"<div class=\"jeg_meta_author\"> 
			{$meta_label}
			{$avatar}" .
			"<a href='#'>admin</a>" .
			"</div>";
	}

	/** Frontend */
	public function render_module_front( $attr, $column_class ) {
		$left_html = $right_html = '';

		$style = $this->generate_style( $attr );

		// add_filter('theme_mod_global_post_date', function() use ($attr) {return $attr['post_date'];});

		$lefts = is_array( $attr['meta_left'] ) ? $attr['meta_left'] : explode( ',', $attr['meta_left'] );
		foreach ( $lefts as $left ) {
			$left_html .= $this->render_meta( $left, $attr );
		}
		$left_html = "<div class='meta_left'>{$left_html}</div>";

		if ( isset( $attr['align'] ) && $attr['align'] != "center" ) {
		$rights = is_array( $attr['meta_right'] ) ? $attr['meta_right'] : explode( ',', $attr['meta_right'] );
		foreach ( $rights as $right ) {
			$right_html .= $this->render_meta( $right, $attr );
		}
		$right_html = "<div class='meta_right'>{$right_html}</div>";
		}

		if ( isset( $attr['vertical_line'] ) && $attr['vertical_line'] == "yes") {
			$attr['vertical_line'] = "line";
		}

		return
			"<div {$this->element_id($attr)} class='jeg_custom_meta_wrapper {$attr['meta_style']} {$attr['vertical_line']} {$attr['align']} {$attr['scheme']} {$attr['el_class']}'>
				<div class='jeg_post_meta'>
				{$style}
					" . $left_html . $right_html . "
				</div>
			</div>";
	}


	


	public function render_meta( $meta, $attr ) {
		if ( ! empty( $meta ) ) {
			$func = "render_" . $meta;

			return $this->$func( $attr );
		}
	}

	public function render_date( $attr ) {

		if($attr['meta_style'] == "vertical" ){

				if($attr['vertical_text'] == "yes"){
					$meta_label1 = "<span class=\"meta_label\">". esc_html__('CREATED','Stories') ."</span>";				
					$meta_label2 = "<span class=\"meta_label\">". esc_html__('UPDATED','Stories') ."</span>";
				}

				$publish_date = "
				<div class=\"jeg_meta_date\">
				{$meta_label1}
				<a href=\"" . get_the_permalink() . "\">" . get_the_date() . "</a>
				</div>
				";
			
				$modified_date = "
				<div class=\"jeg_meta_update\">
				{$meta_label2}
				<a href=\"" . get_the_permalink() . "\">" . get_the_modified_date() . "</a>
				</div>
				";
				

			if($attr['post_date'] == "publish" ){

				return $publish_date;

			}else if($attr['post_date'] == "modified" ){
				
				return $modified_date;

			}else if($attr['post_date'] == "both" ){
				
				return $publish_date.$modified_date;

			}

		}else if($attr['meta_style'] == "line" ){

			if($attr['post_date'] == "publish" ){
				$date = get_the_date();
			}else if($attr['post_date'] == "modified" ){
				$date = get_the_modified_date();
			}else{
				$date = get_the_date()." - " . esc_html__("Updated: ","Stories") . get_the_modified_date();
			}
		
			return
			"<div class=\"jeg_meta_date\">
				<a href=\"" . get_the_permalink() . "\">" . esc_html( $date ) . "</a>
			</div>
			";

		}


	}
	

	public function render_category( $attr ) {

		if ( isset( $attr['vertical_text'] ) && $attr['vertical_text'] == "yes" && $attr['meta_style'] == "vertical" ) {
		$meta_label = "<span class=\"meta_label\">". esc_html__('CATEGORY','Stories') ."</span>";
		}

		return
			"<div class=\"jeg_meta_category\">
				{$meta_label}
                <span>
                    " . get_the_category_list( ', ' ) . " 
                </span>
            </div>";
	}

	public function render_comment( $attr ) {

		if ( isset( $attr['vertical_text'] ) && $attr['vertical_text'] == "yes" && $attr['meta_style'] == "vertical" ) {
		$meta_label = "<span class=\"meta_label\">". esc_html__('CREATED DATE','Stories') ."</span>";
		}

		// improve: svg text vertial align
		return "
		<div class=\"jeg_meta_comment\">
			{$meta_label}
			<a href=\"/#respond\">" . file_get_contents(THEME_URI  . '/assets/img/comment-alt-regular.svg') . " " . esc_html( get_comments_number() ) . "</a>
		</div>";
	}



	public function render_author( $attr ) {
		global $post;
		$author_image = "";

		if ( isset( $attr['show_avatar'] ) && $attr['show_avatar']) {
			$author_image = get_avatar( get_the_author_meta( 'ID', $post->post_author ), 80, null, get_the_author_meta( 'display_name', $post->post_author ) );
		}

		if ( isset( $attr['vertical_text'] ) && $attr['vertical_text'] == "yes" && $attr['meta_style'] == "vertical" ) {
		$meta_label = "<span class=\"meta_label\">". esc_html__('AUTHOR','Stories') ."</span>";
		}

		return
			"<div class=\"jeg_meta_author\"> 
			{$meta_label}
			{$author_image} 
			" . epic_the_author_link( $post->post_author, false ) .
			"</div>";
	}



		public function generate_style( $attr ) {

		$unique_class = $result = $style = "";

		
			
			if ($attr['meta_seperator_type'] == "times") { 
				$style .= ".jeg_post_meta .jeg_meta_date:before {   }"; 
			} else if($attr['meta_seperator_type'] == "dot") { 
				$style .= ".jeg_post_meta .jeg_meta_date:before {  content:''; border-radius:50%; width:2px; height:2px; background:rgba(0,0,0,.3); display:inline-block;}"; 
			} else if($attr['meta_seperator_type'] == "hyphens") { 
				$style .= ".jeg_post_meta .jeg_meta_date:before {  content:'';  width:4px; height:1px; background:rgba(0,0,0,.2); display:inline-block;}"; 
			}else if($attr['meta_seperator_type'] == "vline") { 
				$style .= ".jeg_post_meta .jeg_meta_date:before {  content:'';  width:1px; height:9px; background:rgba(0,0,0,.2); display:inline-block;}"; 
			} else { 
				$style .= ".jeg_post_meta .jeg_meta_date:before {  content:'';  width:1px; height:9px; background:rgba(0,0,0,.2); display:inline-block; transform:rotate(18deg); }"; 
			}

			if ( isset( $attr['meta_color'] ) && $attr['meta_color']) {
			$style .= ".jeg_post_meta, .jeg_meta_comment svg{ color: {$attr['meta_color']}; fill: {$attr['meta_color']} } ";
			}

		if ( $style ) {
			$result = '<style>' . $style . '</style>';
		}

		return $result;

		}



}
