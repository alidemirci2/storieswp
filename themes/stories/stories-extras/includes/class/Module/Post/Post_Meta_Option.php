<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Meta_Option extends ModuleOptionAbstract {
	public function get_category() {
		return esc_html__( 'Stories - Post', 'Stories' );
	}

	public function compatible_column() {
		return array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
	}

	public function get_module_name() {
		return esc_html__( 'Stories - Post Meta', 'Stories' );
	}

	public function set_options() {
		$this->set_post_option();
		$this->set_style_option();
	}

	public function set_post_option() {
		$this->options[] = array(
			'type'        => 'select',
			'multiple'    => PHP_INT_MAX,
			'param_name'  => 'meta_left',
			'heading'     => esc_html__( 'Left Meta Element', 'Stories' ),
			'description' => esc_html__( 'Pick element you want to add on meta wrapper.', 'Stories' ),
			'group'       => esc_html__( 'Meta Option', 'Stories' ),
			'std'         => '',
			'value'       => array(
				esc_html__( 'Author', 'Stories' )   => 'author',
				esc_html__( 'Date', 'Stories' )     => 'date',
				esc_html__( 'Category', 'Stories' ) => 'category',
				esc_html__( 'Comment', 'Stories' )  => 'comment',
			)
		);

		$this->options[] = array(
			'type'        => 'select',
			'multiple'    => PHP_INT_MAX,
			'param_name'  => 'meta_right',
			'heading'     => esc_html__( 'Right Meta Element', 'Stories' ),
			'description' => esc_html__( 'Pick element you want to add on meta wrapper.', 'Stories' ),
			'group'       => esc_html__( 'Meta Option', 'Stories' ),
			'dependency'    => array(
				// Improve Iki degisen icin dependency
				'element' => 'align', 'value' => array('left','right'),
				'element' => 'meta_style', 'value' => array('line')
			),
			'std'         => '',
			'value'       => array(
				esc_html__( 'Author', 'Stories' )   => 'author',
				esc_html__( 'Date', 'Stories' )     => 'date',
				esc_html__( 'Category', 'Stories' ) => 'category',
				esc_html__( 'Comment', 'Stories' )  => 'comment',
			)
		);

        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'align',
            'heading'       => esc_html__('Align','Stories'),
			'group'         => esc_html__('Meta Option', 'Stories'), 
            'std'           => 'center',
            'value'         => array(                       
				esc_html__('Left', 'Stories') => 'left', 
				esc_html__('Center', 'Stories') => 'center', 
            )
        ); 


		$this->options[] = array(
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'Show avatar image on author element', 'Stories' ),
			'param_name' => 'show_avatar',
			'group'      => esc_html__( 'Author', 'Stories' ),
			'value'      => array( esc_html__( "Show avatar image.", 'Stories' ) => 'yes' ),
			'std'        => 'no',
		);

		$this->options[] = array(
			'type'        => 'dropdown',
			'param_name'  => 'post_date',
			'heading'     => esc_html__( 'Post Date', 'Stories' ),
			'description' => esc_html__( 'Choose which post date type that you want to show.', 'Stories' ),
			'group'       => esc_html__( 'Date', 'Stories' ),
			'std'         => 'modified',
			'value'       => array(
				esc_html__( 'Modified Date', 'Stories' )  => 'modified',
				esc_html__( 'Published Date', 'Stories' ) => 'publish',
				esc_html__( 'Show Both', 'Stories' ) => 'both',
			)
		);
        
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'meta_seperator_type',
            'heading'       => esc_html__('Meta Seperator Type','Stories'),
			'description'   => esc_html__('Choose Meta Seperator Type.','Stories'), 
			'dependency'    => array('element' => 'meta_style', 'value' => array('line')),
			'group'         => esc_html__('Meta Option', 'Stories'), 
            'std'           => 'times',
            'value'         => array(                       
				esc_html__('Vertical Line', 'Stories')                 => 'vline', 
				esc_html__('Hyphens', 'Stories')                 => 'hyphens',
                esc_html__('Times', 'Stories')               => 'times',
                esc_html__('Dot', 'Stories')      => 'dot',
                esc_html__('Slash', 'Stories')                 => 'slash',
                esc_html__('None', 'Stories')                 => 'none',
            )
        ); 
       $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'meta_style',
            'heading'       => esc_html__('Meta Style','Stories'),
			'group'         => esc_html__('Design', 'Stories'), 
			'std'           => 'line',
            'value'         => array(                       
				esc_html__('Line Style', 'Stories') => 'line', 
				esc_html__('Vertical Style', 'Stories') => 'vertical', 
            )
		); 

		$this->options[] = array(
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'Add Line to Vertical Style', 'Stories' ),
			'param_name' => 'vertical_line',
			'group'      => esc_html__( 'Design', 'Stories' ),		
			'dependency'    => array('element' => 'meta_style', 'value' => array('vertical')),
			'value'      => array( esc_html__( "Show Line between blocks.", 'Stories' ) => 'yes' ),
			'std'        => 'no',
		);

		$this->options[] = array(
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'Add Text to Vertical Style', 'Stories' ),
			'param_name' => 'vertical_text',
			'group'      => esc_html__( 'Design', 'Stories' ),		
			'dependency'    => array('element' => 'meta_style', 'value' => array('vertical')),
			'value'      => array( esc_html__( "Show Line between blocks.", 'Stories' ) => 'yes' ),
			'std'        => 'no',
		);

        $this->options[] = array(
            'type'          => 'colorpicker',
            'param_name'    => 'meta_color',
            'heading'       => esc_html__('Meta Color','Stories'),
            'description'   => esc_html__('Choose Meta Color.','Stories'),
            'group'         => esc_html__('Design', 'Stories'), 
            'std'           => ''
		); 


	}

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'meta_typography',
				'label'    => __( 'Typography', 'Stories' ),
				'selector' => '{{WRAPPER}} .jeg_post_meta, {{WRAPPER}} .jeg_post_meta a',
			]
		);
	}
}
