<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Prev_Option extends ModuleOptionAbstract {
	public function get_category() {
		return esc_html__( 'Stories - Post', 'Stories' );
	}

	public function compatible_column() {
		return array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
	}

	public function get_module_name() {
		return esc_html__( 'Stories - Previous Post', 'Stories' );
	}

	public function set_options() {
	

        $this->options[] = array(
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'Title Visibility', 'Stories' ),
			'param_name' => 'show_title',
			'value'      => array( esc_html__( "Show Title", 'Stories' ) => 'yes' ),
			'std'        => 'yes',
        );

		$this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'nav_font_size',
			'heading'     => esc_html__( 'Nav Text Font Size', 'Stories' ),
			'description' => esc_html__( 'Set font size with unit (Ex: 36px or 4em) for nav text.', 'Stories' ),
			'group'       => esc_html__( 'Design', 'Stories' ),
		);

		$this->options[] = array(
			'type'        => 'colorpicker',
			'param_name'  => 'nav_color',
			'heading'     => esc_html__( 'Nav Text Color', 'Stories' ),
			'description' => esc_html__( 'Set nav text color.', 'Stories' ),
			'group'       => esc_html__( 'Design', 'Stories' ),
		);

		$this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'title_font_size',
			'heading'     => esc_html__( 'Post Title Font Size', 'Stories' ),
			'description' => esc_html__( 'Set font size with unit (Ex: 36px or 4em) for post title.', 'Stories' ),
			'group'       => esc_html__( 'Design', 'Stories' ),
		);

		$this->options[] = array(
			'type'        => 'colorpicker',
			'param_name'  => 'title_color',
			'heading'     => esc_html__( 'Post Title Color', 'Stories' ),
			'description' => esc_html__( 'Set post title text color.', 'Stories' ),
			'group'       => esc_html__( 'Design', 'Stories' ),
		);

		$this->set_style_option();
	}

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'label'    => __( 'Title Typography', 'Stories' ),
				'selector' => '{{WRAPPER}} .jeg_prev_next_post .prev_text_title .post-title',
			]
		);

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'nav_typography',
				'label'    => __( 'Nav Text Typography', 'Stories' ),
				'selector' => '{{WRAPPER}} .jeg_prev_next_post .caption',
			]
		);
	}
}
