<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

Class Post_Title_View extends PostViewAbstract {

	public function render_module_back( $attr, $column_class ) {

		$style = $this->generate_style( $attr );

		return
			"<div {$this->element_id($attr)} class='stories_custom_title_wrapper {$attr['scheme']} {$attr['el_class']}'>
				{$style}
                <h1 class=\"stories_post_title\">This is dummy title and will be replaced with real title of your post</h1>
            </div>";
	}

	public function render_module_front( $attr, $column_class ) {

		$style = $this->generate_style( $attr );

		return
			"<div {$this->element_id($attr)} class='stories_custom_title_wrapper {$attr['scheme']} {$attr['el_class']}'>
				{$style}
                <h1 class=\"stories_post_title\">" . get_the_title() . "</h1>
            </div>";
	}

	public function generate_style( $attr ) {

		$result = $wrapper = '';

		if ( isset( $attr['title_color'] ) && $attr['title_color'] ) {
			$result .= 'color: ' . $attr['title_color'] . ';';
		}

		if ( isset( $attr['font_size'] ) && $attr['font_size'] ) {
			$result .= 'font-size: ' . $attr['font_size'] . ';';
		}

		if ( isset( $attr['title_width'] ) && $attr['title_width'] ) {
		
			$result .= "width:{$attr['title_width']};";

		}

		if ( isset( $attr['title_align'] ) && $attr['title_align'] ) {

			switch ($attr['title_align']) {
				case 'left':
					$title_align = "flex-start";
					break;
				case 'center':
					$title_align = "center";
					break;
				case 'right':
					$title_align = "flex-end";
					break;
				
				default:
					$title_align = "center";
					break;
			}

			$wrapper .= "justify-content:{$title_align}; text-align:{$attr['title_align']};";

		}



		// Improve: Bos deger gozukmesini engelle
		if ( $result ||  $wrapper ) {
			$result = '<style>' . $this->element_id( $attr ) . ' 
			.stories_custom_title_wrapper   {' . $wrapper . '}
			.stories_post_title {' . $result . '} 
			 ' . '</style>';
		}

		return $result;
	}
}
