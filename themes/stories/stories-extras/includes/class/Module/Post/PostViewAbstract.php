<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Post;

use EPIC\Module\ModuleViewAbstract;

abstract Class PostViewAbstract extends ModuleViewAbstract {

    public function is_on_editor() {

        if ( function_exists('jeg_is_frontend_vc') && jeg_is_frontend_vc() )
            return true;

        if ( isset( $_REQUEST['action'] ) ) {

            if ( ( $_REQUEST['action'] === 'elementor' || $_REQUEST['action'] === 'elementor_ajax' ) )
                return true;
        }

        return false;
    }

    public function render_module($attr, $column_class) {

        if ( $this->is_on_editor() ) {
            return $this->render_module_back($attr, $column_class);
        } else {
            return $this->render_module_front($attr, $column_class);
        }
    }



 public function set_header_filter_style_options( $instance ) { 
	    $instance->add_group_control(
		    \Elementor\Group_Control_Typography::get_type(),
		    [
			    'name'        => 'header_filter_item_typography',
			    'label'       => __( 'Header Filter Item Typography', 'Stories' ),
			    'description' => __( 'Set typography for Header Filter Items', 'Stories' ),
                'selector'    => '{{WRAPPER}} .jeg_subcat a',
		    ]
        );   
    
        $instance->add_responsive_control(
            'header_filter_padding',
            [
                'label' => __( 'Header Filter Padding', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .stories_block_heading .jeg_subcat' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
                ],
            ]
            
        );
    
        $instance->add_responsive_control(
            'header_filter_margin',
            [
                'label' => __( 'Header Filter Margin', 'Stories' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .stories_block_heading .jeg_subcat' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
                ],
            ]
            
        );
    }



    public abstract function render_module_back($attr, $column_class);
    public abstract function render_module_front($attr, $column_class);
}
