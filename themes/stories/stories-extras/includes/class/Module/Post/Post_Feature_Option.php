<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Feature_Option extends ModuleOptionAbstract
{
    public function get_category()
    {
        return esc_html__('Stories - Post', 'Stories');
    }

    public function compatible_column()
    {
        return array( 1,2,3,4,5,6,7,8,9,10,11,12 );
    }

    public function get_module_name()
    {
        return esc_html__('Stories - Post Featured Image', 'Stories');
    }

    public function set_options()
    {
        $this->set_post_option();
        $this->set_style_option();
    }

    public function set_post_option()
    {
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'image_size',
            'heading'       => esc_html__('Featured Image Size', 'Stories'),
            'description'   => esc_html__('choose which feature image size','Stories'),
            'std'           => '',
            'value'         => array(
                esc_html__('1140x570', 'Stories')                => '1140x570',
                esc_html__('750x375', 'Stories')                 => '750x375',
                esc_html__('1140x815', 'Stories')                => '1140x815',
                esc_html__('750x536', 'Stories')                 => '750x536',
                esc_html__('Width 1140', 'Stories')              => 'featured-1140',
                esc_html__('Width 750', 'Stories')               => 'featured-750',
                esc_html__('Full Width', 'Stories')              => 'full',
            )
        );

//        $this->options[] = array(
//            'type'          => 'dropdown',
//            'param_name'    => 'gallery_size',
//            'heading'       => esc_html__('Gallery Image Size', 'Stories'),
//            'description'   => esc_html__('choose which gallery image size','Stories'),
//            'std'           => '',
//            'value'         => array(
//                esc_html__('1140x570', 'Stories')                => '1140x570',
//                esc_html__('750x375', 'Stories')                 => '750x375',
//                esc_html__('1140x815', 'Stories')                => '1140x815',
//                esc_html__('750x536', 'Stories')                 => '750x536',
//                esc_html__('Width 1140', 'Stories')              => 'featured-1140',
//                esc_html__('Width 750', 'Stories')               => 'featured-750',
//            )
//        );
    }
}
