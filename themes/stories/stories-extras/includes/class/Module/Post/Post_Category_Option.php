<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

use EPIC\Module\ModuleOptionAbstract;

Class Post_Category_Option extends ModuleOptionAbstract {
	public function get_category() {
		return esc_html__( 'Stories - Post', 'Stories' );
	}

	public function compatible_column() {
		return array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
	}

	public function get_module_name() {
		return esc_html__( 'Stories - Post Category', 'Stories' );
    }
    public function set_options()
    { 
        $this->set_category_options1();
        $this->set_category_options2(); 
    }
	public function set_category_options1() {
 

        $this->options[] = array(
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'Reading Time Visibility', 'Stories' ),
			'param_name' => 'reading_time',
			'value'      => array( esc_html__( "Show Reading Icon.", 'Stories' ) => 'yes' ),
			'std'        => 'yes',
		);

		$this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'font_size',
			'heading'     => esc_html__( 'Font Size', 'Stories' ),
			'description' => esc_html__( 'Set font size with unit (Ex: 36px or 4em).', 'Stories' ),
            'group'         => esc_html__('Category Typography', 'Stories') ,
        );
    }
    
    public function set_post_typography_option( $instance ) {
        $instance->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name'     => 'title_typography',
                'label'    => __( 'Typography', 'Stories' ),
                'selector' => '{{WRAPPER}} .jeg_post_category a, .jeg_post_reading_time', 
            ]
        );
    }

    public function set_category_options2() {


        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'text_color_options',
            'heading'     => esc_html__( 'Text Color', 'Stories' ),
            'description' => esc_html__( 'Choose element color for your Category Text ', 'Stories' ),
            'group'       => esc_html__( 'Design', 'Stories' ),
            'std'     => 'inherit',
            'value'       => array(
                esc_html__( 'Inherit', 'Stories' ) => 'inherit',
                esc_html__( 'Category Style', 'Stories' )  => 'category-style',
                esc_html__( 'Custom', 'Stories' )  => 'custom'
            )
        );

		$this->options[] = array(
			'type'        => 'colorpicker',
			'param_name'  => 'text_color',
			'heading'     => esc_html__( 'Text Color', 'Stories' ),
			'description' => esc_html__( 'Set text color.', 'Stories' ),
            'group'       => esc_html__( 'Design', 'Stories' ),
            'dependency'    => array('element' => 'text_color_options', 'value' => array('custom')),
		);

        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'text_back_options',
            'heading'     => esc_html__( 'Background Color', 'Stories' ),
            'description' => esc_html__( 'Choose element color for your Category Background ', 'Stories' ),
            'group'       => esc_html__( 'Design', 'Stories' ),
            'std'     => 'custom',
            'value'       => array(
                esc_html__( 'Custom', 'Stories' )  => 'custom',
                esc_html__( 'Category Style', 'Stories' )  => 'category-style',
            )
        );

		$this->options[] = array(
			'type'        => 'colorpicker',
			'param_name'  => 'bg_color',
			'heading'     => esc_html__( 'Background Color', 'Stories' ),
            'description' => esc_html__( 'Set background color.', 'Stories' ),
            'dependency'    => array('element' => 'text_back_options', 'value' => array('custom')),
			'group'       => esc_html__( 'Design', 'Stories' ),
        );


        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'padding',
            'heading'     => esc_html__( 'Add Padding?', 'Stories' ),
            'group'       => esc_html__( 'Design', 'Stories' ),
            'std'     => 'no',            
            'description' => esc_html__( 'Add Padding in Category Link individually. You can add Margin to Category Container in Advanced Settings.', 'Stories' ),
            'value'       => array(
                esc_html__( 'Yes', 'Stories' )  => 'yes',
                esc_html__( 'No', 'Stories' )  => 'no',
            )
        );

        $this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'padding_left',
            'heading'     => esc_html__( 'Padding Left', 'Stories' ),
            'std'         => '',
            'description' => esc_html__( 'Write px,em or %. Exp: 20px', 'Stories' ),
            'dependency'  => array('element' => 'padding', 'value' => array('yes')),
			'group'       => esc_html__( 'Design', 'Stories' ),
        );

        $this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'padding_top',
            'heading'     => esc_html__( 'Padding Top', 'Stories' ),
            'std'         => '',
            'description' => esc_html__( 'Write px,em or %. Exp: 20px', 'Stories' ),
            'dependency'  => array('element' => 'padding', 'value' => array('yes')),
			'group'       => esc_html__( 'Design', 'Stories' ),
        );

        $this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'padding_right',
            'heading'     => esc_html__( 'Padding Right', 'Stories' ),
            'std'         => '',
            'description' => esc_html__( 'Write px,em or %. Exp: 20px', 'Stories' ),
            'dependency'  => array('element' => 'padding', 'value' => array('yes')),
			'group'       => esc_html__( 'Design', 'Stories' ),
        );

        $this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'padding_bottom',
            'heading'     => esc_html__( 'Padding Bottom', 'Stories' ),
            'std'         => '',
            'description' => esc_html__( 'Write px,em or %. Exp: 20px', 'Stories' ),
            'dependency'  => array('element' => 'padding', 'value' => array('yes')),
			'group'       => esc_html__( 'Design', 'Stories' ),
        ); 

        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'border_options',
            'heading'     => esc_html__( 'Border Type', 'Stories' ),
            'group'       => esc_html__( 'Design', 'Stories' ),
            'std'     => 'none',
            'value'       => array(
                esc_html__( 'None', 'Stories' )  => '',
                esc_html__( 'Solid', 'Stories' )  => 'solid',
                esc_html__( 'Double', 'Stories' )  => 'double',
                esc_html__( 'Dotted', 'Stories' )  => 'dotted',
                esc_html__( 'Dashed', 'Stories' )  => 'dashed',
                esc_html__( 'Groove', 'Stories' )  => 'groove',
            )
        );

        $this->options[] = array(
			'type'        => 'colorpicker',
			'param_name'  => 'border_color',
			'heading'     => esc_html__( 'Border Color', 'Stories' ),
            'description' => esc_html__( 'Set background color.', 'Stories' ),
            'dependency'    => array('element' => 'border_options', 'value' => array('solid','double','dotted','dashed','groove')),
			'group'       => esc_html__( 'Design', 'Stories' ),
        );

		$this->options[] = array(
			'type'        => 'number',
			'param_name'  => 'border_width',
			'heading'     => esc_html__( 'Border Width', 'Stories' ),
			'description' => esc_html__( 'Set border width', 'Stories' ),
			'group'       => esc_html__( 'Design', 'Stories' ),
        );

        $this->options[] = array(
            'type'        => 'dropdown',
            'param_name'  => 'align',
            'heading'     => esc_html__( 'Align', 'Stories' ),
            'group'       => esc_html__( 'Design', 'Stories' ),
            'std'     => 'center',
            'value'       => array(
                esc_html__( 'Left', 'Stories' )  => 'left',
                esc_html__( 'Center', 'Stories' )  => 'center',
                esc_html__( 'Right', 'Stories' )  => 'right',
            )
        );



        $this->options[] = array(
			'type'        => 'colorpicker',
			'param_name'  => 'reading_color',
			'heading'     => esc_html__( 'Reading Time Color', 'Stories' ),
			'group'       => esc_html__( 'Reading Time Styles', 'Stories' ),
        );

		$this->options[] = array(
			'type'        => 'textfield',
			'param_name'  => 'reading_font_size',
			'heading'     => esc_html__( 'Font Size', 'Stories' ),
            'description' => esc_html__( 'Set font size with unit (Ex: 36px or 4em).', 'Stories' ),			
            'group'       => esc_html__( 'Reading Time Styles', 'Stories' ),
        );

        $this->options[] = array(
			'type'       => 'checkbox',
			'heading'    => esc_html__( 'Show Icon', 'Stories' ),
			'param_name' => 'show_reading_icon',
			'group'      => esc_html__( 'Reading Time Styles', 'Stories' ),
			'value'      => array( esc_html__( "Show Reading Icon.", 'Stories' ) => 'yes' ),
			'std'        => 'yes',
		);

        $this->set_style_option();


    }



}
