<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Module\Post;

Class Post_Category_View extends PostViewAbstract {
	public function render_module_back( $attr, $column_class ) {

		$style = $this->generate_style( $attr );

		$reading_icon = "";		
		if ( isset( $attr['show_reading_icon'] ) && $attr['show_reading_icon']) {
			$reading_icon = file_get_contents(THEME_URI  . '/assets/img/reading-time.svg');
		}

		if ( isset( $attr['reading_time'] ) && $attr['reading_time']) {
		$reading_time = "
			<div class=\"jeg_post_reading_time\">
				{$reading_icon} 3 min
			</div>
		";
		}

		return
			"<div {$this->element_id($attr)} class='jeg_custom_category_wrapper {$attr['scheme']} {$attr['el_class']}'>
				{$style}
				<div class=\"post-category-reading-container\"> 
					<div class=\"jeg_post_category\">
						<a href=\"#\" rel=\"category\">Dummy</a>,
						<a href=\"#\" rel=\"category\">Category</a>
					</div>
					{$reading_time}
				</div>
            </div>";
	}


	public function render_module_front( $attr, $column_class ) {

		$style = $this->generate_style( $attr );

		if ( has_category() ) {
			$category = get_the_category_list( ', ' );

			// Improve Reading Time alincak.
			global $post;
			$reading_time = epic_module_post_reading_time(  $post->ID );

			// improve front ve back de bu kod tekrar ediyor.
			$reading_icon = "";		
			if ( isset( $attr['show_reading_icon'] ) && $attr['show_reading_icon']) {
				$reading_icon = file_get_contents(THEME_URI  . '/assets/img/reading-time.svg');
			}

			if ( isset( $attr['reading_time'] ) && $attr['reading_time']) {
				$reading_time = "
					<div class=\"jeg_post_reading_time\">
						{$reading_icon} 3 min
					</div>
				";
			}

			return
				"<div {$this->element_id($attr)} class='jeg_custom_category_wrapper {$attr['scheme']} {$attr['el_class']}'>
					{$style}
					<div class=\"post-category-reading-container\"> 
						<div class=\"jeg_post_category\">
							{$category}
						</div>
						{$reading_time}
					</div>
                </div>";
		}
	}
 
	public function generate_style( $attr ) {

		$result = $normal = $container_styles = $hover = $color = $reading = '';


	// Font Size
		if ( isset( $attr['font_size'] ) && $attr['font_size']) {
			$normal .= 'font-size: ' . $attr['font_size'] . ';';
		}


	// Category Text Color
		if ( isset( $attr['text_color_options'] ) && $attr['text_color_options'] == "inherit" ) {
			// improve: Main Color Alincak.
			$normal .= 'color: ' . get_theme_mod('epic-ne[module-title-color]') . ';';
		}else if ( isset( $attr['text_color_options'] ) && $attr['text_color_options'] == "category-style" ){
			// improve: Burada epicin category stilleri cekilecek.
		}else if ( isset( $attr['text_color_options'] ) && $attr['text_color_options'] == "custom" ){
			$color .= 'color: ' . $attr['text_color'] . ';';
			$normal .= 'color: ' . $attr['text_color'] . ';';
		}


	// Category Background Color 
		if ( isset( $attr['text_back_options'] ) && $attr['text_back_options'] == "custom" ) {
			$normal .= 'padding: 5px 8px; background-color: ' . $attr['bg_color']. ';';
		}else if ( isset( $attr['text_back_options'] ) && $attr['text_back_options'] == "category-style" ){
			// improve: Burada epicin category stilleri cekilecek.
		}

		
	// Padding
		if ( isset( $attr['padding'] ) && $attr['padding'] == "yes") { 
		$paddingleft = $paddingtop = $paddingright = $paddingbottom = 0;
		if ( isset( $attr['padding_top'] ) && $attr['padding_top']) { $paddingtop = $attr['padding_top']; }
		if ( isset( $attr['padding_right'] ) && $attr['padding_right']) { $paddingright = $attr['padding_right']; }
		if ( isset( $attr['padding_bottom'] ) && $attr['padding_bottom']) { $paddingbottom = $attr['padding_bottom']; }
		if ( isset( $attr['padding_left'] ) && $attr['padding_left']) { $paddingleft = $attr['padding_left']; }

		$normal .= "padding: {$paddingtop} {$paddingright} {$paddingbottom} {$paddingleft};";

		}


	// Border Options
		if ( isset( $attr['border_options'] ) && $attr['border_options']) { 
			$normal .=  'border: ' . $attr["border_options"] . ' ' . $attr["border_color"] . ' '.  $attr["border_width"].'px;';
		}

	// Container Align
		if ( isset( $attr['align'] ) && $attr['align']) { 
			// Center
			if ( isset( $attr['align'] ) && $attr['align'] == "center" ) {
				$container_styles .=  'justify-content: center;';
			}
			// Right
			else if ( isset( $attr['align'] ) && $attr['align'] == "right" ) {
				$container_styles .=  'justify-content: flex-end;';
			}
			
		// Reading Time Color
			if ( isset( $attr['reading_color'] ) && $attr['reading_color']) {
				$reading .= 'color: ' . $attr['reading_color'] . '; fill: ' . $attr['reading_color'] . ';';
			}
		
		// Reading Time Font Size
			if ( isset( $attr['reading_font_size'] ) && $attr['reading_font_size']) {
				$reading .= 'font-size: ' . $attr['reading_font_size'] . ';';
			}
		
	

		}



		if ( $normal || $reading || $container_styles ) {
			$result .= $this->element_id( $attr ) . ' 
			.post-category-reading-container{' . $container_styles . '} 
			.jeg_post_category{' . $color . '}  
			.jeg_post_category a{' . $normal . '}  
			.jeg_post_reading_time {' . $reading . '}  
			';
		}



		// Hover Ornek olsun diye
		if ( isset( $attr['bg_color_hover'] ) && $attr['bg_color_hover'] ) {
			$hover .= 'background-color: ' . $attr['bg_color_hover'] . ';';
		}

		if ( $hover ) {
			$result .= $this->element_id( $attr ) . ' .jeg_post_tags a:hover{' . $hover . '}';
		}

		if ( $result ) {
			$result = '<style>' . $result . '</style>';
		}

		return $result;
	}
}
