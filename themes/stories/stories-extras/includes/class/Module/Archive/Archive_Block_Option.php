<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Archive;

use EPIC\Module\ModuleOptionAbstract;

Class Archive_Block_Option extends ModuleOptionAbstract
{
    public function get_category()
    {
        return esc_html__('EPIC - Archive', 'Stories');
    }

    public function compatible_column()
    {
        return array( 1,2,3,4,5,6,7,8,9,10,11,12 );
    }

    public function get_module_name()
    {
        return esc_html__('EPIC - Archive Block', 'Stories');
    }

    public function set_options()
    {
    	$this->set_general_option();
//	    $this->set_pagination_option();
        $this->set_style_option();
    }

    public function set_pagination_option() {
	    $this->options[] = array(
		    'type'          => 'dropdown',
		    'param_name'    => 'pagination_mode',
		    'heading'       => esc_html__('Pagination Mode', 'Stories'),
		    'description'   => esc_html__('Choose which pagination mode that fit with your block.', 'Stories'),
		    'group'         => esc_html__('Pagination', 'Stories'),
		    'std'           => 'nav_1',
		    'value'         => array(
			    esc_html__('Normal - Navigation 1', 'Stories') => 'nav_1',
			    esc_html__('Normal - Navigation 2', 'Stories') => 'nav_2',
			    esc_html__('Normal - Navigation 3', 'Stories') => 'nav_3',
//			    esc_html__('Ajax - Next Prev', 'Stories') => 'nextprev',
//			    esc_html__('Ajax - Load More', 'Stories') => 'loadmore',
//			    esc_html__('Ajax - Auto Scroll Load', 'Stories') => 'scrollload',
		    )
	    );

	    $this->options[] = array(
		    'type'          => 'slider',
		    'param_name'    => 'pagination_scroll_limit',
		    'group'         => esc_html__('Pagination', 'Stories'),
		    'heading'       => esc_html__('Auto Load Limit', 'Stories'),
		    'description'   => esc_html__('Limit of auto load when scrolling, set to zero to always load until end of content.', 'Stories'),
		    'min'           => 0,
		    'max'           => 99999,
		    'step'          => 1,
		    'std'           => 0,
		    'dependency'    => array( 'element' => 'pagination_mode', 'value' => array( 'scrollload' ) )
	    );

	    $this->options[] = array(
		    'type'          => 'dropdown',
		    'param_name'    => 'pagination_align',
		    'heading'       => esc_html__('Pagination Align', 'Stories'),
		    'description'   => esc_html__('Choose pagination alignment.', 'Stories'),
		    'group'         => esc_html__('Pagination', 'Stories'),
		    'std'           => 'nav_1',
		    'value'         => array(
			    esc_html__('Left', 'Stories') => 'left',
			    esc_html__('Center', 'Stories') => 'center',
		    ),
		    'dependency'    => array( 'element' => 'pagination_mode', 'value' => array( 'nav_1', 'nav_2', 'nav_3' ) )
	    );

	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'pagination_navtext',
		    'group'         => esc_html__('Pagination', 'Stories'),
		    'heading'       => esc_html__('Show Navigation Text', 'Stories'),
		    'value'         => array( esc_html__("Show navigation text (next, prev).", 'Stories') => 'yes' ),
		    'dependency'    => array( 'element' => 'pagination_mode', 'value' => array( 'nav_1', 'nav_2', 'nav_3' ) )
	    );

	    $this->options[] = array(
		    'type'          => 'checkbox',
		    'param_name'    => 'pagination_pageinfo',
		    'group'         => esc_html__('Pagination', 'Stories'),
		    'heading'       => esc_html__('Show Page Info', 'Stories'),
		    'value'         => array( esc_html__("Show page info text (Page x of y).", 'Stories') => 'yes' ),
		    'dependency'    => array( 'element' => 'pagination_mode', 'value' => array( 'nav_1', 'nav_2', 'nav_3' ) )
	    );
    }

	public function set_general_option()
	{
		$this->options[] = array(
			'type'          => 'radioimage',
			'param_name'    => 'block_type',
			'std'           => '3',
			'value'         => array(
				EPIC_URL . '/assets/img/admin/content-3.png'  => '3',
				EPIC_URL . '/assets/img/admin/content-4.png'  => '4',
				EPIC_URL . '/assets/img/admin/content-5.png'  => '5',
				EPIC_URL . '/assets/img/admin/content-6.png'  => '6',
				EPIC_URL . '/assets/img/admin/content-7.png'  => '7',
				EPIC_URL . '/assets/img/admin/content-9.png'  => '9',
				EPIC_URL . '/assets/img/admin/content-10.png'  => '10',
				EPIC_URL . '/assets/img/admin/content-11.png'  => '11',
				EPIC_URL . '/assets/img/admin/content-12.png'  => '12',
				EPIC_URL . '/assets/img/admin/content-14.png'  => '14',
				EPIC_URL . '/assets/img/admin/content-15.png'  => '15',
				EPIC_URL . '/assets/img/admin/content-18.png'  => '18',
				EPIC_URL . '/assets/img/admin/content-22.png'  => '22',
				EPIC_URL . '/assets/img/admin/content-23.png'  => '23',
				EPIC_URL . '/assets/img/admin/content-25.png'  => '25',
				EPIC_URL . '/assets/img/admin/content-26.png'  => '26',
				EPIC_URL . '/assets/img/admin/content-27.png'  => '27',
				EPIC_URL . '/assets/img/admin/content-32.png'  => '32',
				EPIC_URL . '/assets/img/admin/content-33.png'  => '33',
				EPIC_URL . '/assets/img/admin/content-34.png'  => '34',
				EPIC_URL . '/assets/img/admin/content-35.png'  => '35',
				EPIC_URL . '/assets/img/admin/content-36.png'  => '36',
				EPIC_URL . '/assets/img/admin/content-37.png'  => '37'
			),
			'heading'       => esc_html__('Block Type', 'Stories'),
			'description'   => esc_html__('Choose which block type that fit your content design.', 'Stories'),
		);

		$this->options[] = array(
			'type'          => 'slider',
			'param_name'    => 'number_post',
			'heading'       => esc_html__('Number of post', 'Stories'),
			'description'   => esc_html__('Set number of post for this block.', 'Stories'),
			'min'           => 1,
			'max'           => 100,
			'step'          => 1,
			'std'           => 5
		);

		$this->options[] = array(
			'type'          => 'checkbox',
			'param_name'    => 'boxed',
			'heading'       => esc_html__('Enable Boxed', 'Stories'),
			'value'         => array( esc_html__("This option will turn the module into boxed.", 'Stories') => 'yes' ),
			'dependency'    => array( 'element' => 'block_type', 'value' => array( '3', '4', '5', '6', '7', '9', '10', '14', '18', '22', '23', '25', '26', '27' ) )
		);

		$this->options[] = array(
			'type'          => 'checkbox',
			'param_name'    => 'boxed_shadow',
			'heading'       => esc_html__('Enable Shadow', 'Stories'),
			'description'   => esc_html__('Enable excerpt ellipsis', 'Stories'),
			'dependency'    => array('element' => "boxed", 'value' => 'yes')
		);

		$this->options[] = array(
			'type'          => 'slider',
			'param_name'    => 'excerpt_length',
			'heading'       => esc_html__('Excerpt Length', 'Stories'),
			'description'   => esc_html__('Set word length of excerpt on post block.', 'Stories'),
			'min'           => 0,
			'max'           => 200,
			'step'          => 1,
			'std'           => 20,
			'dependency'    => array( 'element' => 'block_type', 'value' => array( '3', '4', '5', '6', '7', '10', '12', '23', '25', '26', '27', '32', '33', '35', '36' ) )
		);

		$this->options[] = array(
			'type'          => 'textfield',
			'param_name'    => 'excerpt_ellipsis',
			'heading'       => esc_html__('Excerpt Ellipsis', 'Stories'),
			'description'   => esc_html__('Define excerpt ellipsis', 'Stories'),
			'std'           => '...',
			'dependency'    => array( 'element' => 'block_type', 'value' => array( '3', '4', '5', '6', '7', '10', '12', '23', '25', '26', '27', '32', '33', '35', '36' ) )
		);

		$this->options[] = array(
			'type'          => 'dropdown',
			'param_name'    => 'date_format',
			'heading'       => esc_html__('Content Date Format', 'Stories'),
			'description'   => esc_html__('Choose which date format you want to use.', 'Stories'),
			'std'           => 'default',
			'value'         => array(
				esc_html__('Relative Date/Time Format (ago)', 'Stories') => 'ago',
				esc_html__('WordPress Default Format', 'Stories')        => 'default',
				esc_html__('Custom Format', 'Stories')                   => 'custom',
			),
			'dependency'    => array( 'element' => 'block_type', 'value' => array( '3', '4', '5', '6', '7', '10', '12', '23', '25', '26', '27', '32', '33', '35', '36' ) )
		);

		$this->options[] = array(
			'type'          => 'textfield',
			'param_name'    => 'date_format_custom',
			'heading'       => esc_html__('Custom Date Format', 'Stories'),
			'description'   => wp_kses(sprintf(__('Please write custom date format for your module, for more detail about how to write date format, you can refer to this <a href="%s" target="_blank">link</a>.', 'Stories'), 'https://codex.wordpress.org/Formatting_Date_and_Time'), wp_kses_allowed_html()),
			'std'           => 'Y/m/d',
			'dependency'    => array('element' => 'date_format', 'value' => array('custom')),
		);

		$this->options[] = array(
			'type'          => 'checkbox',
			'param_name'    => 'first_page',
			'heading'       => esc_html__('Only First Page', 'Stories'),
			'description'   => esc_html__('Enable this option if you want to show this block only on the first page.', 'Stories'),
			'std'           => false
		);
	}

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'title_typography',
				'label'       => __( 'Title Typography', 'Stories' ),
				'description' => __( 'Set typography for post title', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_title > a',
			]
		);

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'meta_typography',
				'label'       => __( 'Meta Typography', 'Stories' ),
				'description' => __( 'Set typography for post meta', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_meta, {{WRAPPER}} .jeg_post_meta .fa, {{WRAPPER}}.jeg_postblock .jeg_subcat_list > li > a:hover, {{WRAPPER}} .jeg_pl_md_card .jeg_post_category a, {{WRAPPER}}.jeg_postblock .jeg_subcat_list > li > a.current, {{WRAPPER}} .jeg_pl_md_5 .jeg_post_meta, {{WRAPPER}} .jeg_pl_md_5 .jeg_post_meta .fa, {{WRAPPER}} .jeg_post_category a',
			]
		);

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'content_typography',
				'label'       => __( 'Post Content Typography', 'Stories' ),
				'description' => __( 'Set typography for post content', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_excerpt, {{WRAPPER}} .jeg_readmore',
			]
		);
	}
}
