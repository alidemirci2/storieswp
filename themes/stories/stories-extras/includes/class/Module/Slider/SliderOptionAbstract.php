<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Slider;

use EPIC\Module\ModuleOptionAbstract;

abstract Class SliderOptionAbstract extends ModuleOptionAbstract
{
    protected $default_number = 4;

    public function compatible_column()
    {
        return array( 8, 12 );
    }

	public function get_category()
	{
		return esc_html__('EPIC - Slider', 'Stories');
	}

	public function show_color_scheme()
    {
        return false;
    }

    public function set_options()
    {
        $this->set_slider_option();
        $this->set_content_filter_option($this->default_number);
        $this->set_style_option();
    }

    public function set_slider_option()
    {
        $this->options[] = array(
            'type'          => 'checkbox',
            'param_name'    => 'enable_autoplay',
            'heading'       => esc_html__('Enable Autoplay', 'Stories'),
            'description'   => esc_html__('Check this option to enable auto play.', 'Stories'),
        );
        $this->options[] = array(
            'type'          => 'slider',
            'param_name'    => 'autoplay_delay',
            'heading'       => esc_html__('Autoplay Delay', 'Stories'),
            'description'   => esc_html__('Set your autoplay delay (in millisecond).', 'Stories'),
            'min'           => 1000,
            'max'           => 10000,
            'step'          => 500,
            'std'           => 3000,
            'dependency'    => array('element' => 'enable_autoplay', 'value' => 'true')
        );
        $this->options[] = array(
            'type'          => 'dropdown',
            'param_name'    => 'date_format',
            'heading'       => esc_html__('Choose Date Format', 'Stories'),
            'description'   => esc_html__('Choose which date format you want to use.', 'Stories'),
            'std'           => 'default',
            'value'         => array(
                esc_html__('Relative Date/Time Format (ago)', 'Stories')  => 'ago',
                esc_html__('WordPress Default Format', 'Stories')         => 'default',
                esc_html__('Custom Format', 'Stories')                    => 'custom',
            )
        );
        $this->options[] = array(
            'type'          => 'textfield',
            'param_name'    => 'date_format_custom',
            'heading'       => esc_html__('Custom Date Format', 'Stories'),
            'description'   => wp_kses(sprintf(__('Please write custom date format for your module, for more detail about how to write date format, you can refer to this <a href="%s" target="_blank">link</a>.', 'Stories'), 'https://codex.wordpress.org/Formatting_Date_and_Time'), wp_kses_allowed_html()),
            'std'           => 'Y/m/d',
            'dependency'    => array('element' => 'date_format', 'value' => array('custom'))
        );
    }

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'title_typography',
				'label'       => __( 'Title Typography', 'Stories' ),
				'description' => __( 'Set typography for post title', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_title > a',
			]
		);

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'meta_typography',
				'label'       => __( 'Meta Typography', 'Stories' ),
				'description' => __( 'Set typography for post meta', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_meta, {{WRAPPER}} .jeg_post_meta .fa, {{WRAPPER}}.jeg_postblock .jeg_subcat_list > li > a:hover, {{WRAPPER}} .jeg_pl_md_card .jeg_post_category a, {{WRAPPER}}.jeg_postblock .jeg_subcat_list > li > a.current, {{WRAPPER}} .jeg_pl_md_5 .jeg_post_meta, {{WRAPPER}} .jeg_pl_md_5 .jeg_post_meta .fa, {{WRAPPER}} .jeg_post_category a',
			]
		);
	}
}
