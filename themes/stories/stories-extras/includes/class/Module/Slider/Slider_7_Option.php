<?php
/**
 * @author : Jegtheme
 */
namespace EPIC\Module\Slider;

Class Slider_7_Option extends SliderOptionAbstract
{
    protected $default_number = 5;
    protected $design_option = true;

    public function get_module_name()
    {
        return esc_html__('EPIC - Slider 7', 'Stories');
    }

    public function set_style_option()
    {
	    $this->options[] = array(
		    'type'          => 'dropdown',
		    'param_name'    => 'featured_position',
		    'heading'       => esc_html__('Featured Image Position', 'Stories'),
		    'description'   => esc_html__('Choose position for post featured image.', 'Stories'),
		    'std'           => 'left',
		    'group'         => esc_html__('Design', 'Stories'),
		    'value'         => array(
			    esc_html__('Left', 'Stories')  => 'left',
			    esc_html__('Right', 'Stories') => 'right',
		    )
	    );

    	parent::set_style_option();
    }

	public function set_typography_option( $instance ) {

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'title_typography',
				'label'       => __( 'Title Typography', 'Stories' ),
				'description' => __( 'Set typography for post title', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_title > a',
			]
		);

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'meta_typography',
				'label'       => __( 'Meta Typography', 'Stories' ),
				'description' => __( 'Set typography for post meta', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_meta, {{WRAPPER}} .jeg_post_meta .fa, {{WRAPPER}}.jeg_postblock .jeg_subcat_list > li > a:hover, {{WRAPPER}} .jeg_pl_md_card .jeg_post_category a, {{WRAPPER}}.jeg_postblock .jeg_subcat_list > li > a.current, {{WRAPPER}} .jeg_pl_md_5 .jeg_post_meta, {{WRAPPER}} .jeg_pl_md_5 .jeg_post_meta .fa, {{WRAPPER}} .jeg_post_category a',
			]
		);

		$instance->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'        => 'content_typography',
				'label'       => __( 'Post Content Typography', 'Stories' ),
				'description' => __( 'Set typography for post content', 'Stories' ),
				'selector'    => '{{WRAPPER}} .jeg_post_excerpt, {{WRAPPER}} .jeg_readmore',
			]
		);
	}
}
