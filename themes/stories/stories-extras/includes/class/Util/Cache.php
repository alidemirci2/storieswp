<?php
/**
 * @author : Jegtheme
 */

namespace EPIC\Util;

Class Cache {

	/**
	 * Handle cache for term
	 *
	 * @param $terms
	 */
	public static function cache_term( $terms ) {
		foreach ( $terms as $term ) {
			wp_cache_add( $term->term_id, $term, 'terms' );
		}
	}

	/**
	 * Get all users
	 *
	 * @return array|bool|mixed
	 */
	public static function get_users() {
		if ( ! $users = wp_cache_get( 'users', 'Stories' ) ) {
			$users = get_users();
			wp_cache_set( 'users', $users, 'Stories' );
		}

		return $users;
	}

	/**
	 * Get the total of users
	 *
	 * @return array|bool|mixed
	 */
	public static function get_count_users() {
		if ( ! $count = wp_cache_get( 'count_users', 'Stories' ) ) {
			$count = count_users();
			wp_cache_set( 'count_users', $count, 'Stories' );
		}

		return $count;
	}

	/**
	 * Get all categories
	 *
	 * @return array|bool|mixed
	 */
	public static function get_categories() {
		if ( ! $categories = wp_cache_get( 'categories', 'Stories' ) ) {
			$categories = get_categories( array( 'hide_empty' => 0 ) );
			wp_cache_set( 'categories', $categories, 'Stories' );
			self::cache_term( $categories );
		}

		return $categories;
	}

	/**
	 * Get the total of categories
	 *
	 * @return array|bool|int|mixed|\WP_Error
	 */
	public static function get_categories_count() {
		if ( ! $count = wp_cache_get( 'categories_count', 'Stories' ) ) {
			$count = wp_count_terms( 'category' );
			wp_cache_set( 'categories_count', $count, 'Stories' );
		}

		return $count;
	}

	/**
	 * Get all tags
	 *
	 * @return array|bool|mixed
	 */
	public static function get_tags() {
		if ( ! $tags = wp_cache_get( 'tags', 'Stories' ) ) {
			$tags = get_tags( array( 'hide_empty' => 0 ) );
			wp_cache_set( 'tags', $tags, 'Stories' );
			self::cache_term( $tags );
		}

		return $tags;
	}

	/**
	 * Get the total of tags
	 *
	 * @return array|bool|int|mixed|\WP_Error
	 */
	public static function get_tags_count() {
		if ( ! $count = wp_cache_get( 'tags_count', 'Stories' ) ) {
			$count = wp_count_terms( 'post_tag' );
			wp_cache_set( 'tags_count', $count, 'Stories' );
		}

		return $count;
	}

	/**
	 * Get all menus
	 *
	 * @return array|bool|mixed
	 */
	public static function get_menu() {
		if ( ! $menu = wp_cache_get( 'menu', 'Stories' ) ) {
			$menu = wp_get_nav_menus();
			wp_cache_set( 'menu', $menu, 'Stories' );
		}

		return $menu;
	}

	/**
	 * Get all post types
	 *
	 * @return array|bool|mixed
	 */
	public static function get_post_type() {
		if ( ! $post_type = wp_cache_get( 'post_type', 'Stories' ) ) {
			$post_type = get_post_types( array(
				'public'  => true,
				'show_ui' => true
			) );
			wp_cache_set( 'post_type', $post_type, 'Stories' );
		}

		return $post_type;
	}

	/**
	 * Get all excluded post types
	 *
	 * @return array|bool|mixed
	 */
	public static function get_exclude_post_type() {
		if ( ! $post_type = wp_cache_get( 'exclude_post_type', 'Stories' ) ) {
			$post_types = self::get_post_type();
			$result     = array();

			$exclude_post_type = array(
				'attachment',
				'custom-post-template',
				'archive-template',
				'elementor_library'
			);

			foreach ( $post_types as $type ) {
				if ( ! in_array( $type, $exclude_post_type ) ) {
					$result[ $type ] = get_post_type_object( $type )->label;
				}
			}

			$post_type = $result;

			wp_cache_set( 'exclude_post_type', $post_type, 'Stories' );
		}

		return $post_type;
	}

	/**
	 * Get all enabled custom taxonomies
	 *
	 * @return array|bool|mixed
	 */
	public static function get_enable_custom_taxonomies() {
		if ( ! $result = wp_cache_get( 'enable_custom_taxonomies', 'Stories' ) ) {
			$result     = array();
			$post_types = epic_get_enable_post_type();

			unset( $post_types['page'] );

			if ( ! empty( $post_types ) ) {

				foreach ( $post_types as $post_type => $label ) {

					$taxonomies = get_object_taxonomies( $post_type );

					if ( ! empty( $taxonomies ) && is_array( $taxonomies ) ) {

						foreach ( $taxonomies as $taxonomy ) {

							$taxonomy_data = get_taxonomy( $taxonomy );

							if ( $taxonomy_data->show_in_menu ) {
								$result[ $taxonomy ] = array(
									'name' => $taxonomy_data->labels->name,
									'post_types' => $taxonomy_data->object_type
								);
							}
						}
					}
				}
			}

			unset( $result['category'] );
			unset( $result['post_tag'] );

			wp_cache_set( 'enable_custom_taxonomies', $result, 'Stories' );
		}

		return $result;
	}
}
