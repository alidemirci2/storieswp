<?php 


    // Home Page
    if ( is_home() || is_front_page() ){
        $page = 'index';
    }

    // Pages
    elseif( is_page() ){
        $page = 'page'; 
    }

    // Posts
    elseif ( is_single() ){
        $page = 'single';
    }

    // Categories
    elseif ( is_category() ){
        $page = 'category';
    }

    // Author
    elseif ( is_author() ){
        $page = 'author';
    }


    // Tag
    elseif ( is_tag() ){
        $page = 'tag';
    }

    // All Archives
    else{
        $page = 'archieve'; 
    }

    if( epic_get_option( $page . '_sidebar_option' ) ){
        $layout_style = epic_get_option( $page . "_layout" );
    }else{
        $layout_style = epic_get_option( "site_layout" );
    }

?>


<?php 
            $last = $first = $box_shadow = '';

            switch ($layout_style) {
                case 'right-sidebar':
                case '2-right-sidebar':
                    $last = "order-last";
                    break;
                case 'left-sidebar':
                case '2-left-sidebar':
                    $first = "order-first";
                    break;
                case 'both-sidebar':
                    $first = "order-first";
                    $last = "";
                    break;
            }
            
            

            if($layout_style == 'no-sidebar'){
                return;
            }
              
            $sidebar_shadow = epic_get_option( 'sidebar_shadow' );
            if($sidebar_shadow == 1 ){
                $box_shadow = "box-shadow";
            }

            $first_sidebar = epic_get_option( 'index_sidebar_first' );
            $second_sidebar = epic_get_option( 'index_sidebar_second' );

            switch ($layout_style) {
                case 'right-sidebar':
                case 'double-right-sidebar':
                    $last = "order-last";
                    break;
                case 'left-sidebar':
                case 'double-left-sidebar':
                    $first = "order-first";
                    break;
                case 'both-sidebar':
                    $first = "order-first";
                    $last = "";
                    break;
            }

            $sidebar_size = epic_get_option( "sidebar_size" );
            $sticky_sidebar = epic_get_option( "sticky_sidebar" );

            if($sidebar_size == 1 ){
                $column .= 'col-lg-4 big-sidebar ';
            }else{
                $column .= 'col-lg-3 small-sidebar ';
            }

            // burak Sticky sidebar entegre eder misin buraya. js dosyasi eklendi.
            if($sticky_sidebar == 1 ){
                $column .= 'sticky-sidebar ';
            }

            ?>

            <aside id="secondary" class="sidebar widget-area main-column <?php echo esc_attr($first) . ' ' . esc_attr($last) ; ?> sidebar-area <?php echo esc_attr($column); ?>">
                <div class="<?php echo esc_attr($box_shadow); ?>">
                    <?php dynamic_sidebar( $first_sidebar ) ?>
                </div>
            </aside>

            <?php  if($layout_style == '2-left-sidebar' || $layout_style == '2-right-sidebar' || $layout_style == 'both-sidebar' ){ ?>

                <?php if($layout_style == 'both-sidebar') { $first = ""; $last = "order-last"; } ?>

                <aside id="secondary-2" class="sidebar widget-area main-column <?php echo esc_attr($first) . ' ' . esc_attr($last) ; ?> sidebar-area <?php echo esc_attr($column); ?>">
                    <div class="<?php echo esc_attr($box_shadow); ?>">
                         <?php dynamic_sidebar( $second_sidebar ) ?>
                    </div>
                </aside>

            <?php } ?>

                