<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/docs/define-meta-boxes
 */

/********************* META BOX DEFINITIONS ***********************/



add_filter( 'rwmb_meta_boxes', 'stories_register_meta_boxes' );
function stories_register_meta_boxes( $meta_boxes ){

$prefix = 'stories_';

global $meta_boxes;


// improve: each single have a layout
global $stories_opt;
if(!empty($stories_opt['single-article-type'])){
	$article_layout_def = $stories_opt['single-article-type'];
}else{
	$article_layout_def = "";
}


$sidebars = $GLOBALS['wp_registered_sidebars'];
foreach ( $sidebars as $sidebar ){
   $sidebar_options[$sidebar['id']] = $sidebar['name'];
}

$meta_boxes = array();

/*-----------------------------------------------------------------------------------*/
/*  Sidebar For Post
/*-----------------------------------------------------------------------------------*/
$meta_boxes[] = array(
	'id' => 'stylemag-sidebar',
	'title' => esc_html__( 'Sidebar', 'stylemag-admin' ),
	'pages' => array( 'post' ),
	'context' => 'side',
	'priority' => 'high',
	'fields' => array(
		array(
			'name'     => __( 'Select Sidebar Position', 'stylemag-admin' ),
			'id'       => $prefix . 'sidebar-position',
			'type'     => 'select',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'right' => __('Right Sidebar','stylemag-admin'), 
				'left' =>  __('Left Sidebar','stylemag-admin'), 
				'none' =>  __('No Sidebar','stylemag-admin')
			),
			'multiple'    => false,
			'placeholder' => __( 'Select Sidebar Position', 'stylemag-admin' ),
		),
		array(
			'name'     => esc_html__( 'Select Sidebar(Default sidebar selected. You can change it from theme options.)', 'stylemag-admin' ),
			'id'       => $prefix . 'sidebar-list',
			'type'     => 'select',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => $sidebar_options,
			// Select multiple values, optional. Default is false.
			'multiple'    => false,
			'placeholder' => esc_html__( 'Select Sidebar', 'stylemag-admin' ),
		),
));

/*-----------------------------------------------------------------------------------*/
/*  Gallery Post Format
/*-----------------------------------------------------------------------------------*/
$meta_boxes[] = array(
	'id'		=> 'stylemag-blogmeta-gallery',
	'title'		=> esc_html__('Blog Post Image Slides','stylemag-admin'),
	'pages'		=> array( 'post', ),
	'context' => 'normal',
	'fields'	=> array(
		array(
			'name'	=> esc_html__('Portfolio Gallery','stylemag-admin'),
			'desc'	=> esc_html__('You can drag and drop photos for reorder.','stylemag-admin'),
			'id'	=> $prefix . 'post-gallery',
			'type'	=> 'image_advanced',
			'max_file_uploads' => 225,
		)
		
	)
);

/*-----------------------------------------------------------------------------------*/
/*  Audio Post Format
/*-----------------------------------------------------------------------------------*/
$meta_boxes[] = array(
	'id' => 'stylemag-blogmeta-audio',
	'title' => esc_html__('Audio Settings','stylemag-admin'),
	'pages' => array( 'post'),
	'context' => 'normal',
	'fields' => array(	
		array(
			'name'		=> esc_html__('Audio Code','stylemag-admin'),
			'id'		=> $prefix . 'post-audio',
			'desc'		=> esc_html__('Enter your Audio URL(Oembed) or Embed Code.','stylemag-admin'),
			'clone'		=> false,
			'type'		=> 'textarea',
			'std'		=> ''
		),
		
	)
);

/*-----------------------------------------------------------------------------------*/
/*  Video Post Format
/*-----------------------------------------------------------------------------------*/
$meta_boxes[] = array(
	'id'		=> 'stylemag-blogmeta-video',
	'title'		=> esc_html__('Blog Video Settings','stylemag-admin'),
	'pages'		=> array( 'post' ),
	'context' => 'normal',
	'fields'	=> array(
		array(
			'name'	=> esc_html__('Embed Code','stylemag-admin'),
			'id'	=> $prefix . 'post-video',
			'desc'	=> esc_html__('Insert paste embed code','stylemag-admin'),
			'type' 	=> 'textarea',
			'std' 	=> "",
			'cols' 	=> "38",
			'rows' 	=> "10"
		),
		array(
			'name'	=> esc_html__('Video Duration','stylemag-admin'),
			'id'	=> $prefix . 'video_duration',
			'clone'	=> false,
			'type'	=> 'text',
			'desc'	=> esc_html__('Example 3:25','stylemag-admin'),
			'std'	=> ''
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/*  Must Read Filter
/*-----------------------------------------------------------------------------------*/
$meta_boxes[] = array(
	'id' => 'must-read',
	'title' => __( 'Featured Post', 'stylemag-admin' ),
	'pages' => array('post'),
	'context' => 'side',
	'priority' => 'high',
	'fields' => array(
		array(
			'name'     => __( '<p>Add this post in Featured posts!</p>', 'stylemag-admin' ),
			'id'       => $prefix."must_read",
			'type'     => 'select',
			'options'  => array(
				'Yes' => __( 'Yes, Please!', 'stylemag-admin' ),
				'No' => __( 'No', 'stylemag-admin' ),
			),
			'multiple'    => false,
			'std'         => "No",
		),								
	),
);


/*-----------------------------------------------------------------------------------*/
/*  Article Summary
/*-----------------------------------------------------------------------------------*/
$meta_boxes[] = array(
	'id' => 'stylemag-post-settings',
	'title' => esc_html__('Article','stylemag-admin'),
	'pages' => array('post'),
	'context' => 'normal',
	'fields' => array(	
		array(
			'name'	=> esc_html__('Post Summary','stylemag-admin'),
			'id'	=> $prefix . 'post_summary',
			'desc'	=> esc_html__('If you want you can use this area for post summary.','stylemag-admin'),
			'clone'	=> false,
			'type'	=> 'textarea',
			'std'	=> ''
		),
		array(
			'name'     => __( 'Article Layout', 'stylemag-admin' ),
			'id'       => $prefix."article_layout",
			'type'     => 'select',
			'options'  => array(
				'' => __( 'Select Layout', 'stylemag-admin' ),
				'layout1' => __( 'Layout 1(Classic)', 'stylemag-admin' ),
				'layout2' => __( 'Layout 2(Full Width Featured Image)', 'stylemag-admin' ),
				'layout3' => __( 'Layout 3(Full Width Covered Featured Image)', 'stylemag-admin' ),
				'layout4' => __( 'Layout 4(Full Width Featured Image with Transparent Header)', 'stylemag-admin' ),
			),
			'multiple'    => false,
			'desc'	=> esc_html__('Default layout selected. You can change it from theme options.','stylemag-admin'),
			'std'         => $article_layout_def,
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/*  Review
/*-----------------------------------------------------------------------------------*/
$meta_boxes[] = array(
	'id' => 'stylemag-post-review',
	'title' => esc_html__('Review','stylemag-admin'),
	'pages' => array('post'),
	'context' => 'normal',
	'fields' => array(	
		array(
			'name'	=> esc_html__('Review Title','stylemag-admin'),
			'id'	=> $prefix . 'review_title',
			'clone'	=> false,
			'type'	=> 'text',
			'std'	=> ''
		),
		array(
			'name'	=> esc_html__('Review Criterias','stylemag-admin'),
			'id'	=> $prefix . 'review_criteria',
			'clone'	=> true,
			'multiple' => true,
			'placeholder' => array('key' => 'Criteria', 'value' => 'Score(Examples: 5, 7.2) Max: 10'),
			'type'	=> 'key_value',
			'std'	=> ''
		),
		array(
			'name'	=> esc_html__('Pros','stylemag-admin'),
			'id'	=> $prefix . 'review_pros',
			'clone'	=> true,
			'type'	=> 'text',
			'std'	=> ''
		),
		array(
			'name'	=> esc_html__('Cons','stylemag-admin'),
			'id'	=> $prefix . 'review_cons',
			'clone'	=> true,
			'type'	=> 'text',
			'std'	=> ''
		),
	)
);

	return $meta_boxes;
}