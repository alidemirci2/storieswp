<?php 
// Customizer Fonts 
  function theme_get_customizer_css() {
    ob_start(); 

    $logo_height = get_theme_mod('logo_height');

    if(! empty( $logo_height  )){
      ?>
      .logo-container img {
        width:auto;
        max-height: <?php echo esc_attr($logo_height); ?>px;
      }
      <?php 
    }

    $logo_font_size = get_theme_mod('site_title_font_size');
    $logo_color = get_header_textcolor();
  
  
    if(! empty( $logo_font_size  )){
    ?>
    .logo-text a{
      font-size: <?php echo esc_attr($logo_font_size); ?>px;
      letter-spacing:0.4px;
    }
    <?php
    }
  
    if(! empty( $logo_color  )){
    ?>
    .logo-text a,
    .site-description{
      color: #<?php echo esc_attr($logo_color); ?>;
    }
    <?php
    }



    

    $main_font = epic_get_option('stories_style_main_font');

    if ( ! empty( $main_font  ) ) {
      ?>
      body, .main_font {
        font-family: <?php echo esc_attr($main_font['font-family']); ?>;
      }
      <?php
    }

    $title_font = epic_get_option('stories_style_helper_title_font');
    if ( ! empty( $title_font  ) ) {
      ?>
      .title_font{
        font-family: <?php echo esc_attr($title_font); ?>;
      }
      <?php
    }

    $helper_title_font = epic_get_option('stories_style_helper_title_font');
    if ( ! empty( $helper_title_font  ) ) {
      ?> 
      .helper_title_font {
        font-family: <?php echo esc_attr($helper_title_font); ?>;
      }
      <?php
    }

    $main_color = epic_get_option('main_color');
    if ( ! empty( $main_color  ) ) {
      ?> 
      .main_color, .single .jeg_meta_category a {
        color: <?php echo esc_attr($main_color); ?>;
      }
      <?php
    }


    $site_gutter = epic_get_option('site_gutter');

  if ( isset($site_gutter) && $site_gutter ) {
    ?>
      .main-row.row{
        margin-left: -<?php echo esc_attr($site_gutter); ?>px;
        margin-right: -<?php echo esc_attr($site_gutter); ?>px;
      }
      <?php 
  }


    $css = ob_get_clean();
    return $css;
  }

