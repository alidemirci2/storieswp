<?php 
/**
 * Rating
 */
    function stories_get_review_score($post_id){
            
            $review_criteria = get_post_meta($post_id, 'stories_review_criteria', true);
            $total_review_count = 0;

            $number_type = "total-odd";
            if (count($review_criteria) % 2 == 0){
                $number_type = "total-even";
            }
            
            if($review_criteria){
            // improve shortcode'da hata veriyor.
            foreach ($review_criteria as $review_criteria_score){
                if(empty($review_criteria_score[1])){ $review_criteria_score[1] = 0; }
                $total_review_count = $total_review_count + $review_criteria_score[1];
            }
            
            $total_review_score = round(($total_review_count/count($review_criteria)),1);
            }
            return $total_review_score;
    }

    if ( ! function_exists( 'stories_review_render' ) ) {

    add_action( 'stories_review', 'stories_review_render', 10, 1 );
	function stories_review_render($post_id) {

        $review_title = get_post_meta($post_id, 'stories_review_title', true);
        if(!empty($review_title)){
            $review_criteria = get_post_meta($post_id, 'stories_review_criteria', true);
            $review_pros = get_post_meta($post_id, 'stories_review_pros', true);
            $review_cons = get_post_meta($post_id, 'stories_review_cons', true);
       
        ?>
        <div class="review-box title_font margint30">
            <div class="review-header">
                <div class="review-header-left"><h3><?php echo esc_attr($review_title); ?></h3></div>
                <div class="review-header-right">
                    <div class="review-total-score"><?php echo esc_attr(stories_get_review_score($post_id)); ?></div>
                    <div class="review-total-text"><?php esc_html_e('Result','Stories'); ?></div>
                </div>
            </div> 
            <div class="review-criteria-list <?php echo esc_attr($number_type); ?>">
            <?php
                foreach ($review_criteria as $review_criteria_score){
            ?>

                    <div class="criteria-area">
                        <div class="criteria-text"><?php echo esc_attr($review_criteria_score[0]); ?></div>
                        <div class="criteria-score"><?php if(!empty($review_criteria_score[1])){ echo esc_attr($review_criteria_score[1]); }else{ $review_criteria_score[1] = 0; } ?></div>
                        <div class="criteria-bar-background">
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <span class="bar-line"></span>
                            <?php for($i = 0; $i < $review_criteria_score[1]; $i++){ ?>
                                <span class="bar-line active"></span>
                            <?php }  ?>
                            <div class="criteria-bar-line" style="width: <?php echo esc_attr($review_criteria_score[1]*10); ?>%;"></div>
                        </div>
                    </div>

            <?php } ?>
            </div>
            <hr>
            <div class="pros-cons">
                <?php if($review_pros): ?>
                <div class="pros">
                    <h6><?php echo __('PROS','Stories'); ?></h6>
                    <ul>
                    <?php 
                        foreach ($review_pros as $pros){
                            echo "<li>".esc_attr($pros)."</li>";
                        }
                    ?>
                    </ul>
                </div>
                <?php endif; ?>

                <?php if($review_cons): ?>
                <div class="cons">
                    <h6><?php echo __('CONS','Stories'); ?></h6>
                    <ul>
                    <?php 
                        foreach ($review_cons as $cons){
                            echo "<li>".esc_attr($cons)."</li>";
                        }
                    ?>
                    </ul>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php 
        }
	}
}







                            
