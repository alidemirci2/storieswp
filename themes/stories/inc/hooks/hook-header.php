<?php


/**
 * Header Bottom
 */
function stories_header_bottom() {
	do_action( 'stories_header_bottom' );
}

/**
* Header Before
 */
function stories_header_before() {
	do_action( 'stories_header_before' );
}

/**
 * Header
 */
function stories_header() {
	do_action( 'stories_header' );
}

/**
 * Main Header Top
 */
function stories_main_header_top() {
	do_action( 'stories_main_header_top' );
}

/**
 * Main Header
 */
function stories_main_header() {
	do_action( 'stories_main_header' );
}

/**
 * Main Header Bottom
 */
function stories_main_header_bottom() {
	do_action( 'stories_main_header_bottom' );
}

/**
 * Header After
 */
function stories_header_after() {
	do_action( 'stories_header_after' );
}

/**
 * Content Before
 */
function stories_content_before() {
	do_action( 'stories_content_before' );
}

/**
 * Content after
 */
function stories_content_after() {
	do_action( 'stories_content_after' );
}


/**
 * Site Header Function
 */
if ( ! function_exists( 'stories_header_cont' ) ) {


	/**
	 * Header
	 */
	function stories_header_cont() {
		?>

		<header itemtype="https://schema.org/WPHeader" itemscope="itemscope" id="headerDefault" role="banner">
			<div class="container">
				<div class="row"> 
					<?php stories_main_header(); ?> 
					<?php stories_header_bottom(); ?>
				<!-- /.row --></div>
			<!-- /.container --></div>	
		</header>

		<?php
    
	}
}

add_action( 'stories_header', 'stories_header_cont' );





/**
 * Logo/Title Function
 */
if ( ! function_exists( 'stories_site_branding_cont' ) ) {

	function stories_site_branding_cont() {
		?> 
	
			<div class="header-main"> 
				<div class="site-branding d-flex justify-content-center col-12" itemscope="itemscope" itemtype="https://schema.org/Organization">
						<?php stories_site_mobile_menu() ?>
						<div class="logo-container">
							<?php //!WPORG customizer logo ve alternatifin yazımı
							if(has_custom_logo()){ //Customizer Logo Seçimi varsa
								print_r(get_custom_logo());
							} else {
							?>
								<div class="logo-text">
									<a href="<?php 	echo esc_url(home_url('/')); ?>"><?php echo esc_attr(get_bloginfo('name')); ?></a>
								</div>
							<?php } ?>
							
						<!-- /.logo-container --></div>
				<!-- /site-branding --></div>
				
				<?php stories_site_search_icon(); ?>	
			<!-- /.headerMain --></div>	 	
		<?php
	}
}

add_action( 'stories_main_header', 'stories_site_branding_cont', 1 );


/**
 * Search Icon
 */
if ( ! function_exists( 'stories_site_search_icon' ) ) {

	function stories_site_search_icon() {
		?>
		<div class="search-box default">
			<div class="search-icon">
				<div class="search-button"> 
					<i class="icon icon icon-search-default"></i>
				<!-- /.search-button --></div>	
			<!-- /.search-icon --></div>
			<div class="search-overlay 	<?php if ( is_user_logged_in() ) { echo esc_attr( 'admin-bar-spacer', 'Stories' ); } ?>">
				<div class="search-container">
					<div class="custom-search-form"> 
						<span class="btn-search-close" aria-label="<?php echo esc_attr(__('Close search form', 'Stories')); ?>">  
							<span class="first-line"></span>
							<span class="second-line"></span> 
						</span>
						<span class="search-input-icon">
							<i class="icon icon icon-search-default"></i>
						</span>
						<?php echo stories_custom_search_form($addSubmit = 0); ?>
					<!-- /.custom-search-form --></div>
					 
					<div class="search-related">
						<div class="search-suggestion">
						<?php 
							// improve: show popular searches
							if (function_exists('sm_list_popular_searches')) {
								sm_list_popular_searches('<h6 class="title">'.esc_attr( __( 'Suggested Search', 'Stories' )).'</h6>');
							} else { 
								$tags = wp_tag_cloud( 'smallest=36&largest=36&unit=px&number=12&orderby=count&format=array' ); 
							 	if ($tags) {
							?>
								<h6><?php esc_html_e( 'Popular Tags', 'Stories' ); ?></h6>
								<?php echo "ddd";  wp_tag_cloud( 'smallest=36&largest=36&unit=px&number=12&orderby=count' ); ?> 
								<?php 
								}
							} 
							?> 
						</div>
					</div>
				</div>	
			<!-- /.search-overlay --></div>
		<!-- /.search-box --></div>
		<?php
	}
} 



/**
 * Search Icon
 */
if ( ! function_exists( 'stories_site_mobile_menu' ) ) {

	function stories_site_mobile_menu() {
		?>
		<div class="toggle-menu default"><span class="first-line"></span><span class="second-line"></span><span class="third-line"></span><div class="clearfix"></div></div> 
		<?php 
			if ( is_user_logged_in() ) {
				echo '
					<style>
						.overlay-menu {
							padding-top:32px;
						}
						header.sticky {
							top:32px !important;
						}
					</style>
				';
			}
		?>
		<div class="overlay-menu"> 
			<div class="row">
				<div class="col-lg-7 col-md-8 col-sm-8 content">
					<div class="site-branding"> 
						<div class="close-overlay-menu">
							<span class="first-line"></span>
							<span class="second-line"></span>
						<!-- /.close-overlay-menu --></div>
						<?php //!WPORG customizer logo ve alternatifin yazımı
										if(has_custom_logo()){ //Customizer Logo Seçimi varsa
											print_r(get_custom_logo());
										} else {
											$url = home_url();
										?>
											<a href="<?php 	echo esc_url( $url ); ?>"><img src="<?php echo esc_url(get_template_directory_uri()).'/assets/img/logo.png'; ?>"  /></a>
										<?php } ?>
					<!-- /site-branding --></div>
					<nav itemtype="http://schema.org/SiteNavigationElement" itemscope="itemscope" role="navigation" aria-label="<?php esc_attr_e( 'Elementor Menu', 'Stories' ); ?>">
						<?php
							wp_nav_menu(
								apply_filters(
									'widget_nav_menu_args',
									$overlay_menu_args,
									$overlay_menu,
									$settings
								)
							);
						?>
					</nav> 
					<!-- improve Widget Yerlestir -->
					<div class="overlay-menu-widgets col-12">
						<div class="row">
							<div class="col-lg-4 col-sm-4 col-12 widget">
								<p class="title">Info</p>
								<ul>
									<li><a href="#">Image</a></li>
									<li><a href="#">The Latest Issue</a></li>
									<li><a href="#">Career</a></li>
								</ul>
							<!-- /.widget --></div>
							<div class="col-lg-4 col-sm-4 col-12 widget">
								<p class="title">Info</p>
								<ul>
									<li><a href="#">Image</a></li>
									<li><a href="#">The Latest Issue</a></li>
									<li><a href="#">Career</a></li>
								</ul>
							<!-- /.widget --></div>
						<!-- /.row --></div>
					<!-- /.overlay-menu-widgets --></div>
				</div>
				<!--  -->
				<!-- <div class="col-lg-5 col-md-4 col-sm-4 d-none d-sm-block image" style="background-image:url();">  -->
				<!-- </div> -->
			</div>				
		<!-- /.overlay-menu --></div>
		<?php
	}
} 



/**
 * Function to get Primary navigation menu
 */
if ( ! function_exists( 'stories_primary_navigation_markup' ) ) {

	function stories_primary_navigation_markup() {  
		$items_wrap  = '<nav itemtype="https://schema.org/SiteNavigationElement" itemscope="itemscope" id="site-navigation" class="ast-flex-grow-1 navigation-accessibility" role="navigation" aria-label="' . esc_attr( 'Site Navigation', 'Stories' ) . '">';
		$items_wrap .= '<div class="main-navigation">';
		$items_wrap .= '<ul id="%1$s" class="%2$s">%3$s</ul>';
		$items_wrap .= '</div>';
		$items_wrap .= '</nav>';

		// Primary Menu.
		$primary_menu_args = array(
			'theme_location'  => 'stories-menu-1',
			'menu_id'         => 'primary-menu',
			'menu_class'      => '',
			'container'       => 'div',
			'container_class' => 'main-header-bar-navigation',
			'items_wrap'      => $items_wrap,
		);

		if ( has_nav_menu( 'primary' ) ) {
			if ( function_exists('max_mega_menu_is_enabled') && max_mega_menu_is_enabled('primary') ) :
				wp_nav_menu( array( 'theme_location' => 'primary') );
			else:
			echo '<div class="primary-nav stories-sticky-header col-12"><div class="main-header-bar-navigation"> <nav itemtype="https://schema.org/SiteNavigationElement" itemscope="itemscope" id="site-navigation" class="ast-flex-grow-1 navigation-accessibility" role="navigation" aria-label="' . esc_attr( 'Site Navigation', 'Stories' ) . '"><div class="main-navigation">' ;
				wp_nav_menu( $primary_menu_args );				
			echo '</div></nav></div></div>';
			endif;
		}  
	 
	}
}

add_action( 'stories_main_header', 'stories_primary_navigation_markup', 2 );





/**
 * Replace header logo.
 */
if ( ! function_exists( 'stories_replace_header_logo' ) ) :

	/**
	 * Replace header logo.
	 *
	 * @param array  $image Size.
	 * @param int    $attachment_id Image id.
	 * @param sting  $size Size name.
	 * @param string $icon Icon.
	 *
	 * @return array Size of image
	 */
	function stories_replace_header_logo( $image, $attachment_id, $size, $icon ) {

		$custom_logo_id = get_theme_mod( 'custom_logo' );

		if ( ! is_customize_preview() && $custom_logo_id == $attachment_id && 'full' == $size ) {

			// improve
			$data = wp_get_attachment_image_src( $attachment_id, 'stories-logo-size' );

			if ( false != $data ) {
				$image = $data;
			}
		}

		return apply_filters( 'stories_replace_header_logo', $image );
	}

endif;



/* Adding Support for Header Footer Elementor Plugin */
function stories_header_footer_elementor_support() {
	add_theme_support( 'header-footer-elementor' );
}

add_action( 'after_setup_theme', 'stories_header_footer_elementor_support' );


// Header & Footer Builder Post Type Size
// Improve TR: Burda header işaretlenmiş post typeların sayısını alıp na göre headerı gizleyelim.
$args = array( 'post_type' => 'elementor-hf' );
$loop = new WP_Query( $args );
$hf_count = $loop->post_count;


// Removed Header if Header Footer Elementor Plugin Active 
if ( function_exists( 'hfe_render_header' ) && $hf_count > 0 ) {
	remove_action( 'stories_header', 'stories_header_cont' );
}


// Activate Header & Footer Builder
function stories_render_hfe_header() {
	
	if ( function_exists( 'hfe_render_header' ) ) {
        hfe_render_header();
	}

}

add_action( 'stories_header', 'stories_render_hfe_header' );

// Improve : Aktif heraderi custom post type da belli et.
// Adding Image Preview for Header & Footer Custom Post Type

add_filter('manage_posts_columns', 'add_img_column');
add_filter('manage_posts_custom_column', 'manage_img_column', 10, 2);

function add_img_column($columns) {
    $columns['img'] = __( 'Header Preview', 'Stories' );
    return $columns;
}




	// IMPROVE Max Width verelim.
	function manage_img_column($column_name, $post_id) {
		if( $column_name == 'img' && get_post_type( $post_id ) == 'elementor-hf'  ) {
			echo get_the_post_thumbnail($post_id, 'full');
		}
		return $column_name;
	}


	// Remove Shortcode Column
	function set_shortcode_columns( $columns ) {
		// WPORG unset kurallara uygun mu acaba?
		unset( $columns['shortcode'] );
		return $columns;
	}

	add_filter( 'manage_elementor-hf_posts_columns', 'set_shortcode_columns' );



?>