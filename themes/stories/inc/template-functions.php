<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package CoverNews
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */



if (!function_exists('covernews_excerpt_length')) :

    /**
     * Simple excerpt length.
     *
     * @since 1.0.0
     */

    function covernews_excerpt_length($length)
    {
        
        if ( is_admin() ) {
                return $length;
        }

        return 15;
    }

endif;
add_filter('excerpt_length', 'covernews_excerpt_length', 999);


/* Display Breadcrumbs */
if (!function_exists('covernews_excerpt_more')) :

    /**
     * Simple excerpt more.
     *
     * @since 1.0.0
     */
    function covernews_excerpt_more($more)
    {
        return '...';
    }

endif;

add_filter('excerpt_more', 'covernews_excerpt_more');



/* Display Pagination */
if (!function_exists('covernews_numeric_pagination')) :

    /**
     * Simple excerpt more.
     *
     * @since 1.0.0
     */
    function covernews_numeric_pagination()
    {
        the_posts_pagination(array(
            'mid_size' => 3,
            'prev_text' => __( 'Previous', 'Stories' ),
            'next_text' => __( 'Next', 'Stories' ),
        ));
    }

endif;