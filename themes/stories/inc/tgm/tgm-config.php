<?php
/**
 * Recommended plugins
 *
 * @package CoverNews
 */

if ( ! function_exists( 'covernews_recommended_plugins' ) ) :

    /**
     * Recommend plugins.
     *
     * @since 1.0.0
     */
    function covernews_recommended_plugins() {

        $plugins = array(
            array(
                'name'     => esc_html__( 'Elementor', 'Stories' ),
                'slug'     => 'elementor',
                'required' => false,
            ),
            array(
                'name'     => esc_html__( 'Header, Footer & Blocks for Elementor', 'Stories' ),
                'slug'     => 'header-footer-elementor',
                'required' => false,
            ),
            array(
                'name'     => esc_html__( 'Max Mega Menu', 'Stories' ),
                'slug'     => 'megamenu',
                'required' => false,
            ),
            array(
                'name'     => esc_html__( 'Metabox', 'Stories' ),
                'slug'     => 'meta-box',
                'required' => false,
            ),
            array(
                'name'     => esc_html__( 'One Click Demo Import', 'Stories' ),
                'slug'     => 'one-click-demo-import',
                'required' => false,
            ),
        );

        tgmpa( $plugins );

    }

endif;

add_action( 'tgmpa_register', 'covernews_recommended_plugins' );
