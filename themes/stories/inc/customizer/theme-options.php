<?php

/**
 * Option Panel
 *
 * @package CoverNews
 */

$default = covernews_get_default_theme_options();
/*theme option panel info*/
require get_template_directory().'/inc/customizer/frontpage-options.php';


// Add Theme Options Panel.
$wp_customize->add_panel('theme_option_panel',
	array(
		'title'      => esc_html__('Theme Options', 'Stories'),
		'priority'   => 200,
		'capability' => 'edit_theme_options',
	)
);



/**
 * Header section
 *
 * @package CoverNews
 */

// Frontpage Section.
$wp_customize->add_section('header_options_settings',
	array(
		'title'      => esc_html__('Header Options', 'Stories'),
		'priority'   => 49,
		'capability' => 'edit_theme_options',
		'panel'      => 'theme_option_panel',
	)
);

// Setting - show_site_title_section.
$wp_customize->add_setting('show_date_section',
    array(
        'default'           => $default['show_date_section'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_checkbox',
    )
);
$wp_customize->add_control('show_date_section',
    array(
        'label'    => esc_html__('Show date on top header', 'Stories'),
        'section'  => 'header_options_settings',
        'type'     => 'checkbox',
        'priority' => 10,
        //'active_callback' => 'covernews_top_header_status'
    )
);



// Setting - show_site_title_section.
$wp_customize->add_setting('show_social_menu_section',
    array(
        'default'           => $default['show_social_menu_section'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_checkbox',
    )
);

$wp_customize->add_control('show_social_menu_section',
    array(
        'label'    => esc_html__('Show social menu on top header', 'Stories'),
        'section'  => 'header_options_settings',
        'type'     => 'checkbox',
        'priority' => 11,
        //'active_callback' => 'covernews_top_header_status'
    )
);



// Setting - sticky_header_option.
$wp_customize->add_setting('disable_sticky_header_option',
    array(
        'default'           => $default['disable_sticky_header_option'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_checkbox',
    )
);
$wp_customize->add_control('disable_sticky_header_option',
    array(
        'label'    => esc_html__('Disable Sticky Header', 'Stories'),
        'section'  => 'header_options_settings',
        'type'     => 'checkbox',
        'priority' => 11,
        //'active_callback' => 'storecommerce_header_layout_status'
    )
);

// Breadcrumb Section.
$wp_customize->add_section('site_breadcrumb_settings',
    array(
        'title'      => esc_html__('Breadcrumb Options', 'Stories'),
        'priority'   => 49,
        'capability' => 'edit_theme_options',
        'panel'      => 'theme_option_panel',
    )
);

// Setting - breadcrumb.
$wp_customize->add_setting('enable_breadcrumb',
    array(
        'default'           => $default['enable_breadcrumb'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_checkbox',
    )
);

$wp_customize->add_control('enable_breadcrumb',
    array(
        'label'    => esc_html__('Show breadcrumbs', 'Stories'),
        'section'  => 'site_breadcrumb_settings',
        'type'     => 'checkbox',
        'priority' => 10,
    )
);



/**
 * Layout options section
 *
 * @package CoverNews
 */

// Layout Section.
$wp_customize->add_section('site_layout_settings',
    array(
        'title'      => esc_html__('Global Settings', 'Stories'),
        'priority'   => 9,
        'capability' => 'edit_theme_options',
        'panel'      => 'theme_option_panel',
    )
);

// Setting - global content alignment of news.
$wp_customize->add_setting('global_content_alignment',
    array(
        'default'           => $default['global_content_alignment'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_select',
    )
);

$wp_customize->add_control( 'global_content_alignment',
    array(
        'label'       => esc_html__('Global Content Alignment', 'Stories'),
        'description' => esc_html__('Select global content alignment', 'Stories'),
        'section'     => 'site_layout_settings',
        'type'        => 'select',
        'choices'               => array(
            'align-content-left' => esc_html__( 'Content - Primary sidebar', 'Stories' ),
            'align-content-right' => esc_html__( 'Primary sidebar - Content', 'Stories' ),
            'full-width-content' => esc_html__( 'Full width content', 'Stories' )
        ),
        'priority'    => 130,
    ));

// Setting - global content alignment of news.
$wp_customize->add_setting('global_post_date_author_setting',
    array(
        'default'           => $default['global_post_date_author_setting'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_select',
    )
);

$wp_customize->add_control( 'global_post_date_author_setting',
    array(
        'label'       => esc_html__('Global Date and Author Display', 'Stories'),
        'description' => esc_html__('Select date and author display settings below post title', 'Stories'),
        'section'     => 'site_layout_settings',
        'type'        => 'select',
        'choices'               => array(
            'show-date-author' => esc_html__( 'Show Date and Author', 'Stories' ),
            'show-date-only' => esc_html__( 'Show Date Only', 'Stories' ),
            'show-author-only' => esc_html__( 'Show Author Only', 'Stories' ),
            'hide-date-author' => esc_html__( 'Hide All', 'Stories' ),
        ),
        'priority'    => 130,
    ));

// Setting - global content alignment of news.
$wp_customize->add_setting('global_widget_excerpt_setting',
    array(
        'default'           => $default['global_widget_excerpt_setting'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_select',
    )
);

$wp_customize->add_control( 'global_widget_excerpt_setting',
    array(
        'label'       => esc_html__('Global Widget Excerpt Mode', 'Stories'),
        'description' => esc_html__('Select Widget Excerpt display mode', 'Stories'),
        'section'     => 'site_layout_settings',
        'type'        => 'select',
        'choices'               => array(
            'trimmed-content' => esc_html__( 'Trimmed Content', 'Stories' ),
            'default-excerpt' => esc_html__( 'Default Excerpt', 'Stories' ),

        ),
        'priority'    => 130,
    ));


// Setting - global content alignment of news.
$wp_customize->add_setting('global_date_display_setting',
    array(
        'default'           => $default['global_date_display_setting'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_select',
    )
);

$wp_customize->add_control( 'global_date_display_setting',
    array(
        'label'       => esc_html__('Global Date Display Format', 'Stories'),
        'description' => esc_html__('Select date display display format', 'Stories'),
        'section'     => 'site_layout_settings',
        'type'        => 'select',
        'choices'               => array(
            'theme-date' => esc_html__( 'Date Format by Theme', 'Stories' ),
            'default-date' => esc_html__( 'WordPress Default Date Format', 'Stories' ),

        ),
        'priority'    => 130,
    ));

/**
 * Archive options section
 *
 * @package CoverNews
 */

// Archive Section.
$wp_customize->add_section('site_archive_settings',
    array(
        'title'      => esc_html__('Archive Settings', 'Stories'),
        'priority'   => 50,
        'capability' => 'edit_theme_options',
        'panel'      => 'theme_option_panel',
    )
);

 //Setting - archive content view of news.
$wp_customize->add_setting('archive_layout',
    array(
        'default'           => $default['archive_layout'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_select',
    )
);

$wp_customize->add_control( 'archive_layout',
    array(
        'label'       => esc_html__('Archive layout', 'Stories'),
        'description' => esc_html__('Select layout for archive', 'Stories'),
        'section'     => 'site_archive_settings',
        'type'        => 'select',
        'choices'               => array(
            'archive-layout-full' => esc_html__( 'Full', 'Stories' ),
            'archive-layout-grid' => esc_html__( 'Grid', 'Stories' ),
        ),
        'priority'    => 130,
    ));

// Setting - archive content view of news.
$wp_customize->add_setting('archive_image_alignment',
    array(
        'default'           => $default['archive_image_alignment'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_select',
    )
);

$wp_customize->add_control( 'archive_image_alignment',
    array(
        'label'       => esc_html__('Image alignment', 'Stories'),
        'description' => esc_html__('Select image alignment for archive', 'Stories'),
        'section'     => 'site_archive_settings',
        'type'        => 'select',
        'choices'               => array(
            'archive-image-left' => esc_html__( 'Left', 'Stories' ),
            'archive-image-right' => esc_html__( 'Right', 'Stories' )
        ),
        'priority'    => 130,
        'active_callback' => 'covernews_archive_image_status'
    ));

 //Setting - archive content view of news.
$wp_customize->add_setting('archive_content_view',
    array(
        'default'           => $default['archive_content_view'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_select',
    )
);

$wp_customize->add_control( 'archive_content_view',
    array(
        'label'       => esc_html__('Content view', 'Stories'),
        'description' => esc_html__('Select content view for archive', 'Stories'),
        'section'     => 'site_archive_settings',
        'type'        => 'select',
        'choices'               => array(
            'archive-content-excerpt' => esc_html__( 'Post excerpt', 'Stories' ),
            'archive-content-full' => esc_html__( 'Full Content', 'Stories' ),
            'archive-content-none' => esc_html__( 'None', 'Stories' ),

        ),
        'priority'    => 130,
    ));


//========== related posts  options ===============

// Single Section.
$wp_customize->add_section('site_single_related_posts_settings',
    array(
        'title'      => esc_html__('Related Posts', 'Stories'),
        'priority'   => 50,
        'capability' => 'edit_theme_options',
        'panel'      => 'theme_option_panel',
    )
);

// Setting - related posts.
$wp_customize->add_setting('single_show_related_posts',
    array(
        'default'           => $default['single_show_related_posts'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_checkbox',
    )
);

$wp_customize->add_control( 'single_show_related_posts',
    array(
        'label'    => __( 'Show on single posts', 'Stories' ),
        'section'  => 'site_single_related_posts_settings',
        'type'     => 'checkbox',
        'priority' => 100,
    )
);

// Setting - related posts.
$wp_customize->add_setting('single_related_posts_title',
    array(
        'default'           => $default['single_related_posts_title'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )
);

$wp_customize->add_control( 'single_related_posts_title',
    array(
        'label'    => __( 'Title', 'Stories' ),
        'section'  => 'site_single_related_posts_settings',
        'type'     => 'text',
        'priority' => 100,
        'active_callback' => 'covernews_related_posts_status'
    )
);




//========== footer latest blog carousel options ===============

// Footer Section.
$wp_customize->add_section('frontpage_latest_posts_settings',
    array(
        'title'      => esc_html__('Latest Posts Options', 'Stories'),
        'priority'   => 50,
        'capability' => 'edit_theme_options',
        'panel'      => 'theme_option_panel',
    )
);
// Setting - latest blog carousel.
$wp_customize->add_setting('frontpage_show_latest_posts',
    array(
        'default'           => $default['frontpage_show_latest_posts'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'covernews_sanitize_checkbox',
    )
);

$wp_customize->add_control( 'frontpage_show_latest_posts',
    array(
        'label'    => __( 'Show Latest Posts Section above Footer', 'Stories' ),
        'section'  => 'frontpage_latest_posts_settings',
        'type'     => 'checkbox',
        'priority' => 100,
    )
);


// Setting - featured_news_section_title.
$wp_customize->add_setting('frontpage_latest_posts_section_title',
    array(
        'default'           => $default['frontpage_latest_posts_section_title'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )
);
$wp_customize->add_control('frontpage_latest_posts_section_title',
    array(
        'label'    => esc_html__('Latest Posts Section Title', 'Stories'),
        'section'  => 'frontpage_latest_posts_settings',
        'type'     => 'text',
        'priority' => 100,
        'active_callback' => 'covernews_latest_news_section_status'

    )
);


//========== footer section options ===============
// Footer Section.
$wp_customize->add_section('site_footer_settings',
    array(
        'title'      => esc_html__('Footer', 'Stories'),
        'priority'   => 50,
        'capability' => 'edit_theme_options',
        'panel'      => 'theme_option_panel',
    )
);

// Setting - global content alignment of news.
$wp_customize->add_setting('footer_copyright_text',
    array(
        'default'           => $default['footer_copyright_text'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )
);

$wp_customize->add_control( 'footer_copyright_text',
    array(
        'label'    => __( 'Copyright Text', 'Stories' ),
        'section'  => 'site_footer_settings',
        'type'     => 'text',
        'priority' => 100,
    )
);

