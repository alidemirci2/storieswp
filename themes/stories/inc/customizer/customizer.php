<?php
/**
 * CoverNews Theme Customizer
 *
 * @package CoverNews
 */

if (!function_exists('covernews_get_option')):
/**
 * Get theme option.
 *
 * @since 1.0.0
 *
 * @param string $key Option key.
 * @return mixed Option value.
 */
function covernews_get_option($key) {

	if (empty($key)) {
		return;
	}

	$value = '';

	$default       = covernews_get_default_theme_options();
	$default_value = null;

	if (is_array($default) && isset($default[$key])) {
		$default_value = $default[$key];
	}

	if (null !== $default_value) {
		$value = get_theme_mod($key, $default_value);
	} else {
		$value = get_theme_mod($key);
	}

	return $value;
}
endif;

// Load customize default values.
require get_template_directory().'/inc/customizer/customizer-callback.php';

// Load customize default values.
require get_template_directory().'/inc/customizer/customizer-default.php';

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function covernews_customize_register($wp_customize) {

	// Load customize controls.
	require get_template_directory().'/inc/customizer/customizer-control.php';

	// Load customize sanitize.
	require get_template_directory().'/inc/customizer/customizer-sanitize.php';


    // Logo Height
    $wp_customize->add_setting('logo_height',
        array(
            'default'           => $default['logo_height'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );

    $wp_customize->add_control('logo_height',
        array(
            'label'    => esc_html__('Logo Height', 'Stories'),
            'section'  => 'title_tagline',
            'type'     => 'number',
            'priority' => 10,
            'description' => __( 'Enter Logo Height (px)', 'Stories' ),
        )
    );






      




    // Mobile Logo Checkbox
    $wp_customize->add_setting('use_mobile_logo',
        array(
            'default'           => $default['use_mobile_logo'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'covernews_sanitize_checkbox',
        )
    );

        $wp_customize->add_control('use_mobile_logo',
        array(
            'label'    => esc_html__('Use Mobile Logo', 'Stories'),
            'section'  => 'title_tagline',
            'type'     => 'checkbox',
            'priority' => 20,
        )
    );


    // Mobile Logo Upload
    $wp_customize->add_setting('mobile_logo',
        array(
            'default'           => $default['mobile_logo'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint',
        )
    );
    
    $wp_customize->add_control(
        new WP_Customize_Cropped_Image_Control($wp_customize, 'mobile_logo',
        array(
            'label'    => esc_html__('Mobile Logo', 'Stories'),
            'section'  => 'title_tagline',
            'priority' => 25,            
            'flex_width' => true,
            'flex_height' => true,
            'active_callback' => 'stories_show_mobile_logo'
        )
        )
    );


    
  





    // Dark Logo Checkbox
    $wp_customize->add_setting('use_dark_logo',
        array(
            'default'           => $default['use_dark_logo'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'covernews_sanitize_checkbox',
        )
    );

        $wp_customize->add_control('use_dark_logo',
        array(
            'label'    => esc_html__('Use Logo for Dark Background', 'Stories'),
            'section'  => 'title_tagline',
            'type'     => 'checkbox',
            'priority' => 30,
        )
    );


    // Dark Logo Upload
    $wp_customize->add_setting('dark_logo',
        array(
            'default'           => $default['dark_logo'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint',
        )
    );
    
    $wp_customize->add_control(
        new WP_Customize_Cropped_Image_Control($wp_customize, 'dark_logo',
        array(
            'label'    => esc_html__('Logo for Dark', 'Stories'),
            'section'  => 'title_tagline',
            'priority' => 30,            
            'flex_width' => true,
            'flex_height' => true,
            'active_callback' => 'stories_show_dark_logo'
        )
        )
    );


 







	$wp_customize->get_setting('blogname')->transport         = 'postMessage';
	$wp_customize->get_setting('blogname')->priority         = 40;
	$wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
	$wp_customize->get_setting('blogdescription')->priority  = 40;
	$wp_customize->get_setting('header_textcolor')->transport = 'postMessage';
	$wp_customize->get_setting('header_textcolor')->priority = 40;

	if (isset($wp_customize->selective_refresh)) {
		$wp_customize->selective_refresh->add_partial('blogname', array(
				'selector'        => '.site-title a',
				'render_callback' => 'covernews_customize_partial_blogname',
			));
		$wp_customize->selective_refresh->add_partial('blogdescription', array(
				'selector'        => '.site-description',
				'render_callback' => 'covernews_customize_partial_blogdescription',
			));
	}

    $default = covernews_get_default_theme_options();


    // Setting - secondary_font.
    $wp_customize->add_setting('site_title_font_size',
        array(
            'default'           => $default['site_title_font_size'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );

    $wp_customize->add_control('site_title_font_size',
        array(
            'label'    => esc_html__('Site Title Size', 'Stories'),
            'section'  => 'title_tagline',
            'type'     => 'number',
            'priority' => 45,
        )
    );

    // use get control
    $wp_customize->get_control( 'header_textcolor')->label = __( 'Site Title/Tagline Color', 'Stories' );
    $wp_customize->get_control( 'header_textcolor')->section = 'title_tagline';
    $wp_customize->get_control( 'header_textcolor')->priority = 50;
    $wp_customize->get_setting( 'header_textcolor' )->transport = 'refresh'; 

    // Setting - header overlay.
    $wp_customize->add_setting('disable_header_image_tint_overlay',
        array(
            'default'           => $default['disable_header_image_tint_overlay'],
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'covernews_sanitize_checkbox',
        )
    );

    $wp_customize->add_control('disable_header_image_tint_overlay',
        array(
            'label'    => esc_html__('Disable Image Tint/Overlay', 'Stories'),
            'section'  => 'header_image',
            'type'     => 'checkbox',
            'priority' => 55,
        )
    );


	/*theme option panel info*/
	require get_template_directory().'/inc/customizer/theme-options.php';

    // Register custom section types.
    $wp_customize->register_section_type( 'CoverNews_Customize_Section_Upsell' );

    // Register sections.
    $wp_customize->add_section(
        new CoverNews_Customize_Section_Upsell(
            $wp_customize,
            'theme_upsell',
            array(
                'title'    => esc_html__( 'CoverNews Pro', 'Stories' ),
                'pro_text' => esc_html__( 'Upgrade now', 'Stories' ),
                'pro_url'  => 'https://www.afthemes.com/products/covernews-pro/',
                'priority'  => 1,
            )
        )
    );



}
add_action('customize_register', 'covernews_customize_register');

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function covernews_customize_partial_blogname() {
	bloginfo('name');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function covernews_customize_partial_blogdescription() {
	bloginfo('description');
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function covernews_customize_preview_js() {
	wp_enqueue_script('covernews-customizer', esc_url(get_template_directory_uri()).'/js/customizer.js', array('customize-preview'), '20151215', true);
}
add_action('customize_preview_init', 'covernews_customize_preview_js');

function covernews_customizer_css() {
    wp_enqueue_script( 'covernews-customize-controls', esc_url(get_template_directory_uri()) . '/assets/customizer-admin.js', array( 'customize-controls' ) );

    wp_enqueue_style( 'covernews-customize-controls-style', esc_url(get_template_directory_uri()) . '/assets/customizer-admin.css' );
}
add_action( 'customize_controls_enqueue_scripts', 'covernews_customizer_css',0 );

