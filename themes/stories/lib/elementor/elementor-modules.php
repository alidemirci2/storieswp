<?php
/**
 * Author: ThemeIsle
 * Author URI: https://themeisle.com/
 */

//Improve TR: Bu plugini daha iyi temileyip temaya entegre edelim.


/**
 * Load gettext translate for our text domain.
 *
 * @since 1.0.0
 *
 * @return void
 */
function elementor_menus_load_plugin() {


	if( function_exists( 'elementor_load_plugin_textdomain' ) ) {

		require get_template_directory().'/lib/elementor/plugin.php';
	}
}
add_action( 'plugins_loaded', 'elementor_menus_load_plugin' );




function navmenu_navbar_menu_choices() {
	$menus = wp_get_nav_menus();
	$items = array();
	$i     = 0;
	foreach ( $menus as $menu ) {
		if ( $i == 0 ) {
			$default = $menu->slug;
			$i ++;
		}
		$items[ $menu->slug ] = $menu->name;
	}

	return $items;
}



/**
 * Add descriptions to menu items
 */
function navmenu_nav_description( $item_output, $item, $depth, $args ) {

	if ( $args->theme_location != 'nav_mega_menu' ) {
		return $item_output;
	}

	if ( $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;

}

add_filter( 'walker_nav_menu_start_el', 'navmenu_nav_description', 10, 4 );


add_filter( 'wp_nav_menu_objects', 'navmenu_thumb_filter_menu', 10, 2 );

function navmenu_thumb_filter_menu( $sorted_menu_objects, $args ) {

	// check for the right menu to filter
	if ( $args->theme_location != 'nav_mega_menu' ) {
		return $sorted_menu_objects;
	}
	// edit the menu objects
	foreach ( $sorted_menu_objects as $menu_object ) {
		// searching for menu items linking to posts or pages
		// can add as many post types to the array
		if ( in_array( $menu_object->object, array( 'post', 'page', 'product' ) ) ) {
			// set the title to the post_thumbnail if available
			// thumbnail size is the second parameter of get_the_post_thumbnail()
			$menu_object->description = has_post_thumbnail( $menu_object->object_id ) ? get_the_post_thumbnail( $menu_object->object_id, 'medium' ) : $menu_object->description;
		}
	}

	return $sorted_menu_objects;

} 
