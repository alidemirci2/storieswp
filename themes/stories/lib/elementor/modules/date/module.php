<?php
namespace ElementorMenus\Modules\Date;

use Elementor\Plugin;
use ElementorMenus\Module_Base;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

class Module extends Module_Base {

	public function __construct() {
		parent::__construct();

		// $this->add_actions();
	}

	public function get_name() {
		return 'elementor-date';
	}

	public function get_widgets() {
		return [
			'Elementor_Date',
		];
	}

}
