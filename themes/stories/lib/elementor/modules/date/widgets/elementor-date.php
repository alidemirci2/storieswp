<?php
namespace ElementorMenus\Modules\Date\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Elementor Navbar
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Elementor_Date extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-date';
	}

	public function get_title() {
		return __( 'Date', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-date';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() {
		// $menus = $this->get_menus();

		$this->start_controls_section(
			'section_date',
			[
				'label' => __( 'Date', 'Stories' ),
			]
		); 

		$this->add_control(
			'date_format',
			[				
				'name'     => 'date_format',
				'label' => __( 'Date Format', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => get_option('date_format'), 
				//'description' => __( 'Desc', 'Stories' ),
				//Improve WP date sayfasina link ver
				'selector' => '{{WRAPPER}} .elementor-date',
			]
		);

		

		$this->add_control(
			'date_text_color',
			[
				'label'     => __( 'Date Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#222222',
				'selectors' => [
					'{{WRAPPER}} .elementor-date ' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_typo',
			[
				'label' => __( 'Typography', 'Stories' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'search_typography',
				'label'    => __( 'Typography', 'Stories' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .elementor-date',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_date_style',
			[
				'label' => __( 'Style', 'Stories' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'        => __( 'Alignment', 'Stories' ),
				'type'         => Controls_Manager::CHOOSE,
				'options'      => [
					'left'   => [
						'title' => __( 'Left', 'Stories' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'Stories' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => __( 'Right', 'Stories' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'default'      => '',
			]
		);


		$this->end_controls_section();

	}

	protected function render() {

		$settings = $this->get_settings(); ?>
		
		<div class="elementor-date ">
			<?php echo get_the_date($settings["date_format"]); ?>
		</div>
		<?php
	}

	protected function _content_template() {}
}
