<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

return [
	'title'              => __( 'Social Media', 'Stories' ),
	'required'           => true,
	'default_activation' => true,
];
