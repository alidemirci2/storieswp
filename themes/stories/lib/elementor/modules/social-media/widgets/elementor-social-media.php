<?php
namespace ElementorMenus\Modules\SocialMedia\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Elementor Social Media
 *
 *
 * @since 1.0.0
 */
class Elementor_SocialMedia extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-social-media';
	}

	public function get_title() {
		return __( 'Social Media', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-share';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() { 
		$this->start_controls_section(
			'sm_content_section',
			[
				'label' => __( 'Social Media Icons', 'Stories' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'sm_icon_count',
			[
				'label' => __( 'Count Limit of Showing Icon', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'range' => [
						'min' => 1,
						'max' => 30,
				],
				'desktop_default' => [
					'size' => 3, 
				],
				'selectors' => [
					'{{WRAPPER}} .widget-image' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);


		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'sm_icon_title', [
				'label' => __( 'Title', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Title' , 'Stories' ),
				'label_block' => true,
			]
		);
		
		$repeater->add_control(
			'sm_icon_url',
			[
				'label' => __( 'Link', 'Stories' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://link', 'Stories' ),
				'show_external' => true,
				'default' => [
					'url' => '#',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		); 
		$repeater->add_control(
			'sm_icon',
			[
				'label' => __( 'Choose Icon', 'Stories' ),
				'type' => \Elementor\Controls_Manager::ICON,
				'include' => [ 
					'eicon-type-tool',
					'fa fa-fclickr',
					'fa fa-google-plus',
					'fa fa-instagram',
					'fa fa-linkedin',
					'fa fa-pinterest',
					'fa fa-reddit',
					'fa fa-twitch',
					'fa fa-twitter',
					'fa fa-vimeo',
					'fa fa-youtube',
				],
				'default' => 'fa fa-facebook',
			]
		);

		
		$this->start_controls_tabs( 'section_sm_icon_colors' );

		$this->start_controls_tab(
			'section_sm_icon_color_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'sm_icon_color',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#131313',
				'selectors' => [
					'{{WRAPPER}} .social-media .social-media-list li a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'section_sm_icon_color_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'sm_icon_color_hover',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .social-media .social-media-list li a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();

		$this->add_control(
			'sm_icon_menu_list',
			[
				'label' => __( 'Social Media Icons', 'Stories' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'sm_icon_title' => __( 'Facebook', 'Stories' ),
						'sm_icon' => __( 'fa fa-facebook', 'Stories' ),
					],
					[
						'sm_icon_title' => __( 'Twitter', 'Stories' ),
						'sm_icon' => __( 'fa fa-twitter', 'Stories' ),
					],
				],
				'title_field' => '{{{ sm_icon_title }}}',
			]
		);

		$this->end_controls_section();

	}

	protected function render() { 
		$settings = $this->get_settings();
		$totalCount = count($settings["sm_icon_menu_list"]);
		$countLimit =  $settings["sm_icon_count"]["size"];  
		?> 
		<div class="social-media">
			<ul class="social-media-list">
				<?php 
					$counter = 0; 
					foreach ( $settings["sm_icon_menu_list"] as $item) {   
						$counter++;
						$target = $item['sm_icon_url']['is_external'] ? ' target="_blank"' : '';
						$nofollow = $item['sm_icon_url']['nofollow'] ? ' rel="nofollow"' : '';
						if ($counter <= $countLimit) {
					?>   
						<li class="elementor-repeater-item-<?php echo esc_attr($item['_id']); ?> ">
							<a href="<?php echo esc_url($item["sm_icon_url"]["url"]); ?>" <?php echo esc_attr($target); ?> <?php echo esc_attr($nofollow); ?> title="<?php echo esc_attr($item["sm_icon_title"]); ?>">
								<i class="<?php echo esc_attr($item["sm_icon"]); ?>"></i>
							</a>
						</li>
					<?php
						}	
					}
					 if($totalCount > $countLimit)	{
					?>
					<li class="more"><button><i class="fas fa-ellipsis-h"></i></button>
					<ul class="social-media-list-more box-shadow">
						<?php 
							$counter =  0;
							foreach ( $settings["sm_icon_menu_list"] as $item) {  
								$counter++;
								$target = $item['sm_icon_url']['is_external'] ? ' target="_blank"' : '';
								$nofollow = $item['sm_icon_url']['nofollow'] ? ' rel="nofollow"' : '';
								if ($counter <=  $countLimit) { 
								}	
								else {
							?>
							<li class="elementor-repeater-item-<?php echo esc_attr($item['_id']); ?> ">
								<a href="<?php echo esc_url($item["sm_icon_url"]["url"]); ?>" <?php echo esc_attr($target); ?> <?php echo esc_attr($nofollow); ?> title="<?php echo esc_attr($item["sm_icon_title"]); ?>">
									<i class="<?php echo esc_attr($item["sm_icon"]); ?>"></i>
								</a>
								</li>
							<?php
								}
							} 	
							?>
					</ul>
				</li>
						</ul>	
			<?php } ?>	
			<div class="clearfix"></div>
		<!-- /.social-media --></div>
		<?php
	}

	protected function _content_template() {}
}
