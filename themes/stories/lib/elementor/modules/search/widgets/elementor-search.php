<?php
namespace ElementorMenus\Modules\Search\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Elementor Navbar
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Elementor_Search extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-search';
	}

	public function get_title() {
		return __( 'Search Box', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-search';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() {
		// $menus = $this->get_menus();
		
		// Search Icon
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Search Icon', 'Stories' ),
			]
		);

		$this->add_control(
			'search_icon',
			[
				'label' => __( 'Search Icon', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'solid',
				'options' => [
					'icon icon-search-default'  => __( 'Creative Icon', 'Stories' ),
					'icon icon-search-bold' => __( 'Bold Icon', 'Stories' ), 
					'icon icon-search-thin' => __( 'Thin Icon', 'Stories' ), 
					'icon icon-search-thin-creative' => __( 'Thin Creative Icon', 'Stories' ), 
				],
				'default'   => 'icon icon-search-default',
			]
		);


		$this->add_control(
			'search_icon_size',
			[
				'label' => __( 'Icon Size', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px', 'em', 'rem' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 200,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 24,
				],
				'selectors' => [
					'{{WRAPPER}} .search-button i' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);




		$this->start_controls_tabs( 'search_icon_color_tabs' );

		$this->add_control(
			'search_icon_color',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .search-button i' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 



		$this->end_controls_tabs();  // Search Icon Color Tabs ending




		$this->add_responsive_control(
			'align',
			[
				'label'        => __( 'Alignment', 'Stories' ),
				'type'         => Controls_Manager::CHOOSE,
				'options'      => [
					'flex-start'   => [
						'title' => __( 'Left', 'Stories' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'Stories' ),
						'icon'  => 'fa fa-align-center',
					],
					'flex-end'  => [
						'title' => __( 'Right', 'Stories' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'default'      => 'flex-end',
				'selectors' => [
					'{{WRAPPER}} .search-box' => 'display:flex; justify-content: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		//Search Text
		$this->start_controls_section(
			'section_search_text',
			[
				'label' => __( 'Search Text', 'Stories' ),
			]
		);

		$this->add_control(
			'search_text',
			[			 
				'label' =>  __( 'Search Text', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,  
			]
		); 
		$this->start_controls_tabs( 'search_text_color_tabs' );
		$this->start_controls_tab(
			'search_text_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'search_text_color',
			[
				'label'     => __( 'Search Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .search-button span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'search_text_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'search_text_color_hover',
			[
				'label'     => __( 'Search Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .search-button:hover span' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();  // Search Text Color Tabs ending



		$this->add_control(
			'search_order',
			[
				'label' => __( 'Order Elements', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'solid',
				'options' => [
					'2'  => __( 'Text First', 'Stories' ),
					'0' => __( 'Icon First', 'Stories' ), 
				],
				'default'   => '0',
				'selectors' => [
					'{{WRAPPER}} .search-button i' => 'order: {{VALUE}};', //Improve önyüzde görünüyor fakat editörde görünmüyor.
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'search_text_typo',
				'label'    => __( 'Typography', 'Stories' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .search-button span',
			]
		);

		$this->end_controls_section();


		// Search Suggestions
		$this->start_controls_section(
			'search_suggestions',
			[
				'label' => __( 'Search Suggestions', 'Stories' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		); 

			$this->add_control(
				'show_search_suggestion',
				[
					'label' => __( 'Suggestion Show', 'Stories' ),
					'type' => \Elementor\Controls_Manager::SWITCHER,
					'label_on' => __( 'Show', 'Stories' ),
					'label_off' => __( 'Hide', 'Stories' ),
					'return_value' => 'yes',
					'default' => 'yes',
				]
			);

			$this->add_control(
			'select_search_suggestion',
			[
				'label' => __( 'Border Style', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'latest',
				'options' => [
					'latest'  => __( 'Latest Tags', 'Stories' ),
					'popular' => __( 'Popular Searches', 'Stories' ), 
				],
				'condition' => [
					'show_search_suggestion' => 'yes',
				],
			]
		);

		$this->add_control(
				'important_note',
				[

					'type' => \Elementor\Controls_Manager::RAW_HTML,
					'raw' => __( 'Make sure You installed Search Meter Plugin.', 'plugin-name' ),
					'condition' => [
						'select_search_suggestion' => 'popular',
					],
				]
			);

		
		$this->end_controls_section(); 



	}

	protected function render() {

		$settings = $this->get_settings(); ?>
		
		<div class="search-box">
			<div class="search-icon">
				<div class="search-button">
					<i class="<?php echo esc_attr($settings["search_icon"]); ?>"></i> <span><?php echo esc_attr($settings["search_text"]); ?></span>
				<!-- /.search-button --></div>	
			<!-- /.search-icon --></div>  
			<div class="search-overlay	<?php if ( is_user_logged_in() ) { echo esc_attr( 'admin-bar-spacer', 'Stories' ); } ?>">
				<div class="search-container">
					<div class="custom-search-form">
						<span  class="btn-search-close" aria-label="<?php echo esc_attr(__('Close search form', 'Stories')); ?>">
							<span class="first-line"></span>
							<span class="second-line"></span>
						</span>
						<span class="search-input-icon">
							<i class="icon icon icon-search-default"></i>
						</span> 
						<?php echo stories_custom_search_form($addSubmit = 0); ?>
					<!-- /.custom-search-form --></div> 
					<?php if($settings['show_search_suggestion'] == "yes" ){ ?>
					<div class="search-related">
						<div class="search-suggestion">
						<?php 
							// improve: show popular searches
							if (function_exists('sm_list_popular_searches') && $settings['select_search_suggestion'] == "popular" ) {
								sm_list_popular_searches('<h6 class="title">'.esc_attr( __( 'Suggested Search', 'Stories' )).'</h6>');
							} else { 
								$tags = wp_tag_cloud( 'smallest=36&largest=36&unit=px&number=12&orderby=count&format=array' ); 
							 	if ($tags) {
							?>
								<h6><?php esc_html_e( 'Popular Tags', 'Stories' ); ?></h6>
								<?php wp_tag_cloud( 'smallest=36&largest=36&unit=px&number=12&orderby=count' ); ?> 
								<?php 
								}
							}  ?>
						</div>
					</div>
							<?php } ?>
				</div>	
			<!-- /.search-overlay --></div>
		<!-- /.search-box --></div>
		<?php
	}

	protected function _content_template() {}
}
