<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

return [
	'title'              => __( 'Branding & Logo', 'Stories' ),
	'required'           => true,
	'default_activation' => true,
];
