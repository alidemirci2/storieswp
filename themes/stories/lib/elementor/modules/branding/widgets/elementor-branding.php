<?php
namespace ElementorMenus\Modules\Branding\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Elementor Navbar
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Elementor_Branding extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-branding';
	}

	public function get_title() {
		return __( 'Branding & Logo', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-banner';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() {
		// $menus = $this->get_menus();

		$this->start_controls_section(
			'section_content',
			[ 
				'label' => __( 'Branding', 'Stories' ),
			]
		);

		$this->add_control(
			'el_site_branding',
			[
				'label'       => __( 'Branding Type', 'Stories' ),
				// improve: Logo nerde olcaksa desc'de onu goster
				'description' => __( 'We get Logo and Title from Appearance > Customize', 'Stories' ),
				'type'        => Controls_Manager::SELECT, 'options' => [
					'title' => __( 'Title', 'Stories' ),
					'logo'  => __( 'Logo', 'Stories' ),
					'logo_dark'  => __( 'Logo for Dark', 'Stories' ),
					'custom_logo'  => __( 'Custom Logo', 'Stories' ),
				],
				'default'     => 'title',
			]
		);
		
		$this->add_responsive_control(
			'align',
			[
				'label'        => __( 'Alignment', 'Stories' ),
				'type'         => Controls_Manager::CHOOSE,
				'options'      => [
					'left'   => [
						'title' => __( 'Left', 'Stories' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'Stories' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => __( 'Right', 'Stories' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'default'      => 'center',
			]
		);

	
		$this->end_controls_section();


		$this->start_controls_section(
			'custom_logo',
			[
				'label' => __( 'Custom Logo', 'Stories' ),
				'condition' => [
					'el_site_branding' => 'custom_logo',
				],
			]
		);

		$this->add_control(
			'custom_logo_upload',
			[
				'label'     => __( 'Choose Image', 'Stories' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'condition' => [
					'el_site_branding' => 'custom_logo',
				],
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

	$this->add_control(
			'logo_height',
			[
				'label' => __( 'Logo Height', 'Stories' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [
					'el_site_branding' => array('custom_logo'),
				],
				'selectors' => [
					'{{WRAPPER}} .custom-logo img' => 'width: auto; max-height: {{SIZE}}{{UNIT}};',
				],
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Brand', 'Stories' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'branding_title_color',
			[
				'label'     => __( 'Title Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'condition' => [
					'el_site_branding' => 'title',
				],
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .elementor-branding .logo-text a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'branding_title_hover',
			[
				'label'     => __( 'Hover', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'condition' => [
					'el_site_branding' => 'title',
				],
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-branding .logo-text a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_padding',
			[
				'label'      => __( 'Title Padding - Default 1em', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'condition'  => [
					'el_site_branding' => 'title',
				],
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .elementor-branding .logo-text a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'title_typography',
				'label'     => __( 'Typography', 'Stories' ),
				'condition' => [
					'el_site_branding' => 'title',
				],
				'scheme'    => Scheme_Typography::TYPOGRAPHY_1,
				'selector'  => '{{WRAPPER}} .elementor-branding .logo-text a',
			]
		);

		$this->add_control(
			'logo_padding',
			[
				'label'      => __( 'Title Padding - Default 1em', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'condition'  => [
					'el_site_branding' => 'logo',
				],
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .elementor-branding .custom-logo' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_desc_style',
			[
				'label'     => __( 'Description Options', 'Stories' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'el_site_branding' => 'title',
				],
			]
		);

		$this->add_control(
			'branding_description_color',
			[
				'label'     => __( 'Description Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'condition' => [
					'el_site_branding' => 'title',
				],
				'default'	=> '#888',
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-branding .site-description' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'desc_padding',
			[
				'label'      => __( 'Description Padding - Default 1em', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'condition'  => [
					'el_site_branding' => 'title',
				],
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .elementor-branding .site-description' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'desc_typography',
				'label'     => __( 'Typography', 'Stories' ),
				'condition' => [
					'el_site_branding' => 'title',
				],
				'scheme'    => Scheme_Typography::TYPOGRAPHY_1,
				'selector'  => '{{WRAPPER}} .elementor-branding .site-description',
			]
		);

		$this->end_controls_section();

	

	}

	protected function branding_output() {
		$settings = $this->get_settings();

		if ( $settings['el_site_branding'] == 'title' ) {
			$this->render_title();
		} elseif ( $settings['el_site_branding'] == 'logo' ) {
			$this->render_logo();
		} elseif ( $settings['el_site_branding'] == 'logo_dark' ) { ?>
			<div class="logo-container">
				<?php echo wp_get_attachment_image( get_theme_mod('dark_logo'), 'full' ); ?>
			</div>
		<?php 
		}elseif ($settings['el_site_branding'] == 'custom_logo') {
			// burak escaping yok burda. 
			?>
			<div class="custom-logo">
			<?php 	
				echo "<a href='". esc_url(home_url('/'))."'><img src=\"".esc_url($settings['custom_logo_upload']['url'])."\" alt=\"".esc_attr(get_bloginfo( 'name' ))."\" /></a>" ; 
 			?>
			</div>
		<?php 
		}
	} 
	protected function elementor_the_site_logo() {
		if ( function_exists( 'the_custom_logo' ) ) {
			?>
			<div class="logo-container">
				<?php the_custom_logo();  ?> 
			</div>
		<?php 
		}
	}
	protected function elementor_custom_elementor_logo() {
		 
	}

	protected function render_title() {
		?>
		<span class="logo-text">
		<?php
			$title = get_bloginfo( 'name' );
		?>
						
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( $title ); /* WPCS: xss ok. */ ?>" alt="<?php echo esc_attr( $title ); ?>">
				<?php bloginfo( 'name' ); ?>
			</a>		
		</span>
		<?php
			$description = get_bloginfo( 'description', 'display' );
		if ( $description || is_customize_preview() ) :
			?>
				<p class="site-description"><?php echo esc_html($description); /* WPCS: xss ok. */ ?></p>
			<?php
		endif;
	}

	protected function render_logo() {
		$this->elementor_the_site_logo();
	}

	protected function render() {

		$settings = $this->get_settings();
		?>
		
		<div id="elementor-branding" class="elementor-branding">
			<div class="header-title">
			<?php
				$this->branding_output();
			?>
			<?php ?>
						</div>
		</div>
		<?php
	}

	protected function _content_template() {}
}
