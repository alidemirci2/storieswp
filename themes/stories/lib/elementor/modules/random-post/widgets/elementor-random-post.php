<?php
namespace ElementorMenus\Modules\RandomPost\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Elementor Random Post
 *
 *
 * @since 1.0.0
 */
class Elementor_RandomPost extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-random-post';
	}

	public function get_title() {
		return __( 'Random Post', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-sync';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() { 
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Random Post Icon', 'Stories' ),
			]
		);

		$this->add_control(
			'random_post_icon',
			[

				'label' => __( 'Choose Icon', 'Stories' ),
				'type' => \Elementor\Controls_Manager::ICON,
				'include' => [
					'fa fa-random',
					'fa fa-facebook',
					'fa fa-flickr',
					'fa fa-google-plus',
					'fa fa-instagram',
					'fa fa-linkedin',
					'fa fa-pinterest',
					'fa fa-reddit',
					'fa fa-twitch',
					'fa fa-twitter',
					'fa fa-vimeo',
					'fa fa-youtube',
				],
				'default' => 'fa fa-random',
			]
		);

		$this->add_control(
			'random_post_icon_size',
			[
				'label' => __( 'Icon Size', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px', 'em', 'rem' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 200,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 16,
				],
				'selectors' => [
					'{{WRAPPER}} .search-button i' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		
		$this->start_controls_tabs( 'random_post_icon_color_tabs' );
		$this->start_controls_tab(
			'random_post_icon_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'random_post_icon_color',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .random-post a .random-post-icon svg g' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'random_post_icon_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'random_post_icon_color_hover',
			[
				'label'     => __( 'Icon Hover Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .random-post a:hover .random-post-icon svg g' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->end_controls_tabs();  
 
		$this->end_controls_section();
		$this->start_controls_section(
			'section_text',
			[
				'label' => __( 'Random Post Text', 'Stories' ),
			]
		);

		$this->add_control(
			'random_post_text',
			[			 
				'label' =>  __( 'Random Post Text', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_attr( __( 'Random Post', 'Stories' ) ),  
			]
		); 
		$this->start_controls_tabs( 'random_post_text_color_tabs' );
		$this->start_controls_tab(
			'random_post_text_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'random_post_text_color',
			[
				'label'     => __( 'Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .random-post a .random-post-text' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'random_post_text_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'random_post_text_color_hover',
			[
				'label'     => __( 'Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .random-post a:hover .random-post-text' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();  


		// Bence buna gerek yok, eger gerek oldugunu dusunuyorsan her iki versiyonda da aradaki boslugu muhafaza edelim.
		$this->add_control(
			'random_post_order',
			[
				'label' => __( 'Order Elements', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SELECT, 
				'options' => [
					'0' => __( 'Icon First', 'Stories' ), 
					'2'  => __( 'Text First', 'Stories' ),
				],
				'default'   => '0',
				'selectors' => [
					'{{WRAPPER}} .random-post a .random-post-icon' => 'order: {{VALUE}};',  
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'full_screen_search_typography',
				'label'    => __( 'Typography', 'Stories' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .elementor-search',
			]
		);

		$this->end_controls_section();


	}

	protected function render() { 
		$settings = $this->get_settings(); 
		?> 
		<div class="random-post">
		<?php // Home url icine slash koyulmasi gerekiyor. ?>
			<a href="<?php echo esc_url(home_url('/')); ?>/?random=1">
				<span class="random-post-icon">
					<?php get_template_part('/assets/svg/random.svg');  ?>
					<?php // Burda random iconu kullanilcak ama beceremedim. ?>
				</span> 
				<span class="random-post-text">
					<?php echo esc_attr($settings["random_post_text"]);  ?>
				</span> 
			</a>
			<div class="clearfix"></div>
		<!-- /.random-post --></div>
		<?php
	}

	protected function _content_template() {}
}
