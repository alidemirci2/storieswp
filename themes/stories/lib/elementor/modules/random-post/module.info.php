<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

return [
	'title'              => __( 'Random Post', 'Stories' ),
	'required'           => true,
	'default_activation' => true,
];
