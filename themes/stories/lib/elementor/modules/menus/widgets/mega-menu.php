<?php
namespace ElementorMenus\Modules\Menus\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Starter Widget
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Mega_Menu extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'mega-menu';
	}

	public function get_title() {
		return __( 'Mega Menu', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-nav-menu';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() {

		 

		$this->start_controls_section(
			'megamenu_content',
			[
				'label' => __( 'MegaMenu Content', 'Stories' ),
			]
		);
		$this->start_controls_tabs( 'megamenu_background_tabs' ,
			[
				'label' => __( 'Menu Background Color', 'Stories' ), 
			]
		);
		
		$this->start_controls_tab(
			'megamenu_background_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);

		$this->add_control(
			'megamenu_background_normal',
			[
				'label'     => __( 'Background Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => 'rgba(0,0,0,0)', 
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu' => 'background: {{VALUE}} !important;',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'megamenu_background_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'megamenu_background_hover',
			[
				'label'     => __( 'Background Color Hover', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => 'rgba(0,0,0,0)', 
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu:hover' => 'background: {{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();   
		
		$this->add_control(
			'megamenu_container_border',
			[
				'label'      => __( 'Menu Container Border', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS, 
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}  !important; border-style:solid !important;',
				],
			]
		);

		$this->start_controls_tabs( 'megamenu_container_border_color_tabs' );
		
		$this->start_controls_tab(
			'megamenu_container_border_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		
		$this->add_control(
			'megamenu_item_color_normal',
			[
				'label'     => __( 'Menu Container Border Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu' => 'border-color:{{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'megamenu_item_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'megamenu_item_color_hover',
			[
				'label'     => __( 'Menu Items Color Hover', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu:hover' => 'border-color: {{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();  


		$this->add_control(
			'megamenu_padding',
			[
				'label'      => __( 'Menu Container Padding', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}  !important;',
				],
			]
		);

		$this->add_control(
			'megamenu_item_align',
			[
				'label' => __( 'Align Items', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SELECT, 
				'options' => [
					'center' => __( 'Center', 'Stories' ), 
					'left'  => __( 'Left', 'Stories' ),
					'right'  => __( 'Right', 'Stories' ),
				],
				'default'   => 'center',
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu' => 'text-align: {{VALUE}} !important;',  
				],
			]
		);
 
		$this->start_controls_tabs( 'megamenu_item_color_tabs' );
		
		$this->start_controls_tab(
			'megamenu_item_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		
		$this->add_control(
			'megamenu_item_color_normal',
			[
				'label'     => __( 'Menu Items Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu li a' => 'color: {{VALUE}} !important;',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'megamenu_item_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'megamenu_item_color_hover',
			[
				'label'     => __( 'Menu Items Color Hover', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu li:hover a' => 'color: {{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();  

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'megamenu_item_typo',
				'label' => __( 'Menu Item Typography', 'Stories' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mega-menu-wrap .mega-menu li a',
			]
		);

		$this->add_control(
			'megamenu_item_border_bottom',
			[
				'label' => __( 'Menu Item Bottom Border', 'Stories' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px','%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 10,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 0,
				],
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu li a' => 'border-bottom: {{SIZE}}{{UNIT}} solid !important;',
				],
			]
		);

		
		$this->start_controls_tabs( 'megamenu_item_border_color_tabs' );
		
		$this->start_controls_tab(
			'megamenu_item_border_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		
		$this->add_control(
			'megamenu_item_border_color_normal',
			[
				'label'     => __( 'Menu Items Border Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu li a' => 'border-color: {{VALUE}} !important;',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'megamenu_item_border_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'megamenu_item_border_color_hover',
			[
				'label'     => __( 'Menu Items Color Hover', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333', 
				'selectors' => [
					'{{WRAPPER}} .mega-menu-wrap .mega-menu li:hover a' => 'border-color: {{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs(); 

		$this->end_controls_section(); 

	}

	protected function render() {
	// Improve TR: Burada Mega Menu Theme Oluşturacağız
		$settings = $this->get_settings(); 
		wp_nav_menu( array( 'theme_location' => 'elementor') ); 
	}

	public function create_mega_menu_theme () {

	}
	protected function _content_template() {}
}
