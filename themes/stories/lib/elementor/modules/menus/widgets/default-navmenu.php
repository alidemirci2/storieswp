<?php
namespace ElementorMenus\Modules\Menus\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Elementor Navbar
 *
 * Elementor widget for Default Navmenu.
 *
 * @since 1.0.0
 */
class Default_Navmenu extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'default-navmenu';
	}

	public function get_title() {
		return __( 'Default Navmenu', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-nav-menu';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() {
		// $menus = $this->get_menus();

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Navigation', 'Stories' ),
			]
		);

		$this->add_control(
			'el_nav_menu',
			[
				'label'   => __( 'Select Menu', 'Stories' ),
				'type'    => Controls_Manager::SELECT, 'options' => navmenu_navbar_menu_choices(),
				'default' => '',
			]
		);

		$this->add_control(
			'el_menu_location',
			[
				'label'       => __( 'Menu Location', 'Stories' ),
				'description' => __( 'Select a location for your menu. This option facilitate the ability to create up to 2 mobile enabled menu locations', 'Stories' ),
				'type'        => Controls_Manager::SELECT, 'options' => [
					'primary'   => __( 'Primary', 'Stories' ),
					'secondary' => __( 'Secondary', 'Stories' ),
				],
				'default'     => 'primary',
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'        => __( 'Navbar/Toggle Alignment', 'Stories' ),
				'type'         => Controls_Manager::CHOOSE,
				'options'      => [
					'left'   => [
						'title' => __( 'Left', 'Stories' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'Stories' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => __( 'Right', 'Stories' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'default'      => 'center',
			]
		);

		$this->add_responsive_control(
			'item_align',
			[
				'label'     => __( 'Mobile Item Alignment', 'Stories' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => __( 'Left', 'Stories' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'Stories' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => __( 'Right', 'Stories' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .stories-menu ul li, .stories-menu ul ul li' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'text_padding',
			[
				'label'      => __( 'Text Padding', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'default' => [

					'right' => "0",

					'left' => "0",
					'isLinked' => 'no',
				],
				'selectors'  => [
					'{{WRAPPER}} .stories-menu a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();





		

		$this->start_controls_section(
			'section_menu_style',
			[
				'label' => __( 'Navbar', 'Stories' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'nav_bar_bg',
			[
				'label'     => __( 'Navbar Background', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .stories-menu,ul.mini-menu li ul' => 'background-color: {{VALUE}};',
				],
			]
		);
		
		$this->start_controls_tabs(
			'el_overlay_menu_text_color_tabs' ,
			[
				'label' => __( 'Menu Item Style', 'Stories' ),
			]
		);

		$this->start_controls_tab(
			'el_overlay_menu_text_color_tab_normale',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);


		$this->add_control(
			'menu_link_color',
			[
				'label'     => __( 'Link Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .stories-menu .menu-item a' => 'color: {{VALUE}};',
				],
			]
		);


		$this->add_control(
			'menu_link_bg',
			[
				'label'     => __( 'Background', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .stories-menu .menu-item a' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'el_overlay_menu_text_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
	$this->add_control(
			'menu_link_hover_color',
			[
				'label'     => __( 'Hover Link Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333',
				'selectors' => [
					'{{WRAPPER}} .stories-menu .menu-item a:hover' => 'color: {{VALUE}};',
				],
			]
		);

	$this->add_control(
			'link_hover_bg_color',
			[
				'label'     => __( 'Hover Background Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .stories-menu .menu-item a:hover,.mini-menu a::after' => 'background: {{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs(); 


		$this->add_control(
			'hr',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_control(
			'menu_padding',
			[
				'label'      => __( 'Menu Padding', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'default' => [
				'top' => "10",
				'right' => "10",
				'bottom' => "10",
				'left' => "10",
				'isLinked' => 'yes',
				],
				'selectors'  => [
					'{{WRAPPER}} .stories-menu nav ul.mini-menu li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .stories-menu nav ul.mini-menu>li>ul' => 'margin-left: {{LEFT}}{{UNIT}} !important;',
				],
			]
		);
	
	
		$this->add_control(
			'hrw',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);


		
		$this->add_control(
			'menu_border',
			[
				'label'      => __( 'Border', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'default' => [
				'top' => "3",
				'right' => "0",
				'bottom' => "1",
				'left' => "0",
				'isLinked' => false,
				],
				'selectors'  => [
					'{{WRAPPER}} .stories-menu' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .stories-menu nav ul.mini-menu>li>ul' => 'border-top-width: {{BOTTOM}}{{UNIT}} !important;',
				],
			]
		);

			$this->add_control(
			'border_bottom_color',
			[
				'label'     => __( 'Border Bottom Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default' => '#eee',
				'selectors' => [
					'{{WRAPPER}} .stories-menu' => 'border-bottom-color: {{VALUE}};',
					'{{WRAPPER}} .stories-menu nav ul.mini-menu li>ul' => 'border-top-color: {{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_section();


	


		$this->start_controls_section(
			'active_color',
			[
				'label' => __( 'Current/Active', 'Stories' ),
				'type'  => Controls_Manager::SECTION,
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);



	$this->start_controls_tabs(
			'active_color_tabs' ,
			[
				'label' => __( 'Menu Item Style', 'Stories' ),
			]
		);

		$this->start_controls_tab(
			'active_color_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);


		$this->add_control(
			'menu_link_active_color',
			[
				'label'     => __( 'Active Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .stories-menu .current-menu-item > a, .stories-menu .current_page_item > a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'link_active_bg_color',
			[
				'label'     => __( 'Active Background', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .stories-menu .current-menu-item > a, .stories-menu .current_page_item > a' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'active_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);

		$this->add_control(
			'active_hover_color',
			[
				'label'     => __( 'Active Link', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .stories-menu .current-menu-item > a:hover, .stories-menu .current_page_item > a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'active_hover_bg_color',
			[
				'label'     => __( 'Active Background', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .stories-menu .current-menu-item > a:hover, .stories-menu .current_page_item > a:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs(); 

		$this->add_control(
			'hr3',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'active_border',
				'label'    => __( 'Border', 'Stories' ),
				'default'  => '1px',
				'selector' => '{{WRAPPER}} .stories-menu .current-menu-item > a, .stories-menu .current_page_item > a',
			]
		);

		$this->add_control(
			'active_radius',
			[
				'label'      => __( 'Border Radius', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .stories-menu .current-menu-item > a, .stories-menu .current_page_item > a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'submenu_color',
			[
				'label' => __( 'Submenu', 'Stories' ),
				'type'  => Controls_Manager::SECTION,
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'submenu_link_color',
			[
				'label'     => __( 'Submenu Links', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .stories-menu .sub-menu .menu-item a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'submenu_link_bg',
			[
				'label'     => __( 'Submenu Background', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .stories-menu .sub-menu, {{WRAPPER}} .stories-menu .sub-menu .menu-item a' => 'background-color: {{VALUE}};',
				],
			]
		);

	




		$this->end_controls_section();

		$this->start_controls_section(
			'menu_toggle',
			[
				'label' => __( 'Mobile Toggle', 'Stories' ),
				'type'  => Controls_Manager::SECTION,
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'toggle_icon_color',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .stories-menu-toggle i.fa.fa-navicon' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'toggle_bg_color',
			[
				'label'     => __( 'Background Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .stories-menu-toggle' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'toggle_icon_hover',
			[
				'label'     => __( 'Icon Hover', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .stories-menu-toggle:hover i.fa.fa-navicon' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'toggle_bg_hover',
			[
				'label'     => __( 'Background Hover', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .stories-menu-toggle:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'toggle_text_padding',
			[
				'label'      => __( 'Text Padding - Default 1em', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .stories-menu-toggle i.fa.fa-navicon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'toggle_border',
				'label'    => __( 'Border', 'Stories' ),
				'default'  => '1px',
				'selector' => '{{WRAPPER}} .stories-menu-toggle',
			]
		);

		$this->add_control(
			'toggle_border_radius',
			[
				'label'      => __( 'Border Radius', 'Stories' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .stories-menu-toggle' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'menu_typography',
			[
				'label' => __( 'Typography', 'Stories' ),
				'type'  => Controls_Manager::SECTION,
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'menu_typography',
				'label'    => __( 'Typography', 'Stories' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mini-menu a',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {

		$settings      = $this->get_settings();
		$menu_location = $settings['el_menu_location'];
		// Get menu
		$nav_menu = ! empty( $settings['el_nav_menu'] ) ? wp_get_nav_menu_object( $settings['el_nav_menu'] ) : false;

		if ( ! $nav_menu ) {
			return;
		}

		$nav_menu_args = array(
			'fallback_cb'    => false,
			'container'      => false,
			'menu_id'        => 'mini-menu',
			'menu_class'     => 'nav-collapse mini-menu sf-menu superfish',
			'theme_location' => 'default_navmenu', // creating a fake location for better functional control
			'menu'           => $nav_menu,
			'echo'           => true,
			'depth'          => 0,
			'walker'         => '',
		);

		echo '<div id="elementor-header-' . $menu_location . '" class="elementor-header">';
		?>
			<div id="stories-menu" class="stories-menu">
			
				<nav itemtype="http://schema.org/SiteNavigationElement" itemscope="itemscope" role="navigation" aria-label="<?php esc_attr_e( 'Main Menu', 'Stories' ); ?>">				
				<?php
					wp_nav_menu(
						apply_filters(
							'widget_nav_menu_args',
							$nav_menu_args,
							$nav_menu,
							$settings
						)
					);
				?>
				</nav>
			</div>
		</div>
		<?php
	}

	protected function _content_template() {}
}
