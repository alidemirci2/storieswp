<?php
namespace ElementorMenus\Modules\Menus\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Overlay NavMenu
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Navmenu_Overlay extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'navmenu-overlay';
	}

	public function get_title() {
		return __( 'Overlay NavMenu', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-nav-menu';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() {

		
		global $wp_registered_sidebars ;

		$options = [];

		if ( ! $wp_registered_sidebars ) {
			$options[''] = __( 'No sidebars were found', 'Stories' );
		} else {
			$options[''] = __( 'Choose Sidebar', 'Stories' );

			foreach ( $wp_registered_sidebars as $sidebar_id => $sidebar ) {
				$options[ $sidebar_id ] = $sidebar['name'];
			}
		}

		$default_key = array_keys( $options );
		$default_key = array_shift( $default_key );


		$this->start_controls_section(
			'overlay_content',
			[
				'label' => __( 'Menu', 'Stories' ),
			]
		);
		
		$this->add_control(
			'el_overlay_menu',
			[
				'label'   => __( 'Select Menu', 'Stories' ),
				'type'    => Controls_Manager::SELECT, 'options' => navmenu_navbar_menu_choices(),
				'default' => '',
			]
		);
		
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'el_overlay_menu_typography',
				'label'    => __( 'Typography', 'Stories' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .overlay-menu .row .content nav ul li a',
			]
		);

		
		$this->start_controls_tabs(
			'el_overlay_menu_text_color_tabs' ,
			[
				'label' => __( 'Menu Item Style', 'Stories' ),
			]
		);

		$this->start_controls_tab(
			'el_overlay_menu_text_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'el_overlay_menu_text_color',
			[
				'label'     => __( 'Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .overlay-menu .row .content nav ul li a' => 'color: {{VALUE}};',
					'{{WRAPPER}} .overlay-menu .row .content nav ul li.menu-item-has-children > a:after' => 'border-color: {{VALUE}} !important;',
				],
			]

			
		);

	
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'el_overlay_menu_text_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'el_overlay_menu_text_color_hover',
			[
				'label'     => __( 'Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .overlay-menu .row .content nav ul li a:hover' => 'color: {{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_tab(); 

		
		$this->start_controls_tab(
			'el_overlay_menu_text_color_tab_active',
			[
				'label' => __( 'Active', 'Stories' ),
			]
		);
		$this->add_control(
			'el_overlay_menu_text_color_active',
			[
				'label'     => __( 'Active Item Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .overlay-menu .row .content nav ul li.current-menu-item>a' => 'color: {{VALUE}};',
				],
			]
		); 
		
		$this->add_control(
			'el_overlay_menu_text_border_active',
			[
				'label' => __( 'Active Item Border', 'Stories' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .overlay-menu .row .content nav ul li.current-menu-item>a' => 'border-top-width: {{TOP}}{{UNIT}}; border-right-width: {{RIGHT}}{{UNIT}}; border-bottom-width: {{BOTTOM}}{{UNIT}}; border-left-width: {{LEFT}}{{UNIT}};',
				],
			]
		);

		
		$this->add_control(
			'el_overlay_menu_text_border_color_active',
			[
				'label'     => __( 'Active Item Border Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .overlay-menu .row .content nav ul li.current-menu-item>a' => 'border-color: {{VALUE}};',
				],
			]
		); 

		$this->end_controls_tab(); 


		$this->end_controls_tabs(); 

		$this->add_control(
			'show_sub_menu',
			[
				'label' => __( 'If Menu Has Sub-Menu', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'Stories' ),
				'label_off' => __( 'Hide', 'Stories' ), 
				'default' => 'yes',
			]
		);

		$this->add_control(
			'sub_menu_arrow',
			[
				'label' => __( 'Sub Menu Arrow', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'arrow',
				'options' => [
					'chevron'  => __( 'Chevron', 'Stories' ),
					'arrow' => __( 'Arrow', 'Stories' ), 
				],

				'condition' => [
					'show_sub_menu' => 'yes',
				],
			]
		);

		$this->add_control(
			'background_image',
			[
				'label' => __( 'Overlay Menu Background Image', 'Stories' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		); 
		$this->add_control(
			'background_image_position',
			[
				'label' => __( 'Background Image Position', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'right',
				'options' => [
					'right'  => __( 'Right', 'Stories' ),
					'left' => __( 'Left', 'Stories' ), 
				], 
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'toggle_style',
			[
				'label' => __( 'Toggle Style', 'Stories' ), 
			]
		);

		$this->add_control(
			'toggle_color',
			[
				'label'     => __( 'Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333',
				'selectors' => [
					'{{WRAPPER}} .toggle-menu span,.toggle-menu span:after,.toggle-menu span:before,.toggle-menu.on span:before,.toggle-menu.on span:after' => 'background-color: {{VALUE}}  !important;', 
				],
			]
		);

		$this->add_control(
			'toggle_bg_color',
			[
				'label'     => __( 'Background', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#FFF',
				'selectors' => [
					'{{WRAPPER}} .toggle-menu' => 'background-color: {{VALUE}};',
				],
			]
		);

	
		$this->end_controls_section(); 

		$this->start_controls_section(
			'section_widgets',
			[
				'label' => __( 'Widgets', 'Stories' ) 
			]
		);

		
		$this->add_control( 'sidebar1', [
			'label' => __( 'Widget 1', 'Stories' ),
			'type' => Controls_Manager::SELECT,
			'default' => $default_key,
			'options' => $options,
		] );
		
		$this->add_control( 'sidebar2', [
			'label' => __( 'Widget 2', 'Stories' ),
			'type' => Controls_Manager::SELECT,
			'default' => $default_key,
			'options' => $options,
		] );
		
		$this->add_control( 'sidebar3', [
			'label' => __( 'Widget 3', 'Stories' ),
			'type' => Controls_Manager::SELECT,
			'default' => $default_key,
			'options' => $options,
		] );
		
		$this->end_controls_section();  
	}

	protected function render() {

		$settings = $this->get_settings();
		if($settings["background_image"]) {
			$backgroundImage = $settings["background_image"]["url"];
		} else {
			$backgroundImage = "";
		}
		if ($settings["show_sub_menu"] == "yes") {
			$depth = 0;
		} else {
			$depth = 1;
		}
		// Get menu
		$overlay_menu = ! empty( $settings['el_overlay_menu'] ) ? wp_get_nav_menu_object( $settings['el_overlay_menu'] ) : false;

		if ( ! $overlay_menu ) {
			return;
		}

		$overlay_menu_args = array(
			'fallback_cb'    => false,
			'container'      => false,
			'menu_id'        => '',
			'menu_class'     => '',
			'theme_location' => 'nav_overlay_menu', // creating a fake location for better functional control
			'menu'           => $overlay_menu,
			'echo'           => true, 
			'walker'         => '',
			'depth' 	     => $depth
		); 
		?> 
		
		<div class="toggle-menu"><span class="first-line"></span><span class="second-line"></span><span class="third-line"></span><div class="clearfix"></div></div> 
			<?php 
				if ( is_user_logged_in() ) {
					echo '
						<style>
							.overlay-menu {
								padding-top:32px;
							}
						</style>
					';
				}
			?>
		<div class="overlay-menu"> 
			<div class="row">
				<div class="col-lg-7 col-md-8 col-sm-8 content" style="background-image:url(<?php if ($settings["background_image_position"] == "left") { echo esc_url($backgroundImage); }  ?>)">
					<div class="site-branding"> 
						<div class="close-overlay-menu">
							<span class="first-line"></span>
							<span class="second-line"></span>
						<!-- /.close-overlay-menu --></div>
						<?php //!WPORG customizer logo ve alternatifin yazımı
						if(has_custom_logo()){ //Customizer Logo Seçimi varsa
							print_r(get_custom_logo());
						} else {
							$url = home_url();
						?>
								<div class="logo-text">
									<a href="<?php 	echo esc_url(home_url('/')); ?>"><?php echo esc_attr(get_bloginfo('name')); ?></a>
								</div>						
							<?php } ?>
					<!-- /site-branding --></div>
					<nav class="overlay" itemtype="http://schema.org/SiteNavigationElement" itemscope="itemscope" role="navigation" aria-label="<?php esc_attr_e( 'Elementor Menu', 'Stories' ); ?>">
						<?php
							wp_nav_menu(
								apply_filters(
									'widget_nav_menu_args',
									$overlay_menu_args
								)
							);
						?>
					</nav> 
					<div class="overlay-menu-widgets col-12">
						<div class="row">
						<div class="<?php if (empty( $settings["sidebar3"] ) ) {?>col-lg-6 col-md-6 <?php } else { ?> col-lg-4 col-sm-4 <?php } ?> col-12 widget">
								<?php 
									if ( !empty( $settings["sidebar1"] ) ) {
										dynamic_sidebar(  $settings["sidebar1"] );
									} 
								?>
							<!-- /.widget --></div>
							<div class="<?php if (empty( $settings["sidebar3"] ) ) {?>col-lg-6 col-md-6 <?php } else { ?> col-lg-4 col-sm-4 <?php } ?>  col-12 widget">
								<?php 
									if ( !empty( $settings["sidebar2"] ) ) {
										dynamic_sidebar(  $settings["sidebar2"] );
									} 
								?>
							<!-- /.widget --></div>
							<div class="col-lg-4 col-sm-4 col-12 widget">
								<?php 
									if ( !empty( $settings["sidebar3"] ) ) {
										dynamic_sidebar(  $settings["sidebar3"] );
									} 
								?>
							<!-- /.widget --></div>
						<!-- /.row --></div>
					<!-- /.overlay-menu-widgets --></div>
				</div>
								<div class="col-lg-5 col-md-4 col-sm-4 d-none d-sm-block image" style="<?php if ($settings["background_image_position"] == "right") { ?> background-image:url(<?php echo esc_attr($backgroundImage); ?>) <?php } else {?> display:none; <?php }?>"> 
				</div>
			</div>				
		<!-- /.overlay-menu --></div>
		<?php
	}

	protected function _content_template() {}
}
