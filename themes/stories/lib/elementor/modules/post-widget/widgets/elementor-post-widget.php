<?php
namespace ElementorMenus\Modules\PostWidget\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Elementor PostWidget
 *
 *
 * @since 1.0.0
 */
class Elementor_PostWidget extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-Post-widget';
	}

	public function get_title() {
		return __( 'Post Widget', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() { 
		// Subscribe Button
		$this->start_controls_section(
			'section_post_widget_button',
			[
				'label' => __( 'Post Widget Button', 'Stories' ),
			]
		); 
		
		 
		$this->add_control(
			'post_widget_button_text',
			[			 
				'label' =>  __( 'Button Text', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html( __( 'Trending', 'Stories' ) ),  
			]
		); 
		$this->start_controls_tabs( 'post_widget_button_text_color_tabs' );

		$this->start_controls_tab(
			'post_widget_button_text_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'post_widget_button_text_color',
			[
				'label'     => __( 'Button Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .post-widget .post-widget-button button' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'post_widget_button_text_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'post_widget_button_text_color_hover',
			[
				'label'     => __( 'Button Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .post-widget .post-widget-button button:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'post_widget_button_text_style',
				'label' =>  __( 'Text Style', 'Stories' ),
				'selector' => '{{WRAPPER}} .post-widget .post-widget-button button span ',
				'scheme' => Scheme_Typography::TYPOGRAPHY_3,
			]
		);
		 
		$this->add_control(
			'post_widget_button_icon',
			[
				'label' => __( 'Post Widget Icon', 'Stories' ),
				'type' => \Elementor\Controls_Manager::ICON,
				'include' => [
					'fa fa-search',
					'fa fa-facebook',
					'fa fa-flickr',
					'fa fa-google-plus',
					'fa fa-instagram',
					'fa fa-linkedin',
					'fa fa-pinterest',
					'fa fa-reddit',
					'fa fa-twitch',
					'fa fa-twitter',
					'fa fa-vimeo',
					'fa fa-youtube',
				],
				'default' => '',
			]
		);

		$this->start_controls_tabs( 'post_widget_button_icon_color_tabs' , [
			'condition' => [
				'post_widget_button_icon!' => '', 
			],
		] );

		$this->start_controls_tab(
			'post_widget_button_icon_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'post_widget_button_icon_color',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .post-widget .post-widget-button button span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'post_widget_button_icon_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'post_widget_button_icon_color_hover',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .post-widget .post-widget-button button i:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();

		$this->add_control(
			'post_widget_button__icon_size',
			[
				'label' => __( 'Icon Size', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px', 'em', 'rem' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 200,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 16,
				],
				'selectors' => [
					'{{WRAPPER}} .icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'section_button_icon!' => '', 
				],
			]
		); 

		$this->end_controls_section();   

		$this->start_controls_section(
			'post_widget_popup_section',
			[
				'label' => __( 'Post Widget Popup Style', 'Stories' ) 
			]
		);

		$this->add_responsive_control(
			'space_between',
			[
				'label' => __( 'Spacing', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1900,
					],
					'%' => [ 
						'min' => 0,
						'max' => 100,
					]
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 300,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 200,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 100,
					'unit' => '%',
				],
				'selectors' => [
					'body {{WRAPPER}} .post-widget-overlay' => 'width: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render() { 
		$settings = $this->get_settings(); ?> 
		<div class="post-widget"> 
			<div class="post-widget-button">
				<button>
					<?php 
						if ($settings["post_widget_button_icon"]) {
					?>
						<i class="icon <?php echo esc_attr($settings['post_widget_button_icon'], 'Stories') ?>"></i>
					<?php
						}
					?>
					<span><?php echo esc_attr($settings['post_widget_button_text']); ?></span>
				</button> 
			<!-- /.post-widget-button --></div>  
			<div class="post-widget-overlay box-shadow <?php if ( is_user_logged_in() ) { esc_attr_e( 'admin-bar-spacer', 'Stories' ); } ?>">
				<div class="overlay-container"> 
					<?php dynamic_sidebar( 'post-widget' ); ?>   
				<!-- /.overlay-container --></div>	
			<!-- /.post-widget-overlay --></div> 
		<!-- /.post-widget --></div>
		<?php
	}

	protected function _content_template() {}
}
