<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

return [
	'title'              => __( 'Post Widget', 'Stories' ),
	'required'           => true,
	'default_activation' => true,
];
