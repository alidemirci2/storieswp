<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

return [
	'title'              => __( 'Newsletter', 'Stories' ),
	'required'           => true,
	'default_activation' => true,
];
