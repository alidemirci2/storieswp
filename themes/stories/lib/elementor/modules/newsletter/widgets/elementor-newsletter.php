<?php
namespace ElementorMenus\Modules\Newsletter\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Elementor Newsletter
 *
 *
 * @since 1.0.0
 */
class Elementor_Newsletter extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-newsletter';
	}

	public function get_title() {
		return __( 'Newsletter', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-mail';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() { 
		// Subscribe Button
		$this->start_controls_section(
			'section_button_content',
			[
				'label' => __( 'Subscribe Button', 'Stories' ),
			]
		); 
		
		 
		$this->add_control(
			'section_button_text',
			[			 
				'label' =>  __( 'Button Text', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html( __( 'Subscribe', 'Stories' ) ),  
			]
		); 
		$this->start_controls_tabs( 'search_text_color_tabs' );

		$this->start_controls_tab(
			'section_button_text_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'section_button_text_color',
			[
				'label'     => __( 'Button Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .newsletter-link a, {{WRAPPER}} .newsletter-button button' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'section_button_text_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'section_button_text_color_hover',
			[
				'label'     => __( 'Button Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .newsletter-link a:hover, {{WRAPPER}} .newsletter-button button:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_text_style',
				'label' =>  __( 'Subscribe Button Text Style', 'Stories' ),
				'selector' => '{{WRAPPER}} .newsletter-link a, {{WRAPPER}} .newsletter-button button ',
				'scheme' => Scheme_Typography::TYPOGRAPHY_3,
			]
		);
		
		// Search Button Icon
		$this->add_control(
			'section_button_icon',
			[
				'label' => __( 'Search Icon', 'Stories' ),
				'type' => \Elementor\Controls_Manager::ICON,
				'include' => [
					'fa fa-search',
					'fa fa-facebook',
					'fa fa-flickr',
					'fa fa-google-plus',
					'fa fa-instagram',
					'fa fa-linkedin',
					'fa fa-pinterest',
					'fa fa-reddit',
					'fa fa-twitch',
					'fa fa-twitter',
					'fa fa-vimeo',
					'fa fa-youtube',
				],
				'default' => '',
			]
		);

		$this->start_controls_tabs( 'section_button_icon_color_tabs' , [
			'condition' => [
				'section_button_icon!' => '', 
			],
		] );

		$this->start_controls_tab(
			'section_button_icon_color_tab_normal',
			[
				'label' => __( 'Normal', 'Stories' ),
			]
		);
		$this->add_control(
			'section_button_icon_color',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .newsletter-link a .icon, {{WRAPPER}} .newsletter-button button .icon' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab(); 

		$this->start_controls_tab(
			'section_button_icon_color_tab_hover',
			[
				'label' => __( 'Hover', 'Stories' ),
			]
		);
		$this->add_control(
			'section_button_icon_color_hover',
			[
				'label'     => __( 'Icon Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .newsletter-link a:hover .icon, {{WRAPPER}} .newsletter-button button:hover .icon' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab(); 

		$this->end_controls_tabs();

		$this->add_control(
			'section_button_icon_size',
			[
				'label' => __( 'Icon Size', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px', 'em', 'rem' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 200,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 16,
				],
				'selectors' => [
					'{{WRAPPER}} .icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'section_button_icon!' => '', 
				],
			]
		);

		$this->add_control(
			'subscribe_type',
			[
				'label' => __( 'Select Subcscribe Button Type', 'Stories' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'direct_link' => __( 'Direct Link', 'Stories' ),
					'popup' => __( 'Pop-up', 'Stories' ), 
				],
				'default' => 'direct_link',  
			]
		);
		$this->add_control(
			'subscribe_link',
			[			 
				'label' =>  __( 'Subscribe Link', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_attr( __( '#', 'Stories' ) ), 
				'condition' => [
					'subscribe_type' => 'direct_link',
				],
			]
		); 

		$this->end_controls_section(); 

		$this->start_controls_section(
			'section_subscribe_popup',
			[
				'label' => __( 'Subscribe Popup Style', 'Stories' ),
				'condition' => [
					'subscribe_type' => 'popup',
				],
			]
		); 
		
		
		$this->add_control(
			'section_popup_title',
			[			 
				'label' =>  __( 'Subscribe Popup Title', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_attr( __( 'Subscribe to Newsletter', 'Stories' ) ),  
			]
		); 
		
		$this->add_control(
			'section_popup_form_placeholder',
			[			 
				'label' =>  __( 'Subscribe Form Text', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_attr( __( 'E-MAIL', 'Stories' ) ),  
			]
		); 
		
		$this->add_control(
			'section_popup_submit_subscribe_link',
			[			 
				'label' =>  __( 'Submit to Subscribe Link', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_attr( __( '#', 'Stories' ) ),  
			]
		); 
		
		$this->add_control(
			'section_popup_form_agreement_text',
			[			 
				'label' =>  __( 'Subscribe Agreement Text', 'Stories' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_attr( __( 'I agree GDPR Compliant Terms & Conditions', 'Stories' ) ),  
			]
		); 

		$this->end_controls_section();
	}

	protected function render() { 
		$settings = $this->get_settings(); ?> 
		<div class="newsletter">
			<?php if ($settings["subscribe_type"] == "direct_link" ) { ?>
				<div class="newsletter-link">
					<a href="<?php echo esc_url($settings['subscribe_link']); ?>">
						<?php 
							if ($settings["section_button_icon"]) {
						?>
							<i class="icon <?php echo esc_attr($settings['section_button_icon']); ?>"></i>
						<?php
							}
						?>
						<span><?php ECHO esc_attr($settings['section_button_text']); ?></span>
					</a>
				<!-- /.newsletter-link --></div>  
			<?php } else { ?>
			<div class="newsletter-button">
				<button>
					<?php 
						if ($settings["section_button_icon"]) {
					?>
						<i class="icon <?php echo esc_attr($settings['section_button_icon']); ?>"></i>
					<?php
						}
					?>
					<span><?php ECHO esc_attr($settings['section_button_text']); ?></span>
				</button> 
			<!-- /.newsletter-button --></div>  
			<div class="newsletter-overlay box-shadow <?php if ( is_user_logged_in() ) { esc_attr_e( 'admin-bar-spacer', 'Stories' ); } ?>">
				<div class="overlay-container">   
					<p class="title secondary-font"><?php echo esc_attr($settings['section_popup_title']); ?></p>
					<div class="newsletter-form">  
						<form  action="<?php echo esc_url($settings['section_popup_submit_subscribe_link']); ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">	
							<div class="newsletter-input form-element"> 
									<label><?php echo esc_attr($settings['section_popup_form_placeholder']); ?></label>
									  <input type="email" value="" name="EMAIL"  class="required email" id="mce-EMAIL"> 
									<button type="submit"> <?php get_template_part('/assets/svg/arrow-thing-right.svg');  ?></button>
							</div>	
							<div class="clearfix"></div>
							<label class="checkbox">
  							<span class="checkmark"></span><input class="checkbox-item" type="checkbox" name="checkbox" value="terms"><?php echo esc_attr($settings['section_popup_form_agreement_text']); ?></label>
						</form>
					<!-- /.newsletter-form --></div>
				<!-- /.overlay-container --></div>	
			<!-- /.newsletter-overlay --></div>
			<?php } ?>
		<!-- /.newsletter --></div>
		<?php
	}

	protected function _content_template() {}
}
