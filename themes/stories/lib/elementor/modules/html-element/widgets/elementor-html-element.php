<?php
namespace ElementorMenus\Modules\HtmlElement\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor HTML Element
 *
 *
 * @since 1.0.0
 */
class Elementor_HtmlElement extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-html-element';
	}

	public function get_title() {
		return __( 'HTML Element', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-coding';
	}
 

	public function get_categories() {
		return [ 'branding-elements' ];
	}
	protected function _register_controls() { 
		$this->start_controls_section(
			'html_el_content',
			[
				'label' => __( 'Content', 'Stories' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'html_el_widget',
			[
				'label' => __( 'HTML', 'Stories' ),
				'type' => \Elementor\Controls_Manager::CODE,
				'language' => 'html',
				'rows' => 10,
				'default' => __( 'HTML Code Here', 'Stories' ),
			]
		);

		$this->end_controls_section();

	}
	protected function render() { 
		$settings = $this->get_settings(); 
		$res =  $settings['html_el_widget'];  
		$allowed_tags = wp_kses_allowed_html( 'post' );
	?> 
		<div class="html-element">
			 <?php echo wp_kses ($res, $allowed_tags ) ; ?>
		<!-- /.html-element --></div>
	<?php
		
	}

	protected function _content_template() {}
}
