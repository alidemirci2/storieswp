<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

return [
	'title'              => __( 'HTML Element', 'Stories' ),
	'required'           => true,
	'default_activation' => true,
];
