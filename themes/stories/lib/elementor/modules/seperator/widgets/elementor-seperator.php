<?php
namespace ElementorMenus\Modules\Seperator\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Elementor Seperator
 *
 *
 * @since 1.0.0
 */
class Elementor_Seperator extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'elementor-sepertor';
	}

	public function get_title() {
		return __( 'Seperator Horizantal', 'Stories' );
	}

	public function get_icon() {
		return 'eicon-ellipsis-v';
	}

	public function get_categories() {
		return [ 'branding-elements' ];
	}

	protected function _register_controls() { 
		$this->start_controls_section(
			'seperator_section',
			[
				'label' => __( 'Seperator', 'Stories' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		// Seperator Type
		$this->add_control(
			'seperator_type',
			[
				'label' => __( 'Seperator Type', 'Stories' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'solid',
				'options' => [
					'slash'  => __( 'Slash', 'Stories' ),
					'line' => __( 'Line', 'Stories' ), 
					'dots' => __( 'Dots', 'Stories' ), 
				],
				'default'   => 'line', 
			]
		); 

		// Seperator Width
		$this->add_control(
			'seperator_width',
			[
				'label' => __( 'Seperator Width', 'Stories' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 1,
				],
				'selectors' => [
					'{{WRAPPER}} .seperator-widget' => 'width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .seperator-widget i:after' => 'border-left-width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		
		// Seperator Height
		$this->add_control(
			'seperator_height',
			[
				'label' => __( 'Seperator Height', 'Stories' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 28,
				],
				'selectors' => [
					'{{WRAPPER}} .seperator-widget' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		// Seperator Color
		$this->add_control(
			'seperator_color',
			[
				'label'     => __( 'Search Text Color', 'Stories' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .seperator-widget i:after' => 'border-color: {{VALUE}};',
				],
			]
		);


		$this->end_controls_section();

	}

	protected function render() { 
		$settings = $this->get_settings(); 
		?> 
		 <div class="seperator-widget"><i class="<?php echo attr($settings["seperator_type"]); ?>"></i></div>
		<?php
	}

	protected function _content_template() {}
}
