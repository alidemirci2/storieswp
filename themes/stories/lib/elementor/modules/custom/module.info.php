<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

return [
	'title'              => __( 'Custom', 'Stories' ),
	'required'           => true,
	'default_activation' => true,
];
