<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CoverNews
 */
$index = new \EPIC\Archive\IndexArchive();
get_header();  


?>
        
        <?php 
        // burak Boyle veri cekiliyor
       // echo epic_get_option( 'index_sidebar1' ); 
        // default ile cekme
       //echo epic_get_option( 'site_layout','default' );  
      //var_dump($index->main_class());
    ?>
 
<div class="stories_viewport">
<section class="section-block-upper">
    <div class="container">
        <div class="main-row row">

                    <?php get_sidebar(); ?>
                    <div id="primary" class="content-area main-column <?php /* echo esc_attr(get_column_size('index'));  */  $index->main_class(); ?>">
                        <main id="main" class="site-main">


                            <?php
                            if (have_posts()) :

                                if (is_home() && !is_front_page()) : ?>
                                    <header>
                                        <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                                    </header>

                                <?php
                                endif;

                                /* Start the Loop */
                                while (have_posts()) : the_post();

                                    /*
                                    * Include the Post-Format-specific template for the content.
                                    * If you want to override this in a child theme, then include a file
                                    * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                    */

                                    get_template_part('template-parts/content', get_post_format());


                                endwhile; ?>
                                <div class="col col-ten">
                                    <div class="covernews-pagination">
                                        <?php covernews_numeric_pagination(); ?>
                                    </div>
                                </div>
                            <?php

                            else :

                                get_template_part('template-parts/content', 'none');

                            endif; ?>


                        </main><!-- #main -->
                    </div><!-- #primary -->

                    <?php

                    ?>
            </div>
        </div>
    </section>
</div>
<?php
get_footer();
