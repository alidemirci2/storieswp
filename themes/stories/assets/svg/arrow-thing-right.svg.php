
<svg width="42px" height="30px" viewBox="0 0 42 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g transform="translate(-522.000000, -162.000000)" fill="#333333" fill-rule="nonzero">
            <g transform="translate(522.000000, 162.000000)">
                <path d="M41.6666667,15 C36.828125,10.0101176 32.0893229,5.00152941 27.2623264,0 L26.0416667,1.21323529 L38.4927951,14.1176471 L0,14.1176471 L0,15.8823529 L38.4927951,15.8823529 L26.0416667,28.7867647 L27.2623264,30 C32.1118924,25.0213235 36.8019097,19.9631471 41.6666667,15 Z" ></path>
            </g>
        </g>
    </g>
</svg>