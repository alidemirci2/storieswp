(function($) {
	"use strict";

	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (
				isMobile.Android() ||
				isMobile.BlackBerry() ||
				isMobile.iOS() ||
				isMobile.Opera() ||
				isMobile.Windows()
			);
		}
	};

	function equalHeightDiv(groupHeight) {
		groupHeight.each(function() {
			var tallest = 0;
			$(this)
				.find(".equal")
				.each(function() {
					w;
					var thisHeight = $(this).height();
					if (thisHeight > tallest) {
						tallest = thisHeight;
					}
				});
			$(this)
				.find(".equal")
				.height(tallest);
		});
	}

	$.fn.styleIsInViewport = function() {
		var elementTop = $(this).offset().top;
		var elementBottom = elementTop + $(this).outerHeight();
		var viewportTop = $(window).scrollTop();
		var viewportBottom = viewportTop + $(window).height();
		return elementBottom > viewportTop && elementTop < viewportBottom;
	};

	$(document).ready(function() {
		$(".comment-heading").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			$("html, body").animate(
				{ scrollTop: $("#comments").offset().top + 65 },
				600
			);

			setTimeout(function() {
				$(".comments-area").slideToggle("300");

				if (
					$(".comment-expand")
						.find("i")
						.hasClass("icon-chevron-right")
				) {
					$(".comment-expand")
						.find("i")
						.removeClass("icon-chevron-right");
					$(".comment-expand")
						.find("i")
						.addClass("icon-chevron-down");
				} else {
					$(".comment-expand")
						.find("i")
						.removeClass("icon-chevron-down");
					$(".comment-expand")
						.find("i")
						.addClass("icon-chevron-right");
				}
			}, 800);

			return false;
		});

		$("body").on("click", function() {
			$(".search-wrapper").removeClass("open");
			$(".search-wrapper a").removeClass("isOpenYes");
			$(".search-wrapper a").addClass("isOpenNo");
		});

		$(".mobile-menu-button").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			$(".mobile-menu-icon-wrapper").toggleClass("open");
			$(".mobile-menu-box").toggleClass("open");
		});

		$(".fancy-sidebar-button").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			$(this).toggleClass("open");
		});

		$(".search-wrapper a").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			if ($(this).hasClass("isOpenNo")) {
				$(".search-wrapper").addClass("open");
				$(".search-wrapper input").focus();
				$(".search-wrapper a").removeClass("isOpenNo");
				$(".search-wrapper a").addClass("isOpenYes");
			} else {
				$(".search-wrapper").removeClass("open");
				$(".search-wrapper a").removeClass("isOpenYes");
				$(".search-wrapper a").addClass("isOpenNo");
			}
		});

		$(".search-wrapper").click(function(e) {
			e.preventDefault();
			e.stopPropagation();
			$(".search-wrapper").addClass("open");
			$(".search-wrapper input").focus();
		});

		// Menu
		$(".sf-menu")
			.superfish({
				delay: 0
			})
			.supposition();
		// Menu

		$("h2#comments a").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			$("ol.comment-list").slideToggle("300");
		});
		//Mobile Menu Open
		$("#mobileMenuButton").on("click", function() {
			$(".primary-nav").addClass("active");
			$("#headerDefault").addClass("main-wrap--overlay");
			$(".primary-nav").append(
				'<span id="closeMenu"><svg width="22px" height="22px" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' +
					'<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">' +
					'<g transform="translate(-687.000000, -166.000000)" fill="#000000" fill-rule="nonzero">' +
					'	<g transform="translate(687.000000, 166.000000)">' +
					'		<path d="M15.17,11 L21.424375,4.745625 C22.191875,3.978125 22.191875,2.73375 21.424375,1.965625 L20.034375,0.575625 C19.266875,-0.191875 18.0225,-0.191875 17.254375,0.575625 L11,6.83 L4.745625,0.575625 C3.978125,-0.191875 2.73375,-0.191875 1.965625,0.575625 L0.575625,1.965625 C-0.191875,2.733125 -0.191875,3.9775 0.575625,4.745625 L6.83,11 L0.575625,17.254375 C-0.191875,18.021875 -0.191875,19.26625 0.575625,20.034375 L1.965625,21.424375 C2.733125,22.191875 3.978125,22.191875 4.745625,21.424375 L11,15.17 L17.254375,21.424375 C18.021875,22.191875 19.266875,22.191875 20.034375,21.424375 L21.424375,20.034375 C22.191875,19.266875 22.191875,18.0225 21.424375,17.254375 L15.17,11 Z" ></path>' +
					"		</g>" +
					"	</g>" +
					"</g>" +
					"</svg></span>"
			);
		});
		$(document).on("click", "#closeMenu", function() {
			$(".primary-nav").removeClass("active");
			$("#headerDefault").removeClass("main-wrap--overlay");
			$(this).remove();
		});
		// Mobile Menu Sub
		if ($(window).width() < 990) {
			var thisHeight = $(".overlay-menu .menu-item-has-children a").height();
			$(".overlay-menu .menu-item-has-children > ul").before(
				'<span class="arrow" style="height:' + thisHeight + 'px";></span>'
			);
			$(document).on("click", "span.arrow", function() {
				$(".overlay-menu .menu-item-has-children span.arrow").removeClass(
					"clicked"
				);
				$(".overlay-menu .menu-item-has-children>ul").slideUp();
				if (
					$(this)
						.next("ul")
						.is(":visible")
				) {
					console.log("sdf");
				} else {
					$(this).addClass("clicked");
					$(this)
						.next("ul")
						.slideDown();
				}
			});
		} else {
		}

		// Newsletter /
		$(".newsletter-button").click(function() {
			var winWidth = $(window).width();
			var itemWidth = $(this).width();
			var itemPosition = $(this).offset();
			if (
				$(this)
					.parent()
					.children(".newsletter-overlay")
					.is(":visible")
			) {
				$(this)
					.parent()
					.children(".newsletter-overlay")
					.slideUp("slow");
			} else {
				if (winWidth > 980) {
					var rightOrLeft = winWidth - itemPosition.left;
					$(".newsletter .newsletter-overlay").css("width", winWidth / 3);
					if (rightOrLeft < winWidth / 3 + 40) {
						$(this)
							.parent()
							.children(".newsletter-overlay")
							.css("right", "calc(100% - " + itemWidth + "px)");
					} else {
					}
				} else {
					var rightOrLeft = winWidth - itemPosition.left;
					$(".newsletter .newsletter-overlay").css("width", winWidth - 30);
					console.log("ko");
					$(this)
						.parent()
						.children(".newsletter-overlay")
						.css("left", 15 - itemPosition.left);
				}
				$(this)
					.parent()
					.children(".newsletter-overlay")
					.slideDown();
			}
		});
		$(document).click(function(e) {
			if (
				$(e.target).is(
					"div.newsletter-overlay,div.newsletter-overlay p, div.newsletter-overlay form, div.newsletter-overlay label ,div.newsletter-overlay input,div.newsletter-overlay .overlay-container ,  .newsletter-button button,.newsletter-form, .newsletter-input button , .newsletter-input button svg , .newsletter-button i, .newsletter-button span,.checkmark"
				)
			) {
			} else {
				$(".newsletter-overlay").slideUp("slow");
			}
		});
		// newsletter form submit
		$(document).on("click", ".newsletter-input button", function(e) {
			e.preventDefault();
			var checkBoxControl = $(this)
				.parent()
				.parent()
				.children(".checkbox")
				.children("input");
			var emailInput = $(this)
				.parent()
				.children("input");
			var emailInputControl = $(this)
				.parent()
				.children("input")
				.val();
			if (emailInputControl) {
				emailInput.removeClass("error");
				if (checkBoxControl.is(":checked")) {
					$(this)
						.parent()
						.parent()
						.submit();
					checkBoxControl.parent().removeClass("error");
				} else {
					checkBoxControl.parent().addClass("error");
				}
			} else {
				emailInput.addClass("error");
				console.log("mail");
			}
		});
		// Checkbox: remove error class when click on it
		$(document).on("click", "label.checkbox", function(e) {
			if (!$(this).is(":checked")) $(this).removeClass("error");
		});

		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var navbarHeight = $(".stories-sticky-header").outerHeight();

		$(window).scroll(function(event) {
			didScroll = true;
		});

		setInterval(function() {
			if (didScroll) {
				hasScrolled();
				didScroll = false;
			}
		}, 250);

		function hasScrolled() {
			var st = $(window).scrollTop();
			// Make scroll more than delta
			if (Math.abs(lastScrollTop - st) <= delta) return;
			// If scrolled down and past the navbar, add class .nav-up.
			if (st > lastScrollTop && st > navbarHeight) {
				$(".stories-sticky-header").removeClass("sticky-open");
			} else if (st <= navbarHeight) {
				// Scroll Down
				$(".stories-sticky-header").removeClass("sticky-open");
			} else {
				if (st + $(window).height() < $(document).height()) {
					$(".stories-sticky-header").addClass("stories-sticky-header");
					$(".stories-sticky-header").addClass("sticky-open");
				} else {
					$(".stories-sticky-header").removeClass("sticky-open");
				}
			}

			lastScrollTop = st;
		}

		/*
		if (winWidth > 980) {
			$(".newsletter .newsletter-overlay").css("width",winWidth/3);
		} else {
			$(".newsletter .newsletter-overlay").css("width",winWidth);
		}*/

		// Input Focus Effect
		$(document).on("focus", "input , textarea", function() {
			$(this)
				.parent()
				.children("label")
				.eq(0)
				.addClass("focused");
		});
		$(document).on("blur", "input, textarea", function() {
			if ($(this).val()) {
			} else {
				$(this)
					.parent()
					.children("label")
					.eq(0)
					.removeClass("focused");
			}
		});

		// Social Media ICons
		$(".social-media .more button").on("click", function() {
			$(this)
				.parent()
				.children("ul")
				.slideToggle();
		});

		// Post Widget /
		$(".post-widget-button").click(function() {
			var winWidth = $(window).width();
			var itemWidth = $(this).width();
			var itemPosition = $(this).offset();
			if (
				$(this)
					.parent()
					.children(".post-widget-overlay")
					.is(":visible")
			) {
				$(this)
					.parent()
					.children(".post-widget-overlay")
					.slideUp("slow");
			} else {
				if (winWidth > 980) {
					var rightOrLeft = winWidth - itemPosition.left;
					$(".post-widget .post-widget-overlay").css("width", winWidth / 3);
					if (rightOrLeft < winWidth / 3 + 40) {
						$(this)
							.parent()
							.children(".post-widget-overlay")
							.css("right", "calc(100% - " + itemWidth + "px)");
					} else {
					}
				} else {
					var rightOrLeft = winWidth - itemPosition.left;
					$(".post-widget .post-widget-overlay").css("width", winWidth - 30);
					console.log("ko");
					$(this)
						.parent()
						.children(".post-widget-overlay")
						.css("left", 15 - itemPosition.left);
				}
				$(this)
					.parent()
					.children(".post-widget-overlay")
					.slideDown()
					.focus();
			}
		});
		$(".post-widget-overlay").blur(function() {
			$(".post-widget-overlay").slideUp("slow");
		});

		function arrangeBlockSize () {
			var title = $(".stories_block_title").height();
			var nav = $(".jeg_subcat").height();
			if (title > nav ) {
				$(".jeg_subcat").css("height", title);
			} else {
				$(".stories_block_title").css("height", nav);
			}
			console.log(nav + title)
		}
		arrangeBlockSize ();
		$( window ).resize(
			function(){
				arrangeBlockSize ();
			}
		);

		/*
    // LazyLoad
    if($('.lazy-load-active').length){
        var stylemagLazyLoad = new LazyLoad({
            threshold: 300,
            elements_selector: ".lazyimage",
            throttle: 80,
            class_loading: 'image-loading',
            class_loaded: 'image-loaded',
            data_src: "original",
            show_while_loading: true
        });
    }
    // LazyLoad


    $('.scroll-to-top').on('click', function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    if($('.single-sticky-bar, .sticky-header, .scroll-to-top').length){
        var windowScrollTop;
        var scroll_next_point;
        var single_sticky;
        var lastScrollTop = 0;
        var taglastScrollTop = 0;
        var butScrollTop = 0;
        $(window).scroll(function(){
            windowScrollTop = $(window).scrollTop();
            if($('.sticky-header').length){
                var sctop = $(this).scrollTop();
                if(windowScrollTop > 300){
                    if(sctop > lastScrollTop){
                        $('.sticky-header').removeClass('sticky-header-open');
                    }else{
                        $('.sticky-header').addClass('sticky-header-open');
                    }
                    lastScrollTop = sctop;
                }else{
                    $('.sticky-header').removeClass('sticky-header-open');
                }
            }

            if($('.scroll-to-top').length){
                var buttop = $(this).scrollTop();
                if(windowScrollTop > 300){
                    if(buttop > butScrollTop){
                        $('.scroll-to-top').css('bottom', '-45px');
                    }else{
                        $('.scroll-to-top').css('bottom', '45px');
                    }
                    butScrollTop = buttop;
                }else{
                    $('.scroll-to-top').css('bottom', '-45px');
                }
            }

            if($('.single-sticky-bar').length){
                if(windowScrollTop > 300){
                    $('.single-sticky-bar').addClass('single-stick-now');
                }else{
                    $('.single-sticky-bar').removeClass('single-stick-now');
                }
            }
        });
    }



    

    $('.fancy-sidebar-content').slimScroll({
        height: 'auto',
        color: '#4c4c4c',
        wheelStep: 10
    });
*/
	});

	$(window).load(function() {
		if (!isMobile.any()) {
		}
	});
})(jQuery);
