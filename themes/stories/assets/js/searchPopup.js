/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2016, Codrops
 * http://www.codrops.com
 */
(function($) {

	'use strict';

	var mainContainer = $('header'),
		openCtrl = $('.search-button'),
		closeCtrl = $('.btn-search-close'),
		searchContainer = $('.search-overlay'),
		inputSearch = $('.custom-search-form .search-input');
 
	function init() {
		initEvents();	
	}

	function initEvents() {
		openCtrl.on("click",function(){ 
			mainContainer.addClass('main-wrap--overlay');
			$(this).parent().parent().find(".btn-search-close").addClass('btn-show');  
			$(this).parent().parent().children(".search-overlay").addClass('search--open'); 
			setTimeout(function() { 
			}, 500);   
			$(this).parent().parent().find('.search-input').each(function(){
				this.focus(); 
			});
			$('body').css('overflow', 'hidden');
		})		
		closeCtrl.on("click",function(){ 
			mainContainer.removeClass('main-wrap--overlay');
			$(this).removeClass('btn-show');
			searchContainer.removeClass('search--open');   
			$(this).parent().find(".search-input").each(function(){
				this.blur();
				this.value = '';
			}); 
			$('body').css('overflow', 'auto');
		})	
		$(window).on("keyup",function(e){
			if( e.keyCode == 27 ) { 
				mainContainer.removeClass('main-wrap--overlay');
				closeCtrl.removeClass('btn-show');
				searchContainer.removeClass('search--open');  
				inputSearch.each(function(){
					this.blur();
					this.value = '';
				});  
			}
			$('body').css('overflow', 'auto');
		})	 
	} 
	init(); 

	$(".search-suggestion a").hover(function(){ 
			$(".search-suggestion a").not(this).animate({"opacity":".6"}, 400); 
		}, function(){
			$(".search-suggestion a").animate({"opacity":"1"}, 100)
	});


	/// 
	
	var header = $('header'),
		openIcon = $('.toggle-menu'), 
		menuContainer = openIcon.parent().children(".overlay-menu"),
		closeIcon = $(".close-overlay-menu"); 

	function overlayMenuEvents() {
		openIcon.on("click",function(){
			openMenuOverlay(); 
			$('body').css('overflow', 'hidden');
		});	
		closeIcon.on("click",function(){
			closeMenuOverlay(); 
			$('body').css('overflow', 'auto');
		});
		$(window).on("keyup",function(e){
			if( e.keyCode == 27 ) {
				closeMenuOverlay();
				$('body').css('overflow', 'auto');
			}
		});	 	 
	}

	function openMenuOverlay() {
		header.addClass('main-wrap--overlay'); 
		menuContainer.addClass('open');  
	}
	function closeMenuOverlay() {
		header.removeClass('main-wrap--overlay'); 
		menuContainer.removeClass('open'); 
	}
	overlayMenuEvents();  




})(jQuery);