<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CoverNews
 */

?>


</div>

<?php // do_action('covernews_action_full_width_upper_footer_section'); ?>

<?php wp_footer(); ?>

</body>
</html>
