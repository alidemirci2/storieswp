<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CoverNews
 */

?>


    <div class="content-entry<?php if(is_sticky()){ echo " sticky-post"; } ?>"><!-- Blog Entry Start -->
            <article <?php post_class(); ?> class='clearfix' id="post-<?php the_ID(); ?>">
                    <div class="row"><!-- Row Start -->

                        <?php 
                        // improve: Left ve Right sidebara gore degisecek.
                        $thumbnail =  'stories-thumbnail';
                        ?>
                        
                        <?php if(has_post_thumbnail()){ $column = 6;} else { $column = '12'; } ?>
                        <?php if(has_post_thumbnail()){  ?>
                        <div class="col-lg-<?php echo esc_attr($column); ?> content-list-image">

                            <?php the_post_thumbnail( $thumbnail ); ?>

                        </div>
                        
                        <?php } ?>
                        <div class="col-lg-<?php echo esc_attr($column); ?> content-list-elements<?php if(empty($image)){ echo " no-index"; } ?>">
                            <div class="jeg_post_category">
                                <span class="jeg_post_cat"> 
                                    <span><?php the_category(', '); ?></span>
                                </span>
                            </div>

                            <h2 property="headline" class="jeg_post_title">
                                <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                            </h2>

                            <?php 			
                            $author      = $post->post_author;
                            $author_url  = get_author_posts_url( $author );
                            $author_name = get_the_author_meta( 'display_name', $author );
                            
                            
                            $comments_number = get_comments_number( esc_attr(get_the_ID()) );

                            ?>
                            <div class="jeg_post_meta post_meta_4 vline ">
                                <div class="jeg_meta_author"><a href="#"><?php echo esc_attr($author_name); ?></a></div>
                                <div class="jeg_meta_date"><a href="<?php echo esc_url($author_url); ?>"><?php echo esc_attr(get_the_date()); ?></a></div>
                                <!-- improve: comment iconlu meta olcak -->
                                <?php if($comments_number >0 ){ ?>
                                    <div class="jeg_meta_comment">
                                        <i class="icon icon-comment-alt-regular"></i> <?php echo esc_attr($comments_number); ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="jeg_post_excerpt">
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="jeg_readmore"><?php esc_html_e('Read more','Stories'); ?> <?php echo file_get_contents(THEME_URI  . '/assets/img/arrow-thing-right.svg'); ?> </a>
                            </div>
                           

                        </div>


                    </div><!-- Row Finish -->

            </article>
    </div><!-- Blog Entry Finish -->



