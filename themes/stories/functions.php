<?php
/**
 * Stories functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Stories
 */

if (!function_exists('stories_setup')):

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */

    function stories_setup() {
    /*
    * Translations can be filed in the /languages/ directory.
    * We Suggest Loco Translate Plugin to translate theme.
    * Also you can use poedit software to translate theme.
    */
	load_theme_textdomain('Stories', get_template_directory().'/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
        * Let WordPress manage the document title.
        * By adding theme support, we declare that this theme does not use a
        * hard-coded <title> tag in the document head, and expect WordPress to
        * provide it for us.
        */
    add_theme_support( 'title-tag' );

    /*
    * Enable support for Post Thumbnails on posts and pages.
    *
    * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
    */
    add_theme_support( 'post-thumbnails' );

    // Adding Image Size for Images
        add_image_size('stories-slider-large', 1020, 741, true);
        add_image_size('stories-thumbnail', 645, 350, true); // improve: degiscek, deneme icin eklendi

    // Add support for editor styles.
    add_theme_support( 'editor-styles' );
    add_editor_style( 'assets/css/style-editor.css' );

    /*
    * Enable support for Post Formats.
    *
    * See: https://codex.wordpress.org/Post_Formats
    */
    add_theme_support( 'post-formats', array(
        'audio',
        'gallery',
        'image',
        'link',
        'quote',
        'video',
    ) );

 

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary Menu', 'Stories' ),
        'elementor' => esc_html__( 'Elementor Mega Menu', 'Stories' ),
    ) );


    /*
    * Switch default core markup for search form, comment form, and comments
    * to output valid HTML5.
    */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );


    // Add theme support for selective refresh for widgets.
    // improve: arastir
   // add_theme_support( 'customize-selective-refresh-widgets' );

	// Set up the WordPress core custom background feature.
	add_theme_support('custom-background', apply_filters('covernews_custom_background_args', array(
        'default-color' => 'f5f5f5',
        'default-image' => '',
    )));

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support('custom-logo', array(
			'flex-width'  => true,
			'flex-height' => true,
		));


}
endif;
add_action('after_setup_theme', 'stories_setup');



/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function stories_content_width() {
	$GLOBALS['content_width'] = apply_filters('stories_content_width', 640);
}
add_action('after_setup_theme', 'stories_content_width', 0);




/**
 * Enqueue scripts and styles.
 */
function stories_scripts() {

	$min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG?'':'.min';
	wp_enqueue_style('bootstrap', esc_url(get_template_directory_uri()).'/assets/css/bootstrap-grid'.$min.'.css');


    /* WooCommerce Styles */
    if ( class_exists( 'WooCommerce' ) ) {
        wp_enqueue_style('covernews-woocommerce-style', esc_url(get_template_directory_uri()) . '/assets/woocommerce.css');
    }
    
    wp_enqueue_style('stories-style', get_stylesheet_uri());
    wp_enqueue_style('stories-editor-styles', esc_url(get_template_directory_uri()) . '/assets/css/style-editor.css');
 
    wp_add_inline_style( 'stories-style', theme_get_customizer_css() );

	/* Fancy Sidebar */
	wp_enqueue_script( 'stories-fancy-sidebar', esc_url( get_template_directory_uri()) . '/assets/js/stories-fancy-sidebar.js', array('jquery'), null, true);
	

	/* Sticky Sidebar for Header */
	wp_enqueue_script( 'theia-sticky-sidebar', esc_url(get_template_directory_uri()) . '/assets/js/theia-sticky-sidebar.js', array('jquery'), null, true);
		
	/* Superfish */
	wp_enqueue_script( 'superfish', esc_url(get_template_directory_uri()) . '/assets/js/superfish.js', array('jquery'), null, true);
		

    /* Search Popup */
	wp_enqueue_script( 'search-popup', esc_url(get_template_directory_uri()) . '/assets/js/searchPopup.js', array('jquery'), null, true);

    /* Skip Link for Accessibility */
	wp_enqueue_script( 'stories-skip-link-focus-fix', esc_url(get_template_directory_uri()) . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

	/* Theme Main Scripts File */
    wp_enqueue_script('stories-scripts', esc_url(get_template_directory_uri()).'/assets/js/stories-scripts.js', array('jquery'), '', 1);
	
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
}
add_action('wp_enqueue_scripts', 'stories_scripts');


// Register Sidebars - Widgets

if (!function_exists('stories_register_sidebars')){
    function stories_register_sidebars() {
        if (function_exists('register_sidebar')) {

            
            $widget_style = epic_get_option( 'widget_title_styles' );

            register_sidebar(array(
                'name' => esc_html__('Main Sidebar', 'Stories'),
                'id' => 'sidebar-1',
                'description' => esc_html__('Add widgets for main sidebar.', 'Stories'),
                'before_widget' => '<div id="%1$s" class="widget stories-widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => "<div class=\"stories_block_heading {$widget_style}  jeg_subcat_right\"><h3 class=\"stories_block_title\"><span>",
                'after_title' => '</span></h3></div>',
            ));   

            register_sidebar(array(
                'name' => esc_html__('Single Sidebar', 'Stories'),
                'id' => 'single-sidebar',
                'description' => esc_html__('Add widgets for single sidebar.', 'Stories'),
                'before_widget' => '<div id="%1$s" class="widget stories-widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h2 class="widget-title widget-title-1"><span>',
                'after_title' => '</span></h2>',
            ));

        }
    }
    add_action( 'widgets_init', 'stories_register_sidebars');
}


        function get_column_size($page){
            
            if( epic_get_option( $page . '_sidebar_option' ) ){
                $layout_style = epic_get_option( $page . "_layout" );
            }else{
                $layout_style = epic_get_option( "site_layout" );
            }

            
    
            switch ($layout_style) {
                case 'right-sidebar':
                case 'left-sidebar':
                    $column = 'col-lg-9';
                    break;
                case 'both-sidebar':
                case '2-right-sidebar':
                case '2-left-sidebar':
                    $column = 'col-lg-6';
                    break;
                case 'no-sidebar':
                    $column = 'col-lg-12';
                    break;
            }

            
            $sidebar_size = epic_get_option( "sidebar_size" );
            if($sidebar_size == 1 ){
                return column_with_sidebar($column);
            }else{
                return $column;
            }


        }


        function column_with_sidebar($column){

                switch ($column) {
                    case 'col-lg-9':
                        $column = 'col-lg-8';
                        break;
                    case 'col-lg-6':
                        $column = 'col-lg-4';
                        break;
                    case 'col-lg-12':
                        $column = 'col-lg-12';
                        break;
                }

                return $column;

            }






// Register Author Contact Methods

//Author Social Media
function stories_author_contactmethods($contactmethods){
    $contactmethods['behance'] = esc_html__( 'Behance URL', 'Stories' );
    $contactmethods['dribbble'] = esc_html__( 'Dribbble URL', 'Stories' );
    $contactmethods['facebook'] = esc_html__( 'Facebook URL', 'Stories' );
    $contactmethods['facebook-messenger'] = esc_html__( 'Facebook Messenger URL', 'Stories' );
    $contactmethods['foursquare'] = esc_html__( 'Foursquare URL', 'Stories' );
    $contactmethods['instagram'] = esc_html__( 'Instagram URL', 'Stories' );
    $contactmethods['linkedin'] = esc_html__( 'Linkedin URL', 'Stories' );
    $contactmethods['medium'] = esc_html__( 'Medium URL', 'Stories' );
    $contactmethods['patreon'] = esc_html__( 'Patreon URL', 'Stories' );
    $contactmethods['periscope'] = esc_html__( 'Periscope URL', 'Stories' );
    $contactmethods['pinterest'] = esc_html__( 'Pinterest URL', 'Stories' );
    $contactmethods['skype'] = esc_html__( 'Skype URL', 'Stories' );
    $contactmethods['slack'] = esc_html__( 'Slack URL', 'Stories' );
    $contactmethods['tumblr'] = esc_html__( 'Tumblr URL', 'Stories' );
    $contactmethods['twitch'] = esc_html__( 'Twitch URL', 'Stories' );
    $contactmethods['twitter'] = esc_html__( 'Twitter URL', 'Stories' );
    $contactmethods['vimeo'] = esc_html__( 'Vimeo URL', 'Stories' );
    $contactmethods['whatsapp'] = esc_html__( 'Whatsapp URL', 'Stories' );
    $contactmethods['youtube'] = esc_html__( 'Youtube URL', 'Stories' );

    return $contactmethods;
}
add_filter('user_contactmethods', 'stories_author_contactmethods', 10, 1);


function stories_excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt);
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}


function is_elementor(){
  global $post;
  return \Elementor\Plugin::$instance->db->is_built_with_elementor($post->ID);
}

// Stories Extras

require get_template_directory().'/stories-extras/stories-extras.php';


// Meta Box

require get_template_directory().'/inc/metabox.php';


/* REVIEW THESE LATER */

/**
 * Custom template tags for this theme.
 */
require get_template_directory().'/inc/template-tags.php';

/**
 * Custom CSS
 */
require get_template_directory().'/inc/custom-css.php';
require get_template_directory().'/inc/admin-custom-css.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory().'/inc/template-images.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory().'/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory().'/inc/customizer/customizer.php';


/**
 * Customizer additions.
 */
require get_template_directory().'/inc/customizer/stories-customizer.php';


/**
 * Implement the Custom Header feature.
 */
require get_template_directory().'/inc/custom-header.php';

/**
 * Customizer additions.
 */
require get_template_directory().'/inc/init.php';

/**
 * Customizer additions.
*/
require get_template_directory().'/inc/ocdi.php';


/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory().'/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
    require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Descriptions on Header Menu
 * @author AF themes
 * @param string $item_output, HTML outputp for the menu item
 * @param object $item, menu item object
 * @param int $depth, depth in menu structure
 * @param object $args, arguments passed to wp_nav_menu()
 * @return string $item_output
 */
function covernews_header_menu_desc( $item_output, $item, $depth, $args ) {

    if( 'aft-primary-nav' == $args->theme_location  && $item->description )
        $item_output = str_replace( '</a>', '<span class="menu-description">' . $item->description . '</span></a>', $item_output );

    return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'covernews_header_menu_desc', 10, 4 );





/**
 * Demo export/import
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package CoverNews
 */
if (!function_exists('covernews_ocdi_files')) :
    /**
     * OCDI files.
     *
     * @since 1.0.0
     *
     * @return array Files.
     */
    function covernews_ocdi_files() {

        return apply_filters( 'aft_demo_import_files', array(
            array(

                'import_file_name'             => esc_html__( 'CoverNews Default', 'Stories' ),
                'local_import_file'            => trailingslashit( get_template_directory() ) . 'demo-content/default/covernews.xml',
                'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demo-content/default/covernews.wie',
                'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demo-content/default/covernews.dat',
                'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . 'demo-content/assets/covernews.jpg',
                'preview_url'                  => 'https://demo.afthemes.com/covernews/',
            ),
            array(

                'import_file_name'             => esc_html__( 'CoverNews Sport', 'Stories' ),
                'local_import_file'            => trailingslashit( get_template_directory() ) . 'demo-content/sport/covernews.xml',
                'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demo-content/sport/covernews.wie',
                'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demo-content/sport/covernews.dat',
                'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . 'demo-content/assets/covernews-sport.jpg',
                'preview_url'                  => 'https://demo.afthemes.com/covernews/sport',
            ),
            array(

                'import_file_name'             => esc_html__( 'CoverNews Fashion', 'Stories' ),
                'local_import_file'            => trailingslashit( get_template_directory() ) . 'demo-content/fashion/covernews.xml',
                'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demo-content/fashion/covernews.wie',
                'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demo-content/fashion/covernews.dat',
                'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . 'demo-content/assets/covernews-fashion.jpg',
                'preview_url'                  => 'https://demo.afthemes.com/covernews/fashion',
            ),
        ));
    }
endif;
add_filter( 'pt-ocdi/import_files', 'covernews_ocdi_files');




/**
 * function for google fonts
 */
if (!function_exists('covernews_fonts_url')):

    /**
     * Return fonts URL.
     *
     * @since 1.0.0
     * @return string Fonts URL.
     */
    function covernews_fonts_url() {

        $fonts_url = '';
        $fonts = array();
        $subsets = 'latin,latin-ext';

        /* translators: If there are characters in your language that are not supported by Oswald, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Source Sans Pro font: on or off', 'Stories')) {
            $fonts[] = 'Source+Sans+Pro:400,400i,700,700i';
        }

        /* translators: If there are characters in your language that are not supported by Lato, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Lato font: on or off', 'Stories')) {
            $fonts[] = 'Lato:400,300,400italic,900,700';
        }

        /* translators: If there are characters in your language that are not supported by Open Sans, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Open Sans font: on or off', 'Stories')) {
            $fonts[] = 'Open Sans:400,300,400italic,900,700';
        }

        if ($fonts) {
            $fonts_url = add_query_arg(array(
                'family' => urldecode(implode('|', $fonts)),
                'subset' => urldecode($subsets),
            ), 'https://fonts.googleapis.com/css');
        }
        return $fonts_url;
    }
endif;



require get_template_directory().'/lib/elementor/elementor-modules.php';
if( function_exists( 'elementor_load_plugin_textdomain' ) ) { 

    require get_template_directory().'/lib/elementor/plugin.php';

}


/**
 * custom search form for default search popup
 */

 // burdaki default "search" degerininin translate olabilmesi lazim ve escaping olcak
// ajax wp entegre et
function stories_custom_search_form($addSubmit = 1 ) {
    $form = '<form role="search" method="get" class="search-form" action="' . home_url( '/' ) . '" > 
        <label>
            <input class="search-input" type="search" data-swplive="true" placeholder="'. esc_html('Search...','Stories') .'" name="search" class="search-input" name="search"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" /> ';
        if ($addSubmit == 1) {
           $form .= 'sdf'; 
        }
    $form .= '</label> 
    </form>';
 
    return $form;
}
add_filter( 'get_search_form', 'stories_custom_search_form' );  
 



 
 function custom_post_state() {
    global $post;
    global $wpdb;

	$meta_key = 'set_main_header';
		$active_header = $wpdb->get_var( $wpdb->prepare( 
			"
				SELECT meta_id
				FROM $wpdb->postmeta 
				WHERE meta_key = %s
			", 
			$meta_key
		) );
            
    // $show_custom_state = get_post_meta( 'elementor-hf', $active_header, 'set_main_header' );
    print_r(get_post_meta('elementor-hf', 2886));
    echo $show_custom_state;
    if ( $show_custom_state ) {
        $states[] = __( 'Custom State', 'Stories' );
    }
    return $states;
  }	  
  add_filter( 'display_post_states', 'custom_post_state' );
  



		
		
	



/**
 * Burak bunu istedigin yere tasiyabilirsin.
 */
add_action('elementor/element/section/section_layout/after_section_start', function( $section , $args ) {
	$section->add_control(
		'ti-parallax' ,
		[
            'label' => __( 'Sticky Header', 'Stories' ),				
            'description' => __( 'To see sticky header, scroll down then scroll to top. You should only select one column as a Sticky Header', 'Stories' ),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'default' => '',
			'prefix_class' => 'stories-sticky-header ',
			'label_on' => 'On',
			'label_off' => 'Off',
		]
	);
}, 10, 2 ); 


add_action('init','random_post');
function random_post() {
        global $wp;
        $wp->add_query_var('random');
        add_rewrite_rule('random/?$', 'index.php?random=1', 'top');
}

add_action('template_redirect','random_template');
function random_template() {
        if (get_query_var('random') == 1) {
                $posts = get_posts('post_type=post&orderby=rand&numberposts=1');
                foreach($posts as $post) {
                        $link = get_permalink($post);
                }
                wp_redirect($link,307);
                exit;
        }
} 

register_sidebar(array(
    'name' => esc_html__('Post Widget', 'Stories'),
    'id' => 'post-widget',
    'description' => esc_html__('Add Post Widget like Trending Posts', 'Stories')
));
  


function megamenu_elementor_stories($themes) {
    $themes["stories_1556826022"] = array(
        'title' => 'Stories Theme', 
        'container_background_from' => 'rgba(0, 0, 0, 0)',
        'container_background_to' => 'rgba(0, 0, 0, 0)',
        'container_padding_top' => ' 0px',
        'container_padding_bottom' => '0px',
        'menu_item_align' => 'center',
        'menu_item_background_hover_from' => 'rgba(0, 0, 0, 0)',
        'menu_item_background_hover_to' => 'rgba(0, 0, 0, 0)',
        'menu_item_link_color' => 'rgb(34, 34, 34)',
        'menu_item_link_weight' => 'bold',
        'menu_item_link_color_hover' => 'rgb(34, 34, 34)',
        'menu_item_link_weight_hover' => 'bold',
        'menu_item_border_color' => 'rgb(34, 34, 34)',
        'menu_item_border_color_hover' => 'rgb(34, 34, 34)',
        'panel_background_from' => 'rgb(244, 0, 0)',
        'panel_background_to' => 'rgb(244, 0, 0)',
        'panel_header_border_color' => '#555',
        'panel_font_size' => '14px',
        'panel_font_color' => '#666',
        'panel_font_family' => 'inherit',
        'panel_second_level_font_color' => '#555',
        'panel_second_level_font_color_hover' => '#555',
        'panel_second_level_text_transform' => 'uppercase',
        'panel_second_level_font' => 'inherit',
        'panel_second_level_font_size' => '16px',
        'panel_second_level_font_weight' => 'bold',
        'panel_second_level_font_weight_hover' => 'bold',
        'panel_second_level_text_decoration' => 'none',
        'panel_second_level_text_decoration_hover' => 'none',
        'panel_second_level_border_color' => 'rgb(51, 51, 51)',
        'panel_second_level_border_left' => '1px',
        'panel_second_level_border_right' => '1px',
        'panel_second_level_border_bottom' => '1px',
        'panel_third_level_font_color' => '#666',
        'panel_third_level_font_color_hover' => '#666',
        'panel_third_level_font' => 'inherit',
        'panel_third_level_font_size' => '14px',
        'flyout_border_color' => 'rgb(51, 51, 51)',
        'flyout_border_left' => '1px',
        'flyout_border_right' => '1px',
        'flyout_border_top' => '1px',
        'flyout_border_bottom' => '1px',
        'flyout_menu_background_from' => '#fff',
        'flyout_menu_background_to' => '#fff',
        'flyout_link_weight' => 'bold',
        'flyout_link_weight_hover' => 'bold',
        'flyout_background_from' => '#fff',
        'flyout_background_to' => '#fff',
        'flyout_background_hover_from' => '#fff',
        'flyout_background_hover_to' => '#fff',
        'flyout_link_size' => '14px',
        'flyout_link_color' => '#333',
        'flyout_link_color_hover' => '#333',
        'flyout_link_family' => 'inherit',
        'shadow' => 'on',
        'toggle_background_from' => '#222',
        'toggle_background_to' => '#222',
        'mobile_background_from' => '#222',
        'mobile_background_to' => '#222',
        'mobile_menu_item_link_font_size' => '14px',
        'mobile_menu_item_link_color' => '#ffffff',
        'mobile_menu_item_link_text_align' => 'left',
        'mobile_menu_item_link_color_hover' => '#ffffff',
        'mobile_menu_item_background_hover_from' => '#333',
        'mobile_menu_item_background_hover_to' => '#333',
   
    );
   
    return $themes;
}  
add_filter("megamenu_themes", "megamenu_elementor_stories"); 
  

  /**************************************************************************************************/
/* Custom Comments */
/**************************************************************************************************/

function stories_comment( $comment, $args, $depth ) {
       $GLOBALS['comment'] = $comment; ?>                    
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
            <div id="comment-<?php comment_ID(); ?>" class="clearfix"> 
                <div class="user-comment-box clearfix">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                                $check_comment = get_avatar($comment);
                                if($check_comment){
                                    $left_classes = "comment-left-image";
                                    $right_classes = "comment-right-text comment-title";
                                }else{
                                    $right_classes = "comment-title";
                                }
                            ?>
                            <div class="comment-info-top clearfix<?php if($check_comment){echo " with-userimage";} ?>">
                                <?php if($check_comment){ ?>
                                <div class="<?php echo esc_attr($left_classes); ?> avatar-image">
                                    <?php echo get_avatar( $comment, 60, null, null, array('class' => array('img-responsive'))); ?>
                                </div>
                                <?php } ?>
                                <div class="<?php echo esc_attr($right_classes); ?> clearfix">
                                         <div class="author author-title">
                                            <h5><?php printf( esc_html__( '%s', 'Stories'), get_comment_author_link() ) ?></h5>
                                         </div>
                                         <a class="element-date date" title="<?php echo esc_attr($post_date = get_comment_date('F j, Y g:i')); ?>" href="<?php the_permalink(); ?>"><?php echo esc_attr($post_date = get_comment_date()); ?></a>
                                </div>
                            </div>
                            <div class="comment-area-box<?php if($check_comment){echo " with-userimage";} ?>">
                                <div class="comment-text<?php if(is_user_logged_in()){ echo " cuser-in"; } ?>">
                                    <div class="comment-content"><?php comment_text(); ?></div>
                                    <div class="comment-tools">
                                        <h6><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?><?php edit_comment_link( esc_html__( ' (Edit)', 'Stories'),'  ','' ) ?></h6>
                                     </div>
                                </div>
                                 <?php if ( $comment->comment_approved == '0' ) : ?>
                                 <em><?php esc_html__( 'Your comment is awaiting moderation.', 'Stories' ) ?></em>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
<?php }