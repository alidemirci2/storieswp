<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package CoverNews
 */



    $dropcap_class = "";

    $dropcap = epic_get_option('single_dropcap');
    $post_id = get_the_id();

    if( $dropcap == 1 ){
        $dropcap_class = 'dropcap-on';
    }



get_header(); ?>


    <div class="stories_single_viewport">
        <div class="blog-post-wrapper">
            <div class="container">
                <div class="main-row row">

                        <?php get_sidebar(); ?>

                        <div id="primary" class="content-area main-column <?php echo esc_attr(get_column_size('single')); ?>">
                            <main id="main" class="site-main default-single">

                                <?php
                                while (have_posts()) : the_post(); ?>
                                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                            <header class="pos-center">
                                                <div class="jeg_meta_category">
                                                        <h6>
                                                        <?php the_category(', '); ?>
                                                        </h6>
                                                    </span>
                                                </div>
                                                <div class="stories_title_wrapper">
                                                    <h1 class="stories_post_title"><?php the_title(); ?></h1>
                                                </div>

                                                <?php 			
                                                $author      = $post->post_author;
                                                $author_url  = get_author_posts_url( $author );
                                                $author_name = get_the_author_meta( 'display_name', $author );
                                            
                                                $comments_number = get_comments_number( esc_attr(get_the_ID()) );
												$meta_style = epic_get_option('meta_seperator_type');
												?>
												<!-- burak: post_meta_4 manuel eklendi -->
                                                <div class="jeg_post_meta post_meta_4 vline ">
                                                    <div class="jeg_meta_author"><a href="#"><?php echo esc_attr($author_name); ?></a></div>
                                                    <div class="jeg_meta_date"><a href="<?php echo esc_url($author_url); ?>"><?php echo esc_attr(get_the_date()); ?></a></div>
                                                    <!-- improve: comment iconlu meta olcak -->
                                                    <?php if($comments_number >0 ){ ?>
                                                        <div class="jeg_meta_comment">
                                                            <i class="icon icon-comment-alt-regular"></i> <?php echo esc_attr($comments_number); ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </header>

                                        <div class="entry-content-wrap">
                                            <?php if(has_post_thumbnail()){ ?>
                                                <div class="single-featured-image pos-center">
                                                    <?php the_post_thumbnail(); ?>
                                                </div>
                                            <?php } ?>
                                            <div class="entry-content <?php echo esc_attr($dropcap_class); ?> clearfix">

                                                <?php the_content(); ?>
                                              
                                                <?php wp_link_pages(array(
                                                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'Stories'),
                                                    'after' => '</div>',
                                                ));
                                                ?>

                                            </div><!-- .entry-content -->
                                            <div class="entry-botttom">
                                                  <?php if(has_tag()){ ?>
                                                    <div class="post-tags">
                                                        <span><?php esc_html_e('Tags'); ?></span><?php the_tags('#', '#', ''); ?>
                                                    </div>
                                                <?php } ?>
                                                <div class="single stories-seperator">
			
                                                    <div class="seperator-container seperator-three-dot normal d-flex justify-content-center">
                                                        
                                                        <span class="three-dot"></span>
                                                        <span class="three-dot"></span>
                                                        <span class="three-dot"></span>
                                            
                                                    </div>   
                                                </div>
											</div>
											


                                <?php 

                                // Review
                                do_action('stories_review', $post_id); 
                                
                                ?>






              
                                        </div>
                                        <?php
                                        $show_related_posts = esc_attr(covernews_get_option('single_show_related_posts'));
                                        if ($show_related_posts):
                                            if ('post' === get_post_type()) :
                                            endif;
                                        endif;
                                        ?>

                                    </article>
                                <?php

                                endwhile; // End of the loop.
                                ?>
                           
                           <div class="post-nav-with-author">





<?php 

        
        $title_prev = "";
		$prev_post        = get_previous_post();




        $title_prev = "
        <div class=\"prev_text_title\">
            <a href=\"" . esc_url( get_permalink( $prev_post->ID ) ) . "\" class=\"post prev-post\">
                <h5 class=\"post-title\">" . wp_kses_post( $prev_post->post_title ) . "</h5>						
            </a>			
        </div>";

        						

        if($prev_post){
		echo
			"<div class='jeg_custom_next_wrapper pos-center epic_next_container'>
				<div class=\"jeg_prev_next_post jeg_prev_post pos-center  box-shadow\">
					<div class=\"prev_text_icon\"> ". file_get_contents(THEME_URI  . '/assets/svg/arrow-thing-left.svg.php')  ."</div>
                    {$title_prev}
					<h6 class=\"caption\">" . esc_html__( 'PREVIOUS', 'Stories' ) . "</h6>
				</div>                
            </div>";
        }

?>






<div class="single-small-container">



<?php 


			$author_socials = $other_posts = '';
			
			if(get_the_author_meta( 'url', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'url', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">" . file_get_contents(THEME_URI  . '/assets/img/globe.svg') . "</a>";
			}

			$author_socials .= "<a href='" . get_the_author_meta( 'rss', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">" . file_get_contents(THEME_URI  . '/assets/img/rss.svg') . "</a>";


			if(get_the_author_meta( 'behance', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'behance', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/behance.svg') ."</a>";
			}

			if(get_the_author_meta( 'dribbble', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'dribbble', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/dribbble.svg') ."</a>";
			}

			if(get_the_author_meta( 'facebook', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'facebook', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/facebook.svg') ."</a>";
			}

			if(get_the_author_meta( 'facebook-messenger', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'facebook-messenger', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/facebook-messenger.svg') ."</a>";
			}

			if(get_the_author_meta( 'foursquare', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'foursquare', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/foursquare.svg') ."</a>";
			}

			if(get_the_author_meta( 'instagram', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'instagram', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/instagram.svg') ."</a>";
			}

			if(get_the_author_meta( 'linkedin', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'linkedin', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/linkedin.svg') ."</a>";
			}

			if(get_the_author_meta( 'medium', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'medium', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/medium.svg') ."</a>";
			}

			if(get_the_author_meta( 'patreon', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'patreon', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/patreon.svg') ."</a>";
			}

			if(get_the_author_meta( 'periscope', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'periscope', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/periscope.svg') ."</a>";
			}

			if(get_the_author_meta( 'pinterest', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'pinterest', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/pinterest.svg') ."</a>";
			}

			if(get_the_author_meta( 'skype', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'skype', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/skype.svg') ."</a>";
			}

			if(get_the_author_meta( 'slack', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'slack', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/slack.svg') ."</a>";
			}

			if(get_the_author_meta( 'tumblr', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'tumblr', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/tumblr.svg') ."</a>";
			}

			if(get_the_author_meta( 'twitch', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'twitch', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/twitch.svg') ."</a>";
			}

			if(get_the_author_meta( 'twitter', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'twitter', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/twitter.svg') ."</a>";
			}

			if(get_the_author_meta( 'vimeo', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'vimeo', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/vimeo.svg') ."</a>";
			}

			if(get_the_author_meta( 'whatsapp', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'whatsapp', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/whatsapp.svg') ."</a>";
			}

			if(get_the_author_meta( 'youtube', $post->post_author )){
			$author_socials .= "<a href='" . get_the_author_meta( 'youtube', $post->post_author ) . "' rel=\"nofollow\" class=\"url\">". file_get_contents(THEME_URI  . '/assets/img/brand/youtube.svg') ."</a>";
			}


			// improve: author'un son postlarini cekelim

			$other_posts = "
				<div class=\"jeg_authorbox_bottom\"><h6 class='authorbox_segment_title'>". 
					sprintf(esc_html__( '%s\'s Posts ','Stories' ), get_the_author_meta( 'display_name', $post->post_author )) . "</h6>
					<div class=\"author_other_post title_font\">
						<ul>
							<li><a href=\"#\">Already Master Camp on the Red monetize</a></li>
							<li><a href=\"#\">But according to the creator of one parents go them you enginner</a></li>
							<li><a href=\"#\">Of the world’s most</a></li>
							<li><a href=\"#\">Serdar Teacher Red monetize</a></li>
						</ul>
					</div>
				</div>";



		echo 
			"<div class='jeg_custom_author_wrapper epic_author_box_container'>
				<div class=\"jeg_authorbox box-shadow\">
					<h6 class='authorbox_segment_title'>". esc_html('AUTHOR','Stories') ."</h6>
					<div class=\"jeg_authorbox_top clearfix\">
						<div class=\"jeg_author_image\">
							" . get_avatar( get_the_author_meta( 'ID', $post->post_author ), 67, null, get_the_author_meta( 'display_name', $post->post_author ) ) . "
							<div class=\"jeg_author_socials\">
								{$author_socials}
							</div>
							</div>
						<div class=\"jeg_author_content\">
							<h5 class=\"jeg_author_name\">
								" . epic_the_author_link( $post->post_author, false ) . "
							</h5>
							<p class=\"jeg_author_desc\">
								" . get_the_author_meta( 'description', $post->post_author ) ." 
								<span class='more_link'><strong>". esc_html('more','Stories') . "</strong></span>
							</p>
						</div>
					</div>
					{$other_posts}
				</div>
            </div>";
            


?>


</div>








<?php 

        $title = '';
		$next_post        = get_next_post();
        
        $title = "
        <div class=\"prev_text_title\">
            <a href=\"" . esc_url( get_permalink( $next_post->ID ) ) . "\" class=\"post next-post\">
                <h5 class=\"post-title\">" . wp_kses_post( $next_post->post_title ) . "</h5>						
            </a>			
        </div>";
        

        ?>
        <div class='jeg_custom_next_wrapper pos-center epic_next_container'>
            <?php 

        if($next_post ){
		echo
			"
				<div class=\"jeg_prev_next_post jeg_next_post pos-center  box-shadow\">
					<div class=\"prev_text_icon\"> ". file_get_contents(THEME_URI  . '/assets/svg/arrow-thing-right.svg.php')  ."</div>
					{$title}			
					<h6 class=\"caption\">" . esc_html__( 'Next', 'Stories' ) . "</h6>
				</div>                
			";
        }
        
        ?>

        </div>









</div> <!-- .post-nav-with-author -->




                                        <div class="single-small-container">
                                        <?php
                                        // If comments are open or we have at least one comment, load up the comment template.
                                        if (comments_open() || get_comments_number()) :
                                            comments_template();
                                        endif;
                                        ?>
                                        </div>





                                        
                            </main><!-- #main -->
                        </div><!-- #primary -->
                        <?php ?>
                    </div>
            </div>
        </div>
    </div>
<?php
get_footer();
