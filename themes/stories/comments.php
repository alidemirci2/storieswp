<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CoverNews
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
		<div class="comment-heading">
			<!-- improve escaping -->
			<h5 class="comments-title">
				<i class="icon icon-comment-alt-regular"></i>
				<?php
				$comment_count = get_comments_number();
				if ( 1 === $comment_count ) {
					printf(
						/* translators: 1: title. */
						esc_html_e( 'One Comment', 'Stories' ),
						'<span>' . get_the_title() . '</span>'
					);
				} else {
											
					echo sprintf( esc_html( _nx( '%s Comment', '%s Comments', get_comments_number(), 'number of comments', 'Stories' ) ), number_format_i18n( get_comments_number() ) );

				}
				?>
			</h5><!-- .comments-title -->

			<!-- improve: burdaki + carpiya donusecek ust divi tiklaninca. -->
			<div class="comment-expand">
				<i class="icon icon-chevron-right"></i>
			</div>
		</div>
	<div id="comments" class="comments-area">

	
	<?php


	if ( have_comments() ) : ?>


		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'callback' => 'stories_comment'
				) );
			?>
		</ol><!-- .comment-list -->

		<?php the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) : ?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'Stories' ); ?></p>
		<?php
		endif;

	endif; // Check for have_comments().

	comment_form();
	?>

</div><!-- #comments -->
