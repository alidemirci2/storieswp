jQuery(document).ready(function ($) {

	var ehf_hide_shortcode_field = function() {

		var selected = jQuery('#ehf_template_type').val();

		if( 'custom' == selected ) {
			jQuery( '.hfe-options-row.hfe-shortcode' ).show();
		} else {
			jQuery( '.hfe-options-row.hfe-shortcode' ).hide();
		}
	}  
	if (jQuery('#ehf_template_type').val() == "type_header"){
		jQuery("#setFooter").hide();
		jQuery("#setHeader").fadeIn();
	} else if (jQuery('#ehf_template_type').val() == "type_footer") { 
		jQuery("#setFooter").fadeIn();
		jQuery("#setHeader").hide();
	} 
	jQuery(document).on( 'change', '#ehf_template_type', function( e ) { 
		ehf_hide_shortcode_field();
		if (jQuery(this).val() == "type_header"){
			jQuery("#setFooter").hide();
			jQuery("#setHeader").fadeIn();
		} else if (jQuery(this).val() == "type_footer") { 
			jQuery("#setFooter").fadeIn();
			jQuery("#setHeader").hide();
		} 
	});
	jQuery(document)

	ehf_hide_shortcode_field();

});
